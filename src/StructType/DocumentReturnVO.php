<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for DocumentReturnVO StructType
 * @subpackage Structs
 */
class DocumentReturnVO extends AbstractStructBase
{
    /**
     * The documentReference
     * @var string
     */
    public $documentReference;
    /**
     * The instructions
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $instructions;
    /**
     * Constructor method for DocumentReturnVO
     * @uses DocumentReturnVO::setDocumentReference()
     * @uses DocumentReturnVO::setInstructions()
     * @param string $documentReference
     * @param string $instructions
     */
    public function __construct($documentReference = null, $instructions = null)
    {
        $this
            ->setDocumentReference($documentReference)
            ->setInstructions($instructions);
    }
    /**
     * Get documentReference value
     * @return string|null
     */
    public function getDocumentReference()
    {
        return $this->documentReference;
    }
    /**
     * Set documentReference value
     * @param string $documentReference
     * @return \DPDSDK\Shipment\StructType\DocumentReturnVO
     */
    public function setDocumentReference($documentReference = null)
    {
        // validation for constraint: string
        if (!is_null($documentReference) && !is_string($documentReference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($documentReference, true), gettype($documentReference)), __LINE__);
        }
        $this->documentReference = $documentReference;
        return $this;
    }
    /**
     * Get instructions value
     * @return string|null
     */
    public function getInstructions()
    {
        return $this->instructions;
    }
    /**
     * Set instructions value
     * @param string $instructions
     * @return \DPDSDK\Shipment\StructType\DocumentReturnVO
     */
    public function setInstructions($instructions = null)
    {
        // validation for constraint: string
        if (!is_null($instructions) && !is_string($instructions)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($instructions, true), gettype($instructions)), __LINE__);
        }
        $this->instructions = $instructions;
        return $this;
    }
}
