<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IdmSmsVOWL StructType
 * @subpackage Structs
 */
class IdmSmsVOWL extends AbstractStructBase
{
    /**
     * The sms
     * @var string
     */
    public $sms;
    /**
     * The rule
     * @var string
     */
    public $rule;
    /**
     * Constructor method for IdmSmsVOWL
     * @uses IdmSmsVOWL::setSms()
     * @uses IdmSmsVOWL::setRule()
     * @param string $sms
     * @param string $rule
     */
    public function __construct($sms = null, $rule = null)
    {
        $this
            ->setSms($sms)
            ->setRule($rule);
    }
    /**
     * Get sms value
     * @return string|null
     */
    public function getSms()
    {
        return $this->sms;
    }
    /**
     * Set sms value
     * @param string $sms
     * @return \DPDSDK\Shipment\StructType\IdmSmsVOWL
     */
    public function setSms($sms = null)
    {
        // validation for constraint: string
        if (!is_null($sms) && !is_string($sms)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sms, true), gettype($sms)), __LINE__);
        }
        $this->sms = $sms;
        return $this;
    }
    /**
     * Get rule value
     * @return string|null
     */
    public function getRule()
    {
        return $this->rule;
    }
    /**
     * Set rule value
     * @param string $rule
     * @return \DPDSDK\Shipment\StructType\IdmSmsVOWL
     */
    public function setRule($rule = null)
    {
        // validation for constraint: string
        if (!is_null($rule) && !is_string($rule)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rule, true), gettype($rule)), __LINE__);
        }
        $this->rule = $rule;
        return $this;
    }
}
