<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createShipmentWL StructType
 * Meta information extracted from the WSDL
 * - type: tns:createShipmentWL
 * @subpackage Structs
 */
class CreateShipmentWL extends AbstractStructBase
{
    /**
     * The wsUserName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsUserName;
    /**
     * The wsPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsPassword;
    /**
     * The wsLang
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsLang;
    /**
     * The applicationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $applicationType;
    /**
     * The businessApplication
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $businessApplication;
    /**
     * The shipment
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public $shipment;
    /**
     * Constructor method for createShipmentWL
     * @uses CreateShipmentWL::setWsUserName()
     * @uses CreateShipmentWL::setWsPassword()
     * @uses CreateShipmentWL::setWsLang()
     * @uses CreateShipmentWL::setApplicationType()
     * @uses CreateShipmentWL::setBusinessApplication()
     * @uses CreateShipmentWL::setShipment()
     * @param string $wsUserName
     * @param string $wsPassword
     * @param string $wsLang
     * @param string $applicationType
     * @param string $businessApplication
     * @param \DPDSDK\Shipment\StructType\ShipmentVOWL $shipment
     */
    public function __construct($wsUserName = null, $wsPassword = null, $wsLang = null, $applicationType = null, $businessApplication = null, \DPDSDK\Shipment\StructType\ShipmentVOWL $shipment = null)
    {
        $this
            ->setWsUserName($wsUserName)
            ->setWsPassword($wsPassword)
            ->setWsLang($wsLang)
            ->setApplicationType($applicationType)
            ->setBusinessApplication($businessApplication)
            ->setShipment($shipment);
    }
    /**
     * Get wsUserName value
     * @return string|null
     */
    public function getWsUserName()
    {
        return $this->wsUserName;
    }
    /**
     * Set wsUserName value
     * @param string $wsUserName
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setWsUserName($wsUserName = null)
    {
        // validation for constraint: string
        if (!is_null($wsUserName) && !is_string($wsUserName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsUserName, true), gettype($wsUserName)), __LINE__);
        }
        $this->wsUserName = $wsUserName;
        return $this;
    }
    /**
     * Get wsPassword value
     * @return string|null
     */
    public function getWsPassword()
    {
        return $this->wsPassword;
    }
    /**
     * Set wsPassword value
     * @param string $wsPassword
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setWsPassword($wsPassword = null)
    {
        // validation for constraint: string
        if (!is_null($wsPassword) && !is_string($wsPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsPassword, true), gettype($wsPassword)), __LINE__);
        }
        $this->wsPassword = $wsPassword;
        return $this;
    }
    /**
     * Get wsLang value
     * @return string|null
     */
    public function getWsLang()
    {
        return $this->wsLang;
    }
    /**
     * Set wsLang value
     * @param string $wsLang
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setWsLang($wsLang = null)
    {
        // validation for constraint: string
        if (!is_null($wsLang) && !is_string($wsLang)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsLang, true), gettype($wsLang)), __LINE__);
        }
        $this->wsLang = $wsLang;
        return $this;
    }
    /**
     * Get applicationType value
     * @return string|null
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }
    /**
     * Set applicationType value
     * @param string $applicationType
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setApplicationType($applicationType = null)
    {
        // validation for constraint: string
        if (!is_null($applicationType) && !is_string($applicationType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($applicationType, true), gettype($applicationType)), __LINE__);
        }
        $this->applicationType = $applicationType;
        return $this;
    }
    /**
     * Get businessApplication value
     * @return string|null
     */
    public function getBusinessApplication()
    {
        return $this->businessApplication;
    }
    /**
     * Set businessApplication value
     * @param string $businessApplication
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setBusinessApplication($businessApplication = null)
    {
        // validation for constraint: string
        if (!is_null($businessApplication) && !is_string($businessApplication)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($businessApplication, true), gettype($businessApplication)), __LINE__);
        }
        $this->businessApplication = $businessApplication;
        return $this;
    }
    /**
     * Get shipment value
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL|null
     */
    public function getShipment()
    {
        return $this->shipment;
    }
    /**
     * Set shipment value
     * @param \DPDSDK\Shipment\StructType\ShipmentVOWL $shipment
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWL
     */
    public function setShipment(\DPDSDK\Shipment\StructType\ShipmentVOWL $shipment = null)
    {
        $this->shipment = $shipment;
        return $this;
    }
}
