<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getShipment StructType
 * Meta information extracted from the WSDL
 * - type: tns:getShipment
 * @subpackage Structs
 */
class GetShipment extends AbstractStructBase
{
    /**
     * The wsUserName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsUserName;
    /**
     * The wsPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsPassword;
    /**
     * The wsLang
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsLang;
    /**
     * The applicationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $applicationType;
    /**
     * The businessApplication
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $businessApplication;
    /**
     * The shipmentReferenceList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ReferenceVO[]
     */
    public $shipmentReferenceList;
    /**
     * Constructor method for getShipment
     * @uses GetShipment::setWsUserName()
     * @uses GetShipment::setWsPassword()
     * @uses GetShipment::setWsLang()
     * @uses GetShipment::setApplicationType()
     * @uses GetShipment::setBusinessApplication()
     * @uses GetShipment::setShipmentReferenceList()
     * @param string $wsUserName
     * @param string $wsPassword
     * @param string $wsLang
     * @param string $applicationType
     * @param string $businessApplication
     * @param \DPDSDK\Shipment\StructType\ReferenceVO[] $shipmentReferenceList
     */
    public function __construct($wsUserName = null, $wsPassword = null, $wsLang = null, $applicationType = null, $businessApplication = null, array $shipmentReferenceList = array())
    {
        $this
            ->setWsUserName($wsUserName)
            ->setWsPassword($wsPassword)
            ->setWsLang($wsLang)
            ->setApplicationType($applicationType)
            ->setBusinessApplication($businessApplication)
            ->setShipmentReferenceList($shipmentReferenceList);
    }
    /**
     * Get wsUserName value
     * @return string|null
     */
    public function getWsUserName()
    {
        return $this->wsUserName;
    }
    /**
     * Set wsUserName value
     * @param string $wsUserName
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setWsUserName($wsUserName = null)
    {
        // validation for constraint: string
        if (!is_null($wsUserName) && !is_string($wsUserName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsUserName, true), gettype($wsUserName)), __LINE__);
        }
        $this->wsUserName = $wsUserName;
        return $this;
    }
    /**
     * Get wsPassword value
     * @return string|null
     */
    public function getWsPassword()
    {
        return $this->wsPassword;
    }
    /**
     * Set wsPassword value
     * @param string $wsPassword
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setWsPassword($wsPassword = null)
    {
        // validation for constraint: string
        if (!is_null($wsPassword) && !is_string($wsPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsPassword, true), gettype($wsPassword)), __LINE__);
        }
        $this->wsPassword = $wsPassword;
        return $this;
    }
    /**
     * Get wsLang value
     * @return string|null
     */
    public function getWsLang()
    {
        return $this->wsLang;
    }
    /**
     * Set wsLang value
     * @param string $wsLang
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setWsLang($wsLang = null)
    {
        // validation for constraint: string
        if (!is_null($wsLang) && !is_string($wsLang)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsLang, true), gettype($wsLang)), __LINE__);
        }
        $this->wsLang = $wsLang;
        return $this;
    }
    /**
     * Get applicationType value
     * @return string|null
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }
    /**
     * Set applicationType value
     * @param string $applicationType
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setApplicationType($applicationType = null)
    {
        // validation for constraint: string
        if (!is_null($applicationType) && !is_string($applicationType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($applicationType, true), gettype($applicationType)), __LINE__);
        }
        $this->applicationType = $applicationType;
        return $this;
    }
    /**
     * Get businessApplication value
     * @return string|null
     */
    public function getBusinessApplication()
    {
        return $this->businessApplication;
    }
    /**
     * Set businessApplication value
     * @param string $businessApplication
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setBusinessApplication($businessApplication = null)
    {
        // validation for constraint: string
        if (!is_null($businessApplication) && !is_string($businessApplication)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($businessApplication, true), gettype($businessApplication)), __LINE__);
        }
        $this->businessApplication = $businessApplication;
        return $this;
    }
    /**
     * Get shipmentReferenceList value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO[]|null
     */
    public function getShipmentReferenceList()
    {
        return $this->shipmentReferenceList;
    }
    /**
     * This method is responsible for validating the values passed to the setShipmentReferenceList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShipmentReferenceList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShipmentReferenceListForArrayConstraintsFromSetShipmentReferenceList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $getShipmentShipmentReferenceListItem) {
            // validation for constraint: itemType
            if (!$getShipmentShipmentReferenceListItem instanceof \DPDSDK\Shipment\StructType\ReferenceVO) {
                $invalidValues[] = is_object($getShipmentShipmentReferenceListItem) ? get_class($getShipmentShipmentReferenceListItem) : sprintf('%s(%s)', gettype($getShipmentShipmentReferenceListItem), var_export($getShipmentShipmentReferenceListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shipmentReferenceList property can only contain items of type \DPDSDK\Shipment\StructType\ReferenceVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set shipmentReferenceList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ReferenceVO[] $shipmentReferenceList
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function setShipmentReferenceList(array $shipmentReferenceList = array())
    {
        // validation for constraint: array
        if ('' !== ($shipmentReferenceListArrayErrorMessage = self::validateShipmentReferenceListForArrayConstraintsFromSetShipmentReferenceList($shipmentReferenceList))) {
            throw new \InvalidArgumentException($shipmentReferenceListArrayErrorMessage, __LINE__);
        }
        $this->shipmentReferenceList = $shipmentReferenceList;
        return $this;
    }
    /**
     * Add item to shipmentReferenceList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $item
     * @return \DPDSDK\Shipment\StructType\GetShipment
     */
    public function addToShipmentReferenceList(\DPDSDK\Shipment\StructType\ReferenceVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ReferenceVO) {
            throw new \InvalidArgumentException(sprintf('The shipmentReferenceList property can only contain items of type \DPDSDK\Shipment\StructType\ReferenceVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shipmentReferenceList[] = $item;
        return $this;
    }
}
