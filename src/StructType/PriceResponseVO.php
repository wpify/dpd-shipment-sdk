<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PriceResponseVO StructType
 * @subpackage Structs
 */
class PriceResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The priceList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PriceResultVO[]
     */
    public $priceList;
    /**
     * Constructor method for PriceResponseVO
     * @uses PriceResponseVO::setTransactionId()
     * @uses PriceResponseVO::setPriceList()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\PriceResultVO[] $priceList
     */
    public function __construct($transactionId = null, array $priceList = array())
    {
        $this
            ->setTransactionId($transactionId)
            ->setPriceList($priceList);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\PriceResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get priceList value
     * @return \DPDSDK\Shipment\StructType\PriceResultVO[]|null
     */
    public function getPriceList()
    {
        return $this->priceList;
    }
    /**
     * This method is responsible for validating the values passed to the setPriceList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPriceList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePriceListForArrayConstraintsFromSetPriceList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $priceResponseVOPriceListItem) {
            // validation for constraint: itemType
            if (!$priceResponseVOPriceListItem instanceof \DPDSDK\Shipment\StructType\PriceResultVO) {
                $invalidValues[] = is_object($priceResponseVOPriceListItem) ? get_class($priceResponseVOPriceListItem) : sprintf('%s(%s)', gettype($priceResponseVOPriceListItem), var_export($priceResponseVOPriceListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The priceList property can only contain items of type \DPDSDK\Shipment\StructType\PriceResultVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set priceList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\PriceResultVO[] $priceList
     * @return \DPDSDK\Shipment\StructType\PriceResponseVO
     */
    public function setPriceList(array $priceList = array())
    {
        // validation for constraint: array
        if ('' !== ($priceListArrayErrorMessage = self::validatePriceListForArrayConstraintsFromSetPriceList($priceList))) {
            throw new \InvalidArgumentException($priceListArrayErrorMessage, __LINE__);
        }
        $this->priceList = $priceList;
        return $this;
    }
    /**
     * Add item to priceList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\PriceResultVO $item
     * @return \DPDSDK\Shipment\StructType\PriceResponseVO
     */
    public function addToPriceList(\DPDSDK\Shipment\StructType\PriceResultVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\PriceResultVO) {
            throw new \InvalidArgumentException(sprintf('The priceList property can only contain items of type \DPDSDK\Shipment\StructType\PriceResultVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->priceList[] = $item;
        return $this;
    }
}
