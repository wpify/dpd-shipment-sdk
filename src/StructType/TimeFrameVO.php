<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TimeFrameVO StructType
 * @subpackage Structs
 */
class TimeFrameVO extends AbstractStructBase
{
    /**
     * The timeFrom
     * @var string
     */
    public $timeFrom;
    /**
     * The timeTo
     * @var string
     */
    public $timeTo;
    /**
     * Constructor method for TimeFrameVO
     * @uses TimeFrameVO::setTimeFrom()
     * @uses TimeFrameVO::setTimeTo()
     * @param string $timeFrom
     * @param string $timeTo
     */
    public function __construct($timeFrom = null, $timeTo = null)
    {
        $this
            ->setTimeFrom($timeFrom)
            ->setTimeTo($timeTo);
    }
    /**
     * Get timeFrom value
     * @return string|null
     */
    public function getTimeFrom()
    {
        return $this->timeFrom;
    }
    /**
     * Set timeFrom value
     * @param string $timeFrom
     * @return \DPDSDK\Shipment\StructType\TimeFrameVO
     */
    public function setTimeFrom($timeFrom = null)
    {
        // validation for constraint: string
        if (!is_null($timeFrom) && !is_string($timeFrom)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timeFrom, true), gettype($timeFrom)), __LINE__);
        }
        $this->timeFrom = $timeFrom;
        return $this;
    }
    /**
     * Get timeTo value
     * @return string|null
     */
    public function getTimeTo()
    {
        return $this->timeTo;
    }
    /**
     * Set timeTo value
     * @param string $timeTo
     * @return \DPDSDK\Shipment\StructType\TimeFrameVO
     */
    public function setTimeTo($timeTo = null)
    {
        // validation for constraint: string
        if (!is_null($timeTo) && !is_string($timeTo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($timeTo, true), gettype($timeTo)), __LINE__);
        }
        $this->timeTo = $timeTo;
        return $this;
    }
}
