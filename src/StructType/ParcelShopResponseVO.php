<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopResponseVO StructType
 * @subpackage Structs
 */
class ParcelShopResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The parcelShopList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelShopVO[]
     */
    public $parcelShopList;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ParcelShopResponseVO
     * @uses ParcelShopResponseVO::setTransactionId()
     * @uses ParcelShopResponseVO::setParcelShopList()
     * @uses ParcelShopResponseVO::setError()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ParcelShopVO[] $parcelShopList
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, array $parcelShopList = array(), \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setParcelShopList($parcelShopList)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get parcelShopList value
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO[]|null
     */
    public function getParcelShopList()
    {
        return $this->parcelShopList;
    }
    /**
     * This method is responsible for validating the values passed to the setParcelShopList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcelShopList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelShopListForArrayConstraintsFromSetParcelShopList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $parcelShopResponseVOParcelShopListItem) {
            // validation for constraint: itemType
            if (!$parcelShopResponseVOParcelShopListItem instanceof \DPDSDK\Shipment\StructType\ParcelShopVO) {
                $invalidValues[] = is_object($parcelShopResponseVOParcelShopListItem) ? get_class($parcelShopResponseVOParcelShopListItem) : sprintf('%s(%s)', gettype($parcelShopResponseVOParcelShopListItem), var_export($parcelShopResponseVOParcelShopListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcelShopList property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcelShopList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopVO[] $parcelShopList
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public function setParcelShopList(array $parcelShopList = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelShopListArrayErrorMessage = self::validateParcelShopListForArrayConstraintsFromSetParcelShopList($parcelShopList))) {
            throw new \InvalidArgumentException($parcelShopListArrayErrorMessage, __LINE__);
        }
        $this->parcelShopList = $parcelShopList;
        return $this;
    }
    /**
     * Add item to parcelShopList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopVO $item
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public function addToParcelShopList(\DPDSDK\Shipment\StructType\ParcelShopVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelShopVO) {
            throw new \InvalidArgumentException(sprintf('The parcelShopList property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcelShopList[] = $item;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
