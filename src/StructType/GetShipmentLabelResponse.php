<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getShipmentLabelResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:getShipmentLabelResponse
 * @subpackage Structs
 */
class GetShipmentLabelResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\PrintVO
     */
    public $result;
    /**
     * Constructor method for getShipmentLabelResponse
     * @uses GetShipmentLabelResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\PrintVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\PrintVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\PrintVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\PrintVO $result
     * @return \DPDSDK\Shipment\StructType\GetShipmentLabelResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\PrintVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
