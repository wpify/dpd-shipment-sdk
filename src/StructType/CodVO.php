<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for CodVO StructType
 * @subpackage Structs
 */
class CodVO extends AbstractStructBase
{
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The paymentType
     * @var string
     */
    public $paymentType;
    /**
     * The referenceNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $referenceNumber;
    /**
     * Constructor method for CodVO
     * @uses CodVO::setAmount()
     * @uses CodVO::setCurrency()
     * @uses CodVO::setPaymentType()
     * @uses CodVO::setReferenceNumber()
     * @param float $amount
     * @param string $currency
     * @param string $paymentType
     * @param string $referenceNumber
     */
    public function __construct($amount = null, $currency = null, $paymentType = null, $referenceNumber = null)
    {
        $this
            ->setAmount($amount)
            ->setCurrency($currency)
            ->setPaymentType($paymentType)
            ->setReferenceNumber($referenceNumber);
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \DPDSDK\Shipment\StructType\CodVO
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \DPDSDK\Shipment\StructType\CodVO
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get paymentType value
     * @return string|null
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
    /**
     * Set paymentType value
     * @uses \DPDSDK\Shipment\EnumType\PaymentType::valueIsValid()
     * @uses \DPDSDK\Shipment\EnumType\PaymentType::getValidValues()
     * @throws \InvalidArgumentException
     * @param string $paymentType
     * @return \DPDSDK\Shipment\StructType\CodVO
     */
    public function setPaymentType($paymentType = null)
    {
        // validation for constraint: enumeration
        if (!\DPDSDK\Shipment\EnumType\PaymentType::valueIsValid($paymentType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value(s) %s, please use one of: %s from enumeration class \DPDSDK\Shipment\EnumType\PaymentType', is_array($paymentType) ? implode(', ', $paymentType) : var_export($paymentType, true), implode(', ', \DPDSDK\Shipment\EnumType\PaymentType::getValidValues())), __LINE__);
        }
        $this->paymentType = $paymentType;
        return $this;
    }
    /**
     * Get referenceNumber value
     * @return string|null
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }
    /**
     * Set referenceNumber value
     * @param string $referenceNumber
     * @return \DPDSDK\Shipment\StructType\CodVO
     */
    public function setReferenceNumber($referenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($referenceNumber) && !is_string($referenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber, true), gettype($referenceNumber)), __LINE__);
        }
        $this->referenceNumber = $referenceNumber;
        return $this;
    }
}
