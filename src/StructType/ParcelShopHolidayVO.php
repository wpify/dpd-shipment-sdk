<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopHolidayVO StructType
 * @subpackage Structs
 */
class ParcelShopHolidayVO extends AbstractStructBase
{
    /**
     * The startDate
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $startDate;
    /**
     * The startTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $startTime;
    /**
     * The endDate
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $endDate;
    /**
     * The endTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $endTime;
    /**
     * Constructor method for ParcelShopHolidayVO
     * @uses ParcelShopHolidayVO::setStartDate()
     * @uses ParcelShopHolidayVO::setStartTime()
     * @uses ParcelShopHolidayVO::setEndDate()
     * @uses ParcelShopHolidayVO::setEndTime()
     * @param string $startDate
     * @param string $startTime
     * @param string $endDate
     * @param string $endTime
     */
    public function __construct($startDate = null, $startTime = null, $endDate = null, $endTime = null)
    {
        $this
            ->setStartDate($startDate)
            ->setStartTime($startTime)
            ->setEndDate($endDate)
            ->setEndTime($endTime);
    }
    /**
     * Get startDate value
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
    /**
     * Set startDate value
     * @param string $startDate
     * @return \DPDSDK\Shipment\StructType\ParcelShopHolidayVO
     */
    public function setStartDate($startDate = null)
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->startDate = $startDate;
        return $this;
    }
    /**
     * Get startTime value
     * @return string|null
     */
    public function getStartTime()
    {
        return $this->startTime;
    }
    /**
     * Set startTime value
     * @param string $startTime
     * @return \DPDSDK\Shipment\StructType\ParcelShopHolidayVO
     */
    public function setStartTime($startTime = null)
    {
        // validation for constraint: string
        if (!is_null($startTime) && !is_string($startTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startTime, true), gettype($startTime)), __LINE__);
        }
        $this->startTime = $startTime;
        return $this;
    }
    /**
     * Get endDate value
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    /**
     * Set endDate value
     * @param string $endDate
     * @return \DPDSDK\Shipment\StructType\ParcelShopHolidayVO
     */
    public function setEndDate($endDate = null)
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->endDate = $endDate;
        return $this;
    }
    /**
     * Get endTime value
     * @return string|null
     */
    public function getEndTime()
    {
        return $this->endTime;
    }
    /**
     * Set endTime value
     * @param string $endTime
     * @return \DPDSDK\Shipment\StructType\ParcelShopHolidayVO
     */
    public function setEndTime($endTime = null)
    {
        // validation for constraint: string
        if (!is_null($endTime) && !is_string($endTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endTime, true), gettype($endTime)), __LINE__);
        }
        $this->endTime = $endTime;
        return $this;
    }
}
