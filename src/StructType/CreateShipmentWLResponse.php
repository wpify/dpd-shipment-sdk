<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for createShipmentWLResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:createShipmentWLResponse
 * @subpackage Structs
 */
class CreateShipmentWLResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL
     */
    public $result;
    /**
     * Constructor method for createShipmentWLResponse
     * @uses CreateShipmentWLResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL $result
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWLResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
