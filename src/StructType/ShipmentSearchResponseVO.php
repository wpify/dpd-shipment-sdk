<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentSearchResponseVO StructType
 * @subpackage Structs
 */
class ShipmentSearchResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The shipmentInfoList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ShipmentInfoVO[]
     */
    public $shipmentInfoList;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ShipmentSearchResponseVO
     * @uses ShipmentSearchResponseVO::setTransactionId()
     * @uses ShipmentSearchResponseVO::setShipmentInfoList()
     * @uses ShipmentSearchResponseVO::setError()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ShipmentInfoVO[] $shipmentInfoList
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, array $shipmentInfoList = array(), \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setShipmentInfoList($shipmentInfoList)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get shipmentInfoList value
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO[]|null
     */
    public function getShipmentInfoList()
    {
        return $this->shipmentInfoList;
    }
    /**
     * This method is responsible for validating the values passed to the setShipmentInfoList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShipmentInfoList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShipmentInfoListForArrayConstraintsFromSetShipmentInfoList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentSearchResponseVOShipmentInfoListItem) {
            // validation for constraint: itemType
            if (!$shipmentSearchResponseVOShipmentInfoListItem instanceof \DPDSDK\Shipment\StructType\ShipmentInfoVO) {
                $invalidValues[] = is_object($shipmentSearchResponseVOShipmentInfoListItem) ? get_class($shipmentSearchResponseVOShipmentInfoListItem) : sprintf('%s(%s)', gettype($shipmentSearchResponseVOShipmentInfoListItem), var_export($shipmentSearchResponseVOShipmentInfoListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shipmentInfoList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentInfoVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set shipmentInfoList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentInfoVO[] $shipmentInfoList
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO
     */
    public function setShipmentInfoList(array $shipmentInfoList = array())
    {
        // validation for constraint: array
        if ('' !== ($shipmentInfoListArrayErrorMessage = self::validateShipmentInfoListForArrayConstraintsFromSetShipmentInfoList($shipmentInfoList))) {
            throw new \InvalidArgumentException($shipmentInfoListArrayErrorMessage, __LINE__);
        }
        $this->shipmentInfoList = $shipmentInfoList;
        return $this;
    }
    /**
     * Add item to shipmentInfoList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentInfoVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO
     */
    public function addToShipmentInfoList(\DPDSDK\Shipment\StructType\ShipmentInfoVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ShipmentInfoVO) {
            throw new \InvalidArgumentException(sprintf('The shipmentInfoList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentInfoVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shipmentInfoList[] = $item;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
