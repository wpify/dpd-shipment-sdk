<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EShopException StructType
 * Meta information extracted from the WSDL
 * - nillable: true
 * - type: tns:EShopException | ns1:EShopException
 * @subpackage Structs
 */
class EShopException extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for EShopException
     * @uses EShopException::setTransactionId()
     * @uses EShopException::setError()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\EShopException
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\EShopException
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
