<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalServiceVO StructType
 * @subpackage Structs
 */
class AdditionalServiceVO extends AbstractStructBase
{
    /**
     * The cod
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\CodVO
     */
    public $cod;
    /**
     * The saturdayDelivery
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var bool
     */
    public $saturdayDelivery;
    /**
     * The highInsurance
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\HighInsuranceVO
     */
    public $highInsurance;
    /**
     * The documentReturn
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\DocumentReturnVO
     */
    public $documentReturn;
    /**
     * The expay
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ExpayVO
     */
    public $expay;
    /**
     * The idCheck
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\IDCheckVO
     */
    public $idCheck;
    /**
     * The predictEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PredictEmailVO
     */
    public $predictEmail;
    /**
     * The predictSms
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PredictSmsVO
     */
    public $predictSms;
    /**
     * The swap
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $swap;
    /**
     * The returnLabel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $returnLabel;
    /**
     * The parcelShop
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public $parcelShop;
    /**
     * The moreServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO
     */
    public $moreServices;
    /**
     * The timeFrame
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\TimeFrameVO
     */
    public $timeFrame;
    /**
     * The ukdLabel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $ukdLabel;
    /**
     * The completeDelivery
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $completeDelivery;
    /**
     * The tyre
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $tyre;
    /**
     * Constructor method for AdditionalServiceVO
     * @uses AdditionalServiceVO::setCod()
     * @uses AdditionalServiceVO::setSaturdayDelivery()
     * @uses AdditionalServiceVO::setHighInsurance()
     * @uses AdditionalServiceVO::setDocumentReturn()
     * @uses AdditionalServiceVO::setExpay()
     * @uses AdditionalServiceVO::setIdCheck()
     * @uses AdditionalServiceVO::setPredictEmail()
     * @uses AdditionalServiceVO::setPredictSms()
     * @uses AdditionalServiceVO::setSwap()
     * @uses AdditionalServiceVO::setReturnLabel()
     * @uses AdditionalServiceVO::setParcelShop()
     * @uses AdditionalServiceVO::setMoreServices()
     * @uses AdditionalServiceVO::setTimeFrame()
     * @uses AdditionalServiceVO::setUkdLabel()
     * @uses AdditionalServiceVO::setCompleteDelivery()
     * @uses AdditionalServiceVO::setTyre()
     * @param \DPDSDK\Shipment\StructType\CodVO $cod
     * @param bool $saturdayDelivery
     * @param \DPDSDK\Shipment\StructType\HighInsuranceVO $highInsurance
     * @param \DPDSDK\Shipment\StructType\DocumentReturnVO $documentReturn
     * @param \DPDSDK\Shipment\StructType\ExpayVO $expay
     * @param \DPDSDK\Shipment\StructType\IDCheckVO $idCheck
     * @param \DPDSDK\Shipment\StructType\PredictEmailVO $predictEmail
     * @param \DPDSDK\Shipment\StructType\PredictSmsVO $predictSms
     * @param bool $swap
     * @param bool $returnLabel
     * @param \DPDSDK\Shipment\StructType\ParcelShopShipmentVO $parcelShop
     * @param \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO $moreServices
     * @param \DPDSDK\Shipment\StructType\TimeFrameVO $timeFrame
     * @param bool $ukdLabel
     * @param bool $completeDelivery
     * @param bool $tyre
     */
    public function __construct(\DPDSDK\Shipment\StructType\CodVO $cod = null, $saturdayDelivery = null, \DPDSDK\Shipment\StructType\HighInsuranceVO $highInsurance = null, \DPDSDK\Shipment\StructType\DocumentReturnVO $documentReturn = null, \DPDSDK\Shipment\StructType\ExpayVO $expay = null, \DPDSDK\Shipment\StructType\IDCheckVO $idCheck = null, \DPDSDK\Shipment\StructType\PredictEmailVO $predictEmail = null, \DPDSDK\Shipment\StructType\PredictSmsVO $predictSms = null, $swap = null, $returnLabel = null, \DPDSDK\Shipment\StructType\ParcelShopShipmentVO $parcelShop = null, \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO $moreServices = null, \DPDSDK\Shipment\StructType\TimeFrameVO $timeFrame = null, $ukdLabel = null, $completeDelivery = null, $tyre = null)
    {
        $this
            ->setCod($cod)
            ->setSaturdayDelivery($saturdayDelivery)
            ->setHighInsurance($highInsurance)
            ->setDocumentReturn($documentReturn)
            ->setExpay($expay)
            ->setIdCheck($idCheck)
            ->setPredictEmail($predictEmail)
            ->setPredictSms($predictSms)
            ->setSwap($swap)
            ->setReturnLabel($returnLabel)
            ->setParcelShop($parcelShop)
            ->setMoreServices($moreServices)
            ->setTimeFrame($timeFrame)
            ->setUkdLabel($ukdLabel)
            ->setCompleteDelivery($completeDelivery)
            ->setTyre($tyre);
    }
    /**
     * Get cod value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\CodVO|null
     */
    public function getCod()
    {
        return isset($this->cod) ? $this->cod : null;
    }
    /**
     * Set cod value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\CodVO $cod
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setCod(\DPDSDK\Shipment\StructType\CodVO $cod = null)
    {
        if (is_null($cod) || (is_array($cod) && empty($cod))) {
            unset($this->cod);
        } else {
            $this->cod = $cod;
        }
        return $this;
    }
    /**
     * Get saturdayDelivery value
     * @return bool|null
     */
    public function getSaturdayDelivery()
    {
        return $this->saturdayDelivery;
    }
    /**
     * Set saturdayDelivery value
     * @param bool $saturdayDelivery
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setSaturdayDelivery($saturdayDelivery = null)
    {
        // validation for constraint: boolean
        if (!is_null($saturdayDelivery) && !is_bool($saturdayDelivery)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($saturdayDelivery, true), gettype($saturdayDelivery)), __LINE__);
        }
        $this->saturdayDelivery = $saturdayDelivery;
        return $this;
    }
    /**
     * Get highInsurance value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\HighInsuranceVO|null
     */
    public function getHighInsurance()
    {
        return isset($this->highInsurance) ? $this->highInsurance : null;
    }
    /**
     * Set highInsurance value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\HighInsuranceVO $highInsurance
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setHighInsurance(\DPDSDK\Shipment\StructType\HighInsuranceVO $highInsurance = null)
    {
        if (is_null($highInsurance) || (is_array($highInsurance) && empty($highInsurance))) {
            unset($this->highInsurance);
        } else {
            $this->highInsurance = $highInsurance;
        }
        return $this;
    }
    /**
     * Get documentReturn value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\DocumentReturnVO|null
     */
    public function getDocumentReturn()
    {
        return isset($this->documentReturn) ? $this->documentReturn : null;
    }
    /**
     * Set documentReturn value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\DocumentReturnVO $documentReturn
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setDocumentReturn(\DPDSDK\Shipment\StructType\DocumentReturnVO $documentReturn = null)
    {
        if (is_null($documentReturn) || (is_array($documentReturn) && empty($documentReturn))) {
            unset($this->documentReturn);
        } else {
            $this->documentReturn = $documentReturn;
        }
        return $this;
    }
    /**
     * Get expay value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\ExpayVO|null
     */
    public function getExpay()
    {
        return isset($this->expay) ? $this->expay : null;
    }
    /**
     * Set expay value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\ExpayVO $expay
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setExpay(\DPDSDK\Shipment\StructType\ExpayVO $expay = null)
    {
        if (is_null($expay) || (is_array($expay) && empty($expay))) {
            unset($this->expay);
        } else {
            $this->expay = $expay;
        }
        return $this;
    }
    /**
     * Get idCheck value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\IDCheckVO|null
     */
    public function getIdCheck()
    {
        return isset($this->idCheck) ? $this->idCheck : null;
    }
    /**
     * Set idCheck value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\IDCheckVO $idCheck
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setIdCheck(\DPDSDK\Shipment\StructType\IDCheckVO $idCheck = null)
    {
        if (is_null($idCheck) || (is_array($idCheck) && empty($idCheck))) {
            unset($this->idCheck);
        } else {
            $this->idCheck = $idCheck;
        }
        return $this;
    }
    /**
     * Get predictEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\PredictEmailVO|null
     */
    public function getPredictEmail()
    {
        return isset($this->predictEmail) ? $this->predictEmail : null;
    }
    /**
     * Set predictEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\PredictEmailVO $predictEmail
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setPredictEmail(\DPDSDK\Shipment\StructType\PredictEmailVO $predictEmail = null)
    {
        if (is_null($predictEmail) || (is_array($predictEmail) && empty($predictEmail))) {
            unset($this->predictEmail);
        } else {
            $this->predictEmail = $predictEmail;
        }
        return $this;
    }
    /**
     * Get predictSms value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\PredictSmsVO|null
     */
    public function getPredictSms()
    {
        return isset($this->predictSms) ? $this->predictSms : null;
    }
    /**
     * Set predictSms value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\PredictSmsVO $predictSms
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setPredictSms(\DPDSDK\Shipment\StructType\PredictSmsVO $predictSms = null)
    {
        if (is_null($predictSms) || (is_array($predictSms) && empty($predictSms))) {
            unset($this->predictSms);
        } else {
            $this->predictSms = $predictSms;
        }
        return $this;
    }
    /**
     * Get swap value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getSwap()
    {
        return isset($this->swap) ? $this->swap : null;
    }
    /**
     * Set swap value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $swap
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setSwap($swap = null)
    {
        // validation for constraint: boolean
        if (!is_null($swap) && !is_bool($swap)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($swap, true), gettype($swap)), __LINE__);
        }
        if (is_null($swap) || (is_array($swap) && empty($swap))) {
            unset($this->swap);
        } else {
            $this->swap = $swap;
        }
        return $this;
    }
    /**
     * Get returnLabel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getReturnLabel()
    {
        return isset($this->returnLabel) ? $this->returnLabel : null;
    }
    /**
     * Set returnLabel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $returnLabel
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setReturnLabel($returnLabel = null)
    {
        // validation for constraint: boolean
        if (!is_null($returnLabel) && !is_bool($returnLabel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($returnLabel, true), gettype($returnLabel)), __LINE__);
        }
        if (is_null($returnLabel) || (is_array($returnLabel) && empty($returnLabel))) {
            unset($this->returnLabel);
        } else {
            $this->returnLabel = $returnLabel;
        }
        return $this;
    }
    /**
     * Get parcelShop value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO|null
     */
    public function getParcelShop()
    {
        return isset($this->parcelShop) ? $this->parcelShop : null;
    }
    /**
     * Set parcelShop value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\ParcelShopShipmentVO $parcelShop
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setParcelShop(\DPDSDK\Shipment\StructType\ParcelShopShipmentVO $parcelShop = null)
    {
        if (is_null($parcelShop) || (is_array($parcelShop) && empty($parcelShop))) {
            unset($this->parcelShop);
        } else {
            $this->parcelShop = $parcelShop;
        }
        return $this;
    }
    /**
     * Get moreServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO|null
     */
    public function getMoreServices()
    {
        return isset($this->moreServices) ? $this->moreServices : null;
    }
    /**
     * Set moreServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO $moreServices
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setMoreServices(\DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO $moreServices = null)
    {
        if (is_null($moreServices) || (is_array($moreServices) && empty($moreServices))) {
            unset($this->moreServices);
        } else {
            $this->moreServices = $moreServices;
        }
        return $this;
    }
    /**
     * Get timeFrame value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\TimeFrameVO|null
     */
    public function getTimeFrame()
    {
        return isset($this->timeFrame) ? $this->timeFrame : null;
    }
    /**
     * Set timeFrame value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\TimeFrameVO $timeFrame
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setTimeFrame(\DPDSDK\Shipment\StructType\TimeFrameVO $timeFrame = null)
    {
        if (is_null($timeFrame) || (is_array($timeFrame) && empty($timeFrame))) {
            unset($this->timeFrame);
        } else {
            $this->timeFrame = $timeFrame;
        }
        return $this;
    }
    /**
     * Get ukdLabel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getUkdLabel()
    {
        return isset($this->ukdLabel) ? $this->ukdLabel : null;
    }
    /**
     * Set ukdLabel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $ukdLabel
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setUkdLabel($ukdLabel = null)
    {
        // validation for constraint: boolean
        if (!is_null($ukdLabel) && !is_bool($ukdLabel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($ukdLabel, true), gettype($ukdLabel)), __LINE__);
        }
        if (is_null($ukdLabel) || (is_array($ukdLabel) && empty($ukdLabel))) {
            unset($this->ukdLabel);
        } else {
            $this->ukdLabel = $ukdLabel;
        }
        return $this;
    }
    /**
     * Get completeDelivery value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getCompleteDelivery()
    {
        return isset($this->completeDelivery) ? $this->completeDelivery : null;
    }
    /**
     * Set completeDelivery value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $completeDelivery
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setCompleteDelivery($completeDelivery = null)
    {
        // validation for constraint: boolean
        if (!is_null($completeDelivery) && !is_bool($completeDelivery)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($completeDelivery, true), gettype($completeDelivery)), __LINE__);
        }
        if (is_null($completeDelivery) || (is_array($completeDelivery) && empty($completeDelivery))) {
            unset($this->completeDelivery);
        } else {
            $this->completeDelivery = $completeDelivery;
        }
        return $this;
    }
    /**
     * Get tyre value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getTyre()
    {
        return isset($this->tyre) ? $this->tyre : null;
    }
    /**
     * Set tyre value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $tyre
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public function setTyre($tyre = null)
    {
        // validation for constraint: boolean
        if (!is_null($tyre) && !is_bool($tyre)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($tyre, true), gettype($tyre)), __LINE__);
        }
        if (is_null($tyre) || (is_array($tyre) && empty($tyre))) {
            unset($this->tyre);
        } else {
            $this->tyre = $tyre;
        }
        return $this;
    }
}
