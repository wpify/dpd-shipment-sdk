<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PriceResultVO StructType
 * @subpackage Structs
 */
class PriceResultVO extends AbstractStructBase
{
    /**
     * The price
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PriceVO
     */
    public $price;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for PriceResultVO
     * @uses PriceResultVO::setPrice()
     * @uses PriceResultVO::setError()
     * @param \DPDSDK\Shipment\StructType\PriceVO $price
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct(\DPDSDK\Shipment\StructType\PriceVO $price = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setPrice($price)
            ->setError($error);
    }
    /**
     * Get price value
     * @return \DPDSDK\Shipment\StructType\PriceVO|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param \DPDSDK\Shipment\StructType\PriceVO $price
     * @return \DPDSDK\Shipment\StructType\PriceResultVO
     */
    public function setPrice(\DPDSDK\Shipment\StructType\PriceVO $price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\PriceResultVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
