<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelResponseVO StructType
 * @subpackage Structs
 */
class ParcelResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The parcels
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelDetailsVO[]
     */
    public $parcels;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ParcelResponseVO
     * @uses ParcelResponseVO::setTransactionId()
     * @uses ParcelResponseVO::setParcels()
     * @uses ParcelResponseVO::setError()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ParcelDetailsVO[] $parcels
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, array $parcels = array(), \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setParcels($parcels)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ParcelResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get parcels value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\ParcelDetailsVO[]|null
     */
    public function getParcels()
    {
        return isset($this->parcels) ? $this->parcels : null;
    }
    /**
     * This method is responsible for validating the values passed to the setParcels method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcels method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelsForArrayConstraintsFromSetParcels(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $parcelResponseVOParcelsItem) {
            // validation for constraint: itemType
            if (!$parcelResponseVOParcelsItem instanceof \DPDSDK\Shipment\StructType\ParcelDetailsVO) {
                $invalidValues[] = is_object($parcelResponseVOParcelsItem) ? get_class($parcelResponseVOParcelsItem) : sprintf('%s(%s)', gettype($parcelResponseVOParcelsItem), var_export($parcelResponseVOParcelsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelDetailsVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcels value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelDetailsVO[] $parcels
     * @return \DPDSDK\Shipment\StructType\ParcelResponseVO
     */
    public function setParcels(array $parcels = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelsArrayErrorMessage = self::validateParcelsForArrayConstraintsFromSetParcels($parcels))) {
            throw new \InvalidArgumentException($parcelsArrayErrorMessage, __LINE__);
        }
        if (is_null($parcels) || (is_array($parcels) && empty($parcels))) {
            unset($this->parcels);
        } else {
            $this->parcels = $parcels;
        }
        return $this;
    }
    /**
     * Add item to parcels value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelDetailsVO $item
     * @return \DPDSDK\Shipment\StructType\ParcelResponseVO
     */
    public function addToParcels(\DPDSDK\Shipment\StructType\ParcelDetailsVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelDetailsVO) {
            throw new \InvalidArgumentException(sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelDetailsVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcels[] = $item;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ParcelResponseVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
