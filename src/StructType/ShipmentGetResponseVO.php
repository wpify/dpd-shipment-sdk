<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentGetResponseVO StructType
 * @subpackage Structs
 */
class ShipmentGetResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The shipmentResultList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ShipmentGetResultVO[]
     */
    public $shipmentResultList;
    /**
     * Constructor method for ShipmentGetResponseVO
     * @uses ShipmentGetResponseVO::setTransactionId()
     * @uses ShipmentGetResponseVO::setShipmentResultList()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ShipmentGetResultVO[] $shipmentResultList
     */
    public function __construct($transactionId = null, array $shipmentResultList = array())
    {
        $this
            ->setTransactionId($transactionId)
            ->setShipmentResultList($shipmentResultList);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get shipmentResultList value
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResultVO[]|null
     */
    public function getShipmentResultList()
    {
        return $this->shipmentResultList;
    }
    /**
     * This method is responsible for validating the values passed to the setShipmentResultList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setShipmentResultList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateShipmentResultListForArrayConstraintsFromSetShipmentResultList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentGetResponseVOShipmentResultListItem) {
            // validation for constraint: itemType
            if (!$shipmentGetResponseVOShipmentResultListItem instanceof \DPDSDK\Shipment\StructType\ShipmentGetResultVO) {
                $invalidValues[] = is_object($shipmentGetResponseVOShipmentResultListItem) ? get_class($shipmentGetResponseVOShipmentResultListItem) : sprintf('%s(%s)', gettype($shipmentGetResponseVOShipmentResultListItem), var_export($shipmentGetResponseVOShipmentResultListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The shipmentResultList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentGetResultVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set shipmentResultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentGetResultVO[] $shipmentResultList
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResponseVO
     */
    public function setShipmentResultList(array $shipmentResultList = array())
    {
        // validation for constraint: array
        if ('' !== ($shipmentResultListArrayErrorMessage = self::validateShipmentResultListForArrayConstraintsFromSetShipmentResultList($shipmentResultList))) {
            throw new \InvalidArgumentException($shipmentResultListArrayErrorMessage, __LINE__);
        }
        $this->shipmentResultList = $shipmentResultList;
        return $this;
    }
    /**
     * Add item to shipmentResultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentGetResultVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResponseVO
     */
    public function addToShipmentResultList(\DPDSDK\Shipment\StructType\ShipmentGetResultVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ShipmentGetResultVO) {
            throw new \InvalidArgumentException(sprintf('The shipmentResultList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentGetResultVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->shipmentResultList[] = $item;
        return $this;
    }
}
