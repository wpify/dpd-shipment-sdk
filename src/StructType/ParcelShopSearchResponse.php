<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for parcelShopSearchResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:parcelShopSearchResponse
 * @subpackage Structs
 */
class ParcelShopSearchResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public $result;
    /**
     * Constructor method for parcelShopSearchResponse
     * @uses ParcelShopSearchResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ParcelShopResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ParcelShopResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ParcelShopResponseVO $result
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ParcelShopResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
