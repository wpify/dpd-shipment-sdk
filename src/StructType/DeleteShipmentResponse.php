<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for deleteShipmentResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:deleteShipmentResponse
 * @subpackage Structs
 */
class DeleteShipmentResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO
     */
    public $result;
    /**
     * Constructor method for deleteShipmentResponse
     * @uses DeleteShipmentResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO $result
     * @return \DPDSDK\Shipment\StructType\DeleteShipmentResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ShipmentDeleteResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
