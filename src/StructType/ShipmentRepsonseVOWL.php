<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentRepsonseVOWL StructType
 * @subpackage Structs
 */
class ShipmentRepsonseVOWL extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The resultList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var string[]
     */
    public $resultList;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ShipmentRepsonseVOWL
     * @uses ShipmentRepsonseVOWL::setTransactionId()
     * @uses ShipmentRepsonseVOWL::setResultList()
     * @uses ShipmentRepsonseVOWL::setError()
     * @param int $transactionId
     * @param string[] $resultList
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, array $resultList = array(), \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setResultList($resultList)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get resultList value
     * @return string[]|null
     */
    public function getResultList()
    {
        return $this->resultList;
    }
    /**
     * This method is responsible for validating the values passed to the setResultList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResultList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResultListForArrayConstraintsFromSetResultList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentRepsonseVOWLResultListItem) {
            // validation for constraint: itemType
            if (!is_string($shipmentRepsonseVOWLResultListItem)) {
                $invalidValues[] = is_object($shipmentRepsonseVOWLResultListItem) ? get_class($shipmentRepsonseVOWLResultListItem) : sprintf('%s(%s)', gettype($shipmentRepsonseVOWLResultListItem), var_export($shipmentRepsonseVOWLResultListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resultList property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set resultList value
     * @throws \InvalidArgumentException
     * @param string[] $resultList
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL
     */
    public function setResultList(array $resultList = array())
    {
        // validation for constraint: array
        if ('' !== ($resultListArrayErrorMessage = self::validateResultListForArrayConstraintsFromSetResultList($resultList))) {
            throw new \InvalidArgumentException($resultListArrayErrorMessage, __LINE__);
        }
        $this->resultList = $resultList;
        return $this;
    }
    /**
     * Add item to resultList value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL
     */
    public function addToResultList($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The resultList property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resultList[] = $item;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVOWL
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
