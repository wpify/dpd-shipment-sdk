<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelResultVO StructType
 * @subpackage Structs
 */
class ParcelResultVO extends AbstractStructBase
{
    /**
     * The parcelId
     * @var string
     */
    public $parcelId;
    /**
     * The parcelReferenceNumber
     * @var string
     */
    public $parcelReferenceNumber;
    /**
     * The parcelReferenceNumber1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber1;
    /**
     * The parcelReferenceNumber2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber2;
    /**
     * The parcelReferenceNumber3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber3;
    /**
     * The printRequired
     * @var bool
     */
    public $printRequired;
    /**
     * Constructor method for ParcelResultVO
     * @uses ParcelResultVO::setParcelId()
     * @uses ParcelResultVO::setParcelReferenceNumber()
     * @uses ParcelResultVO::setParcelReferenceNumber1()
     * @uses ParcelResultVO::setParcelReferenceNumber2()
     * @uses ParcelResultVO::setParcelReferenceNumber3()
     * @uses ParcelResultVO::setPrintRequired()
     * @param string $parcelId
     * @param string $parcelReferenceNumber
     * @param string $parcelReferenceNumber1
     * @param string $parcelReferenceNumber2
     * @param string $parcelReferenceNumber3
     * @param bool $printRequired
     */
    public function __construct($parcelId = null, $parcelReferenceNumber = null, $parcelReferenceNumber1 = null, $parcelReferenceNumber2 = null, $parcelReferenceNumber3 = null, $printRequired = null)
    {
        $this
            ->setParcelId($parcelId)
            ->setParcelReferenceNumber($parcelReferenceNumber)
            ->setParcelReferenceNumber1($parcelReferenceNumber1)
            ->setParcelReferenceNumber2($parcelReferenceNumber2)
            ->setParcelReferenceNumber3($parcelReferenceNumber3)
            ->setPrintRequired($printRequired);
    }
    /**
     * Get parcelId value
     * @return string|null
     */
    public function getParcelId()
    {
        return $this->parcelId;
    }
    /**
     * Set parcelId value
     * @param string $parcelId
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setParcelId($parcelId = null)
    {
        // validation for constraint: string
        if (!is_null($parcelId) && !is_string($parcelId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelId, true), gettype($parcelId)), __LINE__);
        }
        $this->parcelId = $parcelId;
        return $this;
    }
    /**
     * Get parcelReferenceNumber value
     * @return string|null
     */
    public function getParcelReferenceNumber()
    {
        return $this->parcelReferenceNumber;
    }
    /**
     * Set parcelReferenceNumber value
     * @param string $parcelReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setParcelReferenceNumber($parcelReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber) && !is_string($parcelReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber, true), gettype($parcelReferenceNumber)), __LINE__);
        }
        $this->parcelReferenceNumber = $parcelReferenceNumber;
        return $this;
    }
    /**
     * Get parcelReferenceNumber1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber1()
    {
        return isset($this->parcelReferenceNumber1) ? $this->parcelReferenceNumber1 : null;
    }
    /**
     * Set parcelReferenceNumber1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber1
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setParcelReferenceNumber1($parcelReferenceNumber1 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber1) && !is_string($parcelReferenceNumber1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber1, true), gettype($parcelReferenceNumber1)), __LINE__);
        }
        if (is_null($parcelReferenceNumber1) || (is_array($parcelReferenceNumber1) && empty($parcelReferenceNumber1))) {
            unset($this->parcelReferenceNumber1);
        } else {
            $this->parcelReferenceNumber1 = $parcelReferenceNumber1;
        }
        return $this;
    }
    /**
     * Get parcelReferenceNumber2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber2()
    {
        return isset($this->parcelReferenceNumber2) ? $this->parcelReferenceNumber2 : null;
    }
    /**
     * Set parcelReferenceNumber2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber2
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setParcelReferenceNumber2($parcelReferenceNumber2 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber2) && !is_string($parcelReferenceNumber2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber2, true), gettype($parcelReferenceNumber2)), __LINE__);
        }
        if (is_null($parcelReferenceNumber2) || (is_array($parcelReferenceNumber2) && empty($parcelReferenceNumber2))) {
            unset($this->parcelReferenceNumber2);
        } else {
            $this->parcelReferenceNumber2 = $parcelReferenceNumber2;
        }
        return $this;
    }
    /**
     * Get parcelReferenceNumber3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber3()
    {
        return isset($this->parcelReferenceNumber3) ? $this->parcelReferenceNumber3 : null;
    }
    /**
     * Set parcelReferenceNumber3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber3
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setParcelReferenceNumber3($parcelReferenceNumber3 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber3) && !is_string($parcelReferenceNumber3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber3, true), gettype($parcelReferenceNumber3)), __LINE__);
        }
        if (is_null($parcelReferenceNumber3) || (is_array($parcelReferenceNumber3) && empty($parcelReferenceNumber3))) {
            unset($this->parcelReferenceNumber3);
        } else {
            $this->parcelReferenceNumber3 = $parcelReferenceNumber3;
        }
        return $this;
    }
    /**
     * Get printRequired value
     * @return bool|null
     */
    public function getPrintRequired()
    {
        return $this->printRequired;
    }
    /**
     * Set printRequired value
     * @param bool $printRequired
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO
     */
    public function setPrintRequired($printRequired = null)
    {
        // validation for constraint: boolean
        if (!is_null($printRequired) && !is_bool($printRequired)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($printRequired, true), gettype($printRequired)), __LINE__);
        }
        $this->printRequired = $printRequired;
        return $this;
    }
}
