<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PrintDataVO StructType
 * @subpackage Structs
 */
class PrintDataVO extends AbstractStructBase
{
    /**
     * The shipmentId
     * @var int
     */
    public $shipmentId;
    /**
     * The parcelId
     * @var int
     */
    public $parcelId;
    /**
     * The senderCustName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderCustName;
    /**
     * The senderAddress
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderAddress;
    /**
     * The senderZipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderZipCode;
    /**
     * The senderCountryAlphaCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderCountryAlphaCode;
    /**
     * The senderTaxNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderTaxNumber;
    /**
     * The senderTelNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderTelNumber;
    /**
     * The senderCity
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderCity;
    /**
     * The senderContact
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $senderContact;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyName;
    /**
     * The companyTaxNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyTaxNumber;
    /**
     * The companyAddress
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyAddress;
    /**
     * The companyTelNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyTelNumber;
    /**
     * The companyFaxNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyFaxNumber;
    /**
     * The barcode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $barcode;
    /**
     * The barcodeText
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $barcodeText;
    /**
     * The shipmentReferenceNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber;
    /**
     * The parcelIndex
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $parcelIndex;
    /**
     * The parcelCount
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $parcelCount;
    /**
     * The receiverName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverName;
    /**
     * The receiverAddress1
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverAddress1;
    /**
     * The receiverAddress2
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverAddress2;
    /**
     * The receiverZipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverZipCode;
    /**
     * The receiverCity
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverCity;
    /**
     * The receiverCountryCodeAlpha
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverCountryCodeAlpha;
    /**
     * The receiverTaxNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverTaxNumber;
    /**
     * The extraInformationOnLabel
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $extraInformationOnLabel;
    /**
     * The departureUnitId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $departureUnitId;
    /**
     * The deliveryUnitId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $deliveryUnitId;
    /**
     * The serviceCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $serviceCode;
    /**
     * The serviceCombination
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $serviceCombination;
    /**
     * The routeDsort
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $routeDsort;
    /**
     * The routeOsort
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $routeOsort;
    /**
     * The receiverFirmName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverFirmName;
    /**
     * The receiverContactName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverContactName;
    /**
     * The receiverPhoneNr1
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverPhoneNr1;
    /**
     * The receiverPhoneNr2
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverPhoneNr2;
    /**
     * The receiverAccessCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverAccessCode;
    /**
     * The codAmountOfValue
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $codAmountOfValue;
    /**
     * The codCurrency
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codCurrency;
    /**
     * The codReferenceNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codReferenceNo;
    /**
     * The codPaymentType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codPaymentType;
    /**
     * The parcelNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelNo;
    /**
     * The weight
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $weight;
    /**
     * The volume
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $volume;
    /**
     * The parcelReferenceNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber;
    /**
     * The additionalLabelFlag
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $additionalLabelFlag;
    /**
     * The additionalInformationData
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $additionalInformationData;
    /**
     * The serviceFieldInfo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $serviceFieldInfo;
    /**
     * The serviceMark
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $serviceMark;
    /**
     * The damageInformation
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $damageInformation;
    /**
     * The parcelNoCsd
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelNoCsd;
    /**
     * Constructor method for PrintDataVO
     * @uses PrintDataVO::setShipmentId()
     * @uses PrintDataVO::setParcelId()
     * @uses PrintDataVO::setSenderCustName()
     * @uses PrintDataVO::setSenderAddress()
     * @uses PrintDataVO::setSenderZipCode()
     * @uses PrintDataVO::setSenderCountryAlphaCode()
     * @uses PrintDataVO::setSenderTaxNumber()
     * @uses PrintDataVO::setSenderTelNumber()
     * @uses PrintDataVO::setSenderCity()
     * @uses PrintDataVO::setSenderContact()
     * @uses PrintDataVO::setCompanyName()
     * @uses PrintDataVO::setCompanyTaxNumber()
     * @uses PrintDataVO::setCompanyAddress()
     * @uses PrintDataVO::setCompanyTelNumber()
     * @uses PrintDataVO::setCompanyFaxNumber()
     * @uses PrintDataVO::setBarcode()
     * @uses PrintDataVO::setBarcodeText()
     * @uses PrintDataVO::setShipmentReferenceNumber()
     * @uses PrintDataVO::setParcelIndex()
     * @uses PrintDataVO::setParcelCount()
     * @uses PrintDataVO::setReceiverName()
     * @uses PrintDataVO::setReceiverAddress1()
     * @uses PrintDataVO::setReceiverAddress2()
     * @uses PrintDataVO::setReceiverZipCode()
     * @uses PrintDataVO::setReceiverCity()
     * @uses PrintDataVO::setReceiverCountryCodeAlpha()
     * @uses PrintDataVO::setReceiverTaxNumber()
     * @uses PrintDataVO::setExtraInformationOnLabel()
     * @uses PrintDataVO::setDepartureUnitId()
     * @uses PrintDataVO::setDeliveryUnitId()
     * @uses PrintDataVO::setServiceCode()
     * @uses PrintDataVO::setServiceCombination()
     * @uses PrintDataVO::setRouteDsort()
     * @uses PrintDataVO::setRouteOsort()
     * @uses PrintDataVO::setReceiverFirmName()
     * @uses PrintDataVO::setReceiverContactName()
     * @uses PrintDataVO::setReceiverPhoneNr1()
     * @uses PrintDataVO::setReceiverPhoneNr2()
     * @uses PrintDataVO::setReceiverAccessCode()
     * @uses PrintDataVO::setCodAmountOfValue()
     * @uses PrintDataVO::setCodCurrency()
     * @uses PrintDataVO::setCodReferenceNo()
     * @uses PrintDataVO::setCodPaymentType()
     * @uses PrintDataVO::setParcelNo()
     * @uses PrintDataVO::setWeight()
     * @uses PrintDataVO::setVolume()
     * @uses PrintDataVO::setParcelReferenceNumber()
     * @uses PrintDataVO::setAdditionalLabelFlag()
     * @uses PrintDataVO::setAdditionalInformationData()
     * @uses PrintDataVO::setServiceFieldInfo()
     * @uses PrintDataVO::setServiceMark()
     * @uses PrintDataVO::setDamageInformation()
     * @uses PrintDataVO::setParcelNoCsd()
     * @param int $shipmentId
     * @param int $parcelId
     * @param string $senderCustName
     * @param string $senderAddress
     * @param string $senderZipCode
     * @param string $senderCountryAlphaCode
     * @param string $senderTaxNumber
     * @param string $senderTelNumber
     * @param string $senderCity
     * @param string $senderContact
     * @param string $companyName
     * @param string $companyTaxNumber
     * @param string $companyAddress
     * @param string $companyTelNumber
     * @param string $companyFaxNumber
     * @param string $barcode
     * @param string $barcodeText
     * @param string $shipmentReferenceNumber
     * @param int $parcelIndex
     * @param int $parcelCount
     * @param string $receiverName
     * @param string $receiverAddress1
     * @param string $receiverAddress2
     * @param string $receiverZipCode
     * @param string $receiverCity
     * @param string $receiverCountryCodeAlpha
     * @param string $receiverTaxNumber
     * @param string $extraInformationOnLabel
     * @param string $departureUnitId
     * @param string $deliveryUnitId
     * @param string $serviceCode
     * @param string $serviceCombination
     * @param string $routeDsort
     * @param string $routeOsort
     * @param string $receiverFirmName
     * @param string $receiverContactName
     * @param string $receiverPhoneNr1
     * @param string $receiverPhoneNr2
     * @param string $receiverAccessCode
     * @param float $codAmountOfValue
     * @param string $codCurrency
     * @param string $codReferenceNo
     * @param string $codPaymentType
     * @param string $parcelNo
     * @param float $weight
     * @param float $volume
     * @param string $parcelReferenceNumber
     * @param string $additionalLabelFlag
     * @param string $additionalInformationData
     * @param string $serviceFieldInfo
     * @param string $serviceMark
     * @param string $damageInformation
     * @param string $parcelNoCsd
     */
    public function __construct($shipmentId = null, $parcelId = null, $senderCustName = null, $senderAddress = null, $senderZipCode = null, $senderCountryAlphaCode = null, $senderTaxNumber = null, $senderTelNumber = null, $senderCity = null, $senderContact = null, $companyName = null, $companyTaxNumber = null, $companyAddress = null, $companyTelNumber = null, $companyFaxNumber = null, $barcode = null, $barcodeText = null, $shipmentReferenceNumber = null, $parcelIndex = null, $parcelCount = null, $receiverName = null, $receiverAddress1 = null, $receiverAddress2 = null, $receiverZipCode = null, $receiverCity = null, $receiverCountryCodeAlpha = null, $receiverTaxNumber = null, $extraInformationOnLabel = null, $departureUnitId = null, $deliveryUnitId = null, $serviceCode = null, $serviceCombination = null, $routeDsort = null, $routeOsort = null, $receiverFirmName = null, $receiverContactName = null, $receiverPhoneNr1 = null, $receiverPhoneNr2 = null, $receiverAccessCode = null, $codAmountOfValue = null, $codCurrency = null, $codReferenceNo = null, $codPaymentType = null, $parcelNo = null, $weight = null, $volume = null, $parcelReferenceNumber = null, $additionalLabelFlag = null, $additionalInformationData = null, $serviceFieldInfo = null, $serviceMark = null, $damageInformation = null, $parcelNoCsd = null)
    {
        $this
            ->setShipmentId($shipmentId)
            ->setParcelId($parcelId)
            ->setSenderCustName($senderCustName)
            ->setSenderAddress($senderAddress)
            ->setSenderZipCode($senderZipCode)
            ->setSenderCountryAlphaCode($senderCountryAlphaCode)
            ->setSenderTaxNumber($senderTaxNumber)
            ->setSenderTelNumber($senderTelNumber)
            ->setSenderCity($senderCity)
            ->setSenderContact($senderContact)
            ->setCompanyName($companyName)
            ->setCompanyTaxNumber($companyTaxNumber)
            ->setCompanyAddress($companyAddress)
            ->setCompanyTelNumber($companyTelNumber)
            ->setCompanyFaxNumber($companyFaxNumber)
            ->setBarcode($barcode)
            ->setBarcodeText($barcodeText)
            ->setShipmentReferenceNumber($shipmentReferenceNumber)
            ->setParcelIndex($parcelIndex)
            ->setParcelCount($parcelCount)
            ->setReceiverName($receiverName)
            ->setReceiverAddress1($receiverAddress1)
            ->setReceiverAddress2($receiverAddress2)
            ->setReceiverZipCode($receiverZipCode)
            ->setReceiverCity($receiverCity)
            ->setReceiverCountryCodeAlpha($receiverCountryCodeAlpha)
            ->setReceiverTaxNumber($receiverTaxNumber)
            ->setExtraInformationOnLabel($extraInformationOnLabel)
            ->setDepartureUnitId($departureUnitId)
            ->setDeliveryUnitId($deliveryUnitId)
            ->setServiceCode($serviceCode)
            ->setServiceCombination($serviceCombination)
            ->setRouteDsort($routeDsort)
            ->setRouteOsort($routeOsort)
            ->setReceiverFirmName($receiverFirmName)
            ->setReceiverContactName($receiverContactName)
            ->setReceiverPhoneNr1($receiverPhoneNr1)
            ->setReceiverPhoneNr2($receiverPhoneNr2)
            ->setReceiverAccessCode($receiverAccessCode)
            ->setCodAmountOfValue($codAmountOfValue)
            ->setCodCurrency($codCurrency)
            ->setCodReferenceNo($codReferenceNo)
            ->setCodPaymentType($codPaymentType)
            ->setParcelNo($parcelNo)
            ->setWeight($weight)
            ->setVolume($volume)
            ->setParcelReferenceNumber($parcelReferenceNumber)
            ->setAdditionalLabelFlag($additionalLabelFlag)
            ->setAdditionalInformationData($additionalInformationData)
            ->setServiceFieldInfo($serviceFieldInfo)
            ->setServiceMark($serviceMark)
            ->setDamageInformation($damageInformation)
            ->setParcelNoCsd($parcelNoCsd);
    }
    /**
     * Get shipmentId value
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }
    /**
     * Set shipmentId value
     * @param int $shipmentId
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setShipmentId($shipmentId = null)
    {
        // validation for constraint: int
        if (!is_null($shipmentId) && !(is_int($shipmentId) || ctype_digit($shipmentId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($shipmentId, true), gettype($shipmentId)), __LINE__);
        }
        $this->shipmentId = $shipmentId;
        return $this;
    }
    /**
     * Get parcelId value
     * @return int|null
     */
    public function getParcelId()
    {
        return $this->parcelId;
    }
    /**
     * Set parcelId value
     * @param int $parcelId
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelId($parcelId = null)
    {
        // validation for constraint: int
        if (!is_null($parcelId) && !(is_int($parcelId) || ctype_digit($parcelId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($parcelId, true), gettype($parcelId)), __LINE__);
        }
        $this->parcelId = $parcelId;
        return $this;
    }
    /**
     * Get senderCustName value
     * @return string|null
     */
    public function getSenderCustName()
    {
        return $this->senderCustName;
    }
    /**
     * Set senderCustName value
     * @param string $senderCustName
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderCustName($senderCustName = null)
    {
        // validation for constraint: string
        if (!is_null($senderCustName) && !is_string($senderCustName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderCustName, true), gettype($senderCustName)), __LINE__);
        }
        $this->senderCustName = $senderCustName;
        return $this;
    }
    /**
     * Get senderAddress value
     * @return string|null
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }
    /**
     * Set senderAddress value
     * @param string $senderAddress
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderAddress($senderAddress = null)
    {
        // validation for constraint: string
        if (!is_null($senderAddress) && !is_string($senderAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderAddress, true), gettype($senderAddress)), __LINE__);
        }
        $this->senderAddress = $senderAddress;
        return $this;
    }
    /**
     * Get senderZipCode value
     * @return string|null
     */
    public function getSenderZipCode()
    {
        return $this->senderZipCode;
    }
    /**
     * Set senderZipCode value
     * @param string $senderZipCode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderZipCode($senderZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($senderZipCode) && !is_string($senderZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderZipCode, true), gettype($senderZipCode)), __LINE__);
        }
        $this->senderZipCode = $senderZipCode;
        return $this;
    }
    /**
     * Get senderCountryAlphaCode value
     * @return string|null
     */
    public function getSenderCountryAlphaCode()
    {
        return $this->senderCountryAlphaCode;
    }
    /**
     * Set senderCountryAlphaCode value
     * @param string $senderCountryAlphaCode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderCountryAlphaCode($senderCountryAlphaCode = null)
    {
        // validation for constraint: string
        if (!is_null($senderCountryAlphaCode) && !is_string($senderCountryAlphaCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderCountryAlphaCode, true), gettype($senderCountryAlphaCode)), __LINE__);
        }
        $this->senderCountryAlphaCode = $senderCountryAlphaCode;
        return $this;
    }
    /**
     * Get senderTaxNumber value
     * @return string|null
     */
    public function getSenderTaxNumber()
    {
        return $this->senderTaxNumber;
    }
    /**
     * Set senderTaxNumber value
     * @param string $senderTaxNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderTaxNumber($senderTaxNumber = null)
    {
        // validation for constraint: string
        if (!is_null($senderTaxNumber) && !is_string($senderTaxNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderTaxNumber, true), gettype($senderTaxNumber)), __LINE__);
        }
        $this->senderTaxNumber = $senderTaxNumber;
        return $this;
    }
    /**
     * Get senderTelNumber value
     * @return string|null
     */
    public function getSenderTelNumber()
    {
        return $this->senderTelNumber;
    }
    /**
     * Set senderTelNumber value
     * @param string $senderTelNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderTelNumber($senderTelNumber = null)
    {
        // validation for constraint: string
        if (!is_null($senderTelNumber) && !is_string($senderTelNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderTelNumber, true), gettype($senderTelNumber)), __LINE__);
        }
        $this->senderTelNumber = $senderTelNumber;
        return $this;
    }
    /**
     * Get senderCity value
     * @return string|null
     */
    public function getSenderCity()
    {
        return $this->senderCity;
    }
    /**
     * Set senderCity value
     * @param string $senderCity
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderCity($senderCity = null)
    {
        // validation for constraint: string
        if (!is_null($senderCity) && !is_string($senderCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderCity, true), gettype($senderCity)), __LINE__);
        }
        $this->senderCity = $senderCity;
        return $this;
    }
    /**
     * Get senderContact value
     * @return string|null
     */
    public function getSenderContact()
    {
        return $this->senderContact;
    }
    /**
     * Set senderContact value
     * @param string $senderContact
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setSenderContact($senderContact = null)
    {
        // validation for constraint: string
        if (!is_null($senderContact) && !is_string($senderContact)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($senderContact, true), gettype($senderContact)), __LINE__);
        }
        $this->senderContact = $senderContact;
        return $this;
    }
    /**
     * Get companyName value
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    /**
     * Set companyName value
     * @param string $companyName
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCompanyName($companyName = null)
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyName, true), gettype($companyName)), __LINE__);
        }
        $this->companyName = $companyName;
        return $this;
    }
    /**
     * Get companyTaxNumber value
     * @return string|null
     */
    public function getCompanyTaxNumber()
    {
        return $this->companyTaxNumber;
    }
    /**
     * Set companyTaxNumber value
     * @param string $companyTaxNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCompanyTaxNumber($companyTaxNumber = null)
    {
        // validation for constraint: string
        if (!is_null($companyTaxNumber) && !is_string($companyTaxNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyTaxNumber, true), gettype($companyTaxNumber)), __LINE__);
        }
        $this->companyTaxNumber = $companyTaxNumber;
        return $this;
    }
    /**
     * Get companyAddress value
     * @return string|null
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }
    /**
     * Set companyAddress value
     * @param string $companyAddress
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCompanyAddress($companyAddress = null)
    {
        // validation for constraint: string
        if (!is_null($companyAddress) && !is_string($companyAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyAddress, true), gettype($companyAddress)), __LINE__);
        }
        $this->companyAddress = $companyAddress;
        return $this;
    }
    /**
     * Get companyTelNumber value
     * @return string|null
     */
    public function getCompanyTelNumber()
    {
        return $this->companyTelNumber;
    }
    /**
     * Set companyTelNumber value
     * @param string $companyTelNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCompanyTelNumber($companyTelNumber = null)
    {
        // validation for constraint: string
        if (!is_null($companyTelNumber) && !is_string($companyTelNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyTelNumber, true), gettype($companyTelNumber)), __LINE__);
        }
        $this->companyTelNumber = $companyTelNumber;
        return $this;
    }
    /**
     * Get companyFaxNumber value
     * @return string|null
     */
    public function getCompanyFaxNumber()
    {
        return $this->companyFaxNumber;
    }
    /**
     * Set companyFaxNumber value
     * @param string $companyFaxNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCompanyFaxNumber($companyFaxNumber = null)
    {
        // validation for constraint: string
        if (!is_null($companyFaxNumber) && !is_string($companyFaxNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyFaxNumber, true), gettype($companyFaxNumber)), __LINE__);
        }
        $this->companyFaxNumber = $companyFaxNumber;
        return $this;
    }
    /**
     * Get barcode value
     * @return string|null
     */
    public function getBarcode()
    {
        return $this->barcode;
    }
    /**
     * Set barcode value
     * @param string $barcode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setBarcode($barcode = null)
    {
        // validation for constraint: string
        if (!is_null($barcode) && !is_string($barcode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($barcode, true), gettype($barcode)), __LINE__);
        }
        $this->barcode = $barcode;
        return $this;
    }
    /**
     * Get barcodeText value
     * @return string|null
     */
    public function getBarcodeText()
    {
        return $this->barcodeText;
    }
    /**
     * Set barcodeText value
     * @param string $barcodeText
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setBarcodeText($barcodeText = null)
    {
        // validation for constraint: string
        if (!is_null($barcodeText) && !is_string($barcodeText)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($barcodeText, true), gettype($barcodeText)), __LINE__);
        }
        $this->barcodeText = $barcodeText;
        return $this;
    }
    /**
     * Get shipmentReferenceNumber value
     * @return string|null
     */
    public function getShipmentReferenceNumber()
    {
        return $this->shipmentReferenceNumber;
    }
    /**
     * Set shipmentReferenceNumber value
     * @param string $shipmentReferenceNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setShipmentReferenceNumber($shipmentReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber) && !is_string($shipmentReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber, true), gettype($shipmentReferenceNumber)), __LINE__);
        }
        $this->shipmentReferenceNumber = $shipmentReferenceNumber;
        return $this;
    }
    /**
     * Get parcelIndex value
     * @return int|null
     */
    public function getParcelIndex()
    {
        return $this->parcelIndex;
    }
    /**
     * Set parcelIndex value
     * @param int $parcelIndex
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelIndex($parcelIndex = null)
    {
        // validation for constraint: int
        if (!is_null($parcelIndex) && !(is_int($parcelIndex) || ctype_digit($parcelIndex))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($parcelIndex, true), gettype($parcelIndex)), __LINE__);
        }
        $this->parcelIndex = $parcelIndex;
        return $this;
    }
    /**
     * Get parcelCount value
     * @return int|null
     */
    public function getParcelCount()
    {
        return $this->parcelCount;
    }
    /**
     * Set parcelCount value
     * @param int $parcelCount
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelCount($parcelCount = null)
    {
        // validation for constraint: int
        if (!is_null($parcelCount) && !(is_int($parcelCount) || ctype_digit($parcelCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($parcelCount, true), gettype($parcelCount)), __LINE__);
        }
        $this->parcelCount = $parcelCount;
        return $this;
    }
    /**
     * Get receiverName value
     * @return string|null
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }
    /**
     * Set receiverName value
     * @param string $receiverName
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverName($receiverName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverName) && !is_string($receiverName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverName, true), gettype($receiverName)), __LINE__);
        }
        $this->receiverName = $receiverName;
        return $this;
    }
    /**
     * Get receiverAddress1 value
     * @return string|null
     */
    public function getReceiverAddress1()
    {
        return $this->receiverAddress1;
    }
    /**
     * Set receiverAddress1 value
     * @param string $receiverAddress1
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverAddress1($receiverAddress1 = null)
    {
        // validation for constraint: string
        if (!is_null($receiverAddress1) && !is_string($receiverAddress1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverAddress1, true), gettype($receiverAddress1)), __LINE__);
        }
        $this->receiverAddress1 = $receiverAddress1;
        return $this;
    }
    /**
     * Get receiverAddress2 value
     * @return string|null
     */
    public function getReceiverAddress2()
    {
        return $this->receiverAddress2;
    }
    /**
     * Set receiverAddress2 value
     * @param string $receiverAddress2
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverAddress2($receiverAddress2 = null)
    {
        // validation for constraint: string
        if (!is_null($receiverAddress2) && !is_string($receiverAddress2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverAddress2, true), gettype($receiverAddress2)), __LINE__);
        }
        $this->receiverAddress2 = $receiverAddress2;
        return $this;
    }
    /**
     * Get receiverZipCode value
     * @return string|null
     */
    public function getReceiverZipCode()
    {
        return $this->receiverZipCode;
    }
    /**
     * Set receiverZipCode value
     * @param string $receiverZipCode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverZipCode($receiverZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverZipCode) && !is_string($receiverZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverZipCode, true), gettype($receiverZipCode)), __LINE__);
        }
        $this->receiverZipCode = $receiverZipCode;
        return $this;
    }
    /**
     * Get receiverCity value
     * @return string|null
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }
    /**
     * Set receiverCity value
     * @param string $receiverCity
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverCity($receiverCity = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCity) && !is_string($receiverCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCity, true), gettype($receiverCity)), __LINE__);
        }
        $this->receiverCity = $receiverCity;
        return $this;
    }
    /**
     * Get receiverCountryCodeAlpha value
     * @return string|null
     */
    public function getReceiverCountryCodeAlpha()
    {
        return $this->receiverCountryCodeAlpha;
    }
    /**
     * Set receiverCountryCodeAlpha value
     * @param string $receiverCountryCodeAlpha
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverCountryCodeAlpha($receiverCountryCodeAlpha = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCountryCodeAlpha) && !is_string($receiverCountryCodeAlpha)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCountryCodeAlpha, true), gettype($receiverCountryCodeAlpha)), __LINE__);
        }
        $this->receiverCountryCodeAlpha = $receiverCountryCodeAlpha;
        return $this;
    }
    /**
     * Get receiverTaxNumber value
     * @return string|null
     */
    public function getReceiverTaxNumber()
    {
        return $this->receiverTaxNumber;
    }
    /**
     * Set receiverTaxNumber value
     * @param string $receiverTaxNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverTaxNumber($receiverTaxNumber = null)
    {
        // validation for constraint: string
        if (!is_null($receiverTaxNumber) && !is_string($receiverTaxNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverTaxNumber, true), gettype($receiverTaxNumber)), __LINE__);
        }
        $this->receiverTaxNumber = $receiverTaxNumber;
        return $this;
    }
    /**
     * Get extraInformationOnLabel value
     * @return string|null
     */
    public function getExtraInformationOnLabel()
    {
        return $this->extraInformationOnLabel;
    }
    /**
     * Set extraInformationOnLabel value
     * @param string $extraInformationOnLabel
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setExtraInformationOnLabel($extraInformationOnLabel = null)
    {
        // validation for constraint: string
        if (!is_null($extraInformationOnLabel) && !is_string($extraInformationOnLabel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($extraInformationOnLabel, true), gettype($extraInformationOnLabel)), __LINE__);
        }
        $this->extraInformationOnLabel = $extraInformationOnLabel;
        return $this;
    }
    /**
     * Get departureUnitId value
     * @return string|null
     */
    public function getDepartureUnitId()
    {
        return $this->departureUnitId;
    }
    /**
     * Set departureUnitId value
     * @param string $departureUnitId
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setDepartureUnitId($departureUnitId = null)
    {
        // validation for constraint: string
        if (!is_null($departureUnitId) && !is_string($departureUnitId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($departureUnitId, true), gettype($departureUnitId)), __LINE__);
        }
        $this->departureUnitId = $departureUnitId;
        return $this;
    }
    /**
     * Get deliveryUnitId value
     * @return string|null
     */
    public function getDeliveryUnitId()
    {
        return $this->deliveryUnitId;
    }
    /**
     * Set deliveryUnitId value
     * @param string $deliveryUnitId
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setDeliveryUnitId($deliveryUnitId = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryUnitId) && !is_string($deliveryUnitId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryUnitId, true), gettype($deliveryUnitId)), __LINE__);
        }
        $this->deliveryUnitId = $deliveryUnitId;
        return $this;
    }
    /**
     * Get serviceCode value
     * @return string|null
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }
    /**
     * Set serviceCode value
     * @param string $serviceCode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setServiceCode($serviceCode = null)
    {
        // validation for constraint: string
        if (!is_null($serviceCode) && !is_string($serviceCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceCode, true), gettype($serviceCode)), __LINE__);
        }
        $this->serviceCode = $serviceCode;
        return $this;
    }
    /**
     * Get serviceCombination value
     * @return string|null
     */
    public function getServiceCombination()
    {
        return $this->serviceCombination;
    }
    /**
     * Set serviceCombination value
     * @param string $serviceCombination
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setServiceCombination($serviceCombination = null)
    {
        // validation for constraint: string
        if (!is_null($serviceCombination) && !is_string($serviceCombination)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceCombination, true), gettype($serviceCombination)), __LINE__);
        }
        $this->serviceCombination = $serviceCombination;
        return $this;
    }
    /**
     * Get routeDsort value
     * @return string|null
     */
    public function getRouteDsort()
    {
        return $this->routeDsort;
    }
    /**
     * Set routeDsort value
     * @param string $routeDsort
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setRouteDsort($routeDsort = null)
    {
        // validation for constraint: string
        if (!is_null($routeDsort) && !is_string($routeDsort)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routeDsort, true), gettype($routeDsort)), __LINE__);
        }
        $this->routeDsort = $routeDsort;
        return $this;
    }
    /**
     * Get routeOsort value
     * @return string|null
     */
    public function getRouteOsort()
    {
        return $this->routeOsort;
    }
    /**
     * Set routeOsort value
     * @param string $routeOsort
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setRouteOsort($routeOsort = null)
    {
        // validation for constraint: string
        if (!is_null($routeOsort) && !is_string($routeOsort)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($routeOsort, true), gettype($routeOsort)), __LINE__);
        }
        $this->routeOsort = $routeOsort;
        return $this;
    }
    /**
     * Get receiverFirmName value
     * @return string|null
     */
    public function getReceiverFirmName()
    {
        return $this->receiverFirmName;
    }
    /**
     * Set receiverFirmName value
     * @param string $receiverFirmName
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverFirmName($receiverFirmName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverFirmName) && !is_string($receiverFirmName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverFirmName, true), gettype($receiverFirmName)), __LINE__);
        }
        $this->receiverFirmName = $receiverFirmName;
        return $this;
    }
    /**
     * Get receiverContactName value
     * @return string|null
     */
    public function getReceiverContactName()
    {
        return $this->receiverContactName;
    }
    /**
     * Set receiverContactName value
     * @param string $receiverContactName
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverContactName($receiverContactName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverContactName) && !is_string($receiverContactName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverContactName, true), gettype($receiverContactName)), __LINE__);
        }
        $this->receiverContactName = $receiverContactName;
        return $this;
    }
    /**
     * Get receiverPhoneNr1 value
     * @return string|null
     */
    public function getReceiverPhoneNr1()
    {
        return $this->receiverPhoneNr1;
    }
    /**
     * Set receiverPhoneNr1 value
     * @param string $receiverPhoneNr1
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverPhoneNr1($receiverPhoneNr1 = null)
    {
        // validation for constraint: string
        if (!is_null($receiverPhoneNr1) && !is_string($receiverPhoneNr1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverPhoneNr1, true), gettype($receiverPhoneNr1)), __LINE__);
        }
        $this->receiverPhoneNr1 = $receiverPhoneNr1;
        return $this;
    }
    /**
     * Get receiverPhoneNr2 value
     * @return string|null
     */
    public function getReceiverPhoneNr2()
    {
        return $this->receiverPhoneNr2;
    }
    /**
     * Set receiverPhoneNr2 value
     * @param string $receiverPhoneNr2
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverPhoneNr2($receiverPhoneNr2 = null)
    {
        // validation for constraint: string
        if (!is_null($receiverPhoneNr2) && !is_string($receiverPhoneNr2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverPhoneNr2, true), gettype($receiverPhoneNr2)), __LINE__);
        }
        $this->receiverPhoneNr2 = $receiverPhoneNr2;
        return $this;
    }
    /**
     * Get receiverAccessCode value
     * @return string|null
     */
    public function getReceiverAccessCode()
    {
        return $this->receiverAccessCode;
    }
    /**
     * Set receiverAccessCode value
     * @param string $receiverAccessCode
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setReceiverAccessCode($receiverAccessCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverAccessCode) && !is_string($receiverAccessCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverAccessCode, true), gettype($receiverAccessCode)), __LINE__);
        }
        $this->receiverAccessCode = $receiverAccessCode;
        return $this;
    }
    /**
     * Get codAmountOfValue value
     * @return float|null
     */
    public function getCodAmountOfValue()
    {
        return $this->codAmountOfValue;
    }
    /**
     * Set codAmountOfValue value
     * @param float $codAmountOfValue
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCodAmountOfValue($codAmountOfValue = null)
    {
        // validation for constraint: float
        if (!is_null($codAmountOfValue) && !(is_float($codAmountOfValue) || is_numeric($codAmountOfValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($codAmountOfValue, true), gettype($codAmountOfValue)), __LINE__);
        }
        $this->codAmountOfValue = $codAmountOfValue;
        return $this;
    }
    /**
     * Get codCurrency value
     * @return string|null
     */
    public function getCodCurrency()
    {
        return $this->codCurrency;
    }
    /**
     * Set codCurrency value
     * @param string $codCurrency
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCodCurrency($codCurrency = null)
    {
        // validation for constraint: string
        if (!is_null($codCurrency) && !is_string($codCurrency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codCurrency, true), gettype($codCurrency)), __LINE__);
        }
        $this->codCurrency = $codCurrency;
        return $this;
    }
    /**
     * Get codReferenceNo value
     * @return string|null
     */
    public function getCodReferenceNo()
    {
        return $this->codReferenceNo;
    }
    /**
     * Set codReferenceNo value
     * @param string $codReferenceNo
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCodReferenceNo($codReferenceNo = null)
    {
        // validation for constraint: string
        if (!is_null($codReferenceNo) && !is_string($codReferenceNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codReferenceNo, true), gettype($codReferenceNo)), __LINE__);
        }
        $this->codReferenceNo = $codReferenceNo;
        return $this;
    }
    /**
     * Get codPaymentType value
     * @return string|null
     */
    public function getCodPaymentType()
    {
        return $this->codPaymentType;
    }
    /**
     * Set codPaymentType value
     * @param string $codPaymentType
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setCodPaymentType($codPaymentType = null)
    {
        // validation for constraint: string
        if (!is_null($codPaymentType) && !is_string($codPaymentType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codPaymentType, true), gettype($codPaymentType)), __LINE__);
        }
        $this->codPaymentType = $codPaymentType;
        return $this;
    }
    /**
     * Get parcelNo value
     * @return string|null
     */
    public function getParcelNo()
    {
        return $this->parcelNo;
    }
    /**
     * Set parcelNo value
     * @param string $parcelNo
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelNo($parcelNo = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNo) && !is_string($parcelNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNo, true), gettype($parcelNo)), __LINE__);
        }
        $this->parcelNo = $parcelNo;
        return $this;
    }
    /**
     * Get weight value
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * Set weight value
     * @param float $weight
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        $this->weight = $weight;
        return $this;
    }
    /**
     * Get volume value
     * @return float|null
     */
    public function getVolume()
    {
        return $this->volume;
    }
    /**
     * Set volume value
     * @param float $volume
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setVolume($volume = null)
    {
        // validation for constraint: float
        if (!is_null($volume) && !(is_float($volume) || is_numeric($volume))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($volume, true), gettype($volume)), __LINE__);
        }
        $this->volume = $volume;
        return $this;
    }
    /**
     * Get parcelReferenceNumber value
     * @return string|null
     */
    public function getParcelReferenceNumber()
    {
        return $this->parcelReferenceNumber;
    }
    /**
     * Set parcelReferenceNumber value
     * @param string $parcelReferenceNumber
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelReferenceNumber($parcelReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber) && !is_string($parcelReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber, true), gettype($parcelReferenceNumber)), __LINE__);
        }
        $this->parcelReferenceNumber = $parcelReferenceNumber;
        return $this;
    }
    /**
     * Get additionalLabelFlag value
     * @return string|null
     */
    public function getAdditionalLabelFlag()
    {
        return $this->additionalLabelFlag;
    }
    /**
     * Set additionalLabelFlag value
     * @param string $additionalLabelFlag
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setAdditionalLabelFlag($additionalLabelFlag = null)
    {
        // validation for constraint: string
        if (!is_null($additionalLabelFlag) && !is_string($additionalLabelFlag)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalLabelFlag, true), gettype($additionalLabelFlag)), __LINE__);
        }
        $this->additionalLabelFlag = $additionalLabelFlag;
        return $this;
    }
    /**
     * Get additionalInformationData value
     * @return string|null
     */
    public function getAdditionalInformationData()
    {
        return $this->additionalInformationData;
    }
    /**
     * Set additionalInformationData value
     * @param string $additionalInformationData
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setAdditionalInformationData($additionalInformationData = null)
    {
        // validation for constraint: string
        if (!is_null($additionalInformationData) && !is_string($additionalInformationData)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInformationData, true), gettype($additionalInformationData)), __LINE__);
        }
        $this->additionalInformationData = $additionalInformationData;
        return $this;
    }
    /**
     * Get serviceFieldInfo value
     * @return string|null
     */
    public function getServiceFieldInfo()
    {
        return $this->serviceFieldInfo;
    }
    /**
     * Set serviceFieldInfo value
     * @param string $serviceFieldInfo
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setServiceFieldInfo($serviceFieldInfo = null)
    {
        // validation for constraint: string
        if (!is_null($serviceFieldInfo) && !is_string($serviceFieldInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceFieldInfo, true), gettype($serviceFieldInfo)), __LINE__);
        }
        $this->serviceFieldInfo = $serviceFieldInfo;
        return $this;
    }
    /**
     * Get serviceMark value
     * @return string|null
     */
    public function getServiceMark()
    {
        return $this->serviceMark;
    }
    /**
     * Set serviceMark value
     * @param string $serviceMark
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setServiceMark($serviceMark = null)
    {
        // validation for constraint: string
        if (!is_null($serviceMark) && !is_string($serviceMark)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceMark, true), gettype($serviceMark)), __LINE__);
        }
        $this->serviceMark = $serviceMark;
        return $this;
    }
    /**
     * Get damageInformation value
     * @return string|null
     */
    public function getDamageInformation()
    {
        return $this->damageInformation;
    }
    /**
     * Set damageInformation value
     * @param string $damageInformation
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setDamageInformation($damageInformation = null)
    {
        // validation for constraint: string
        if (!is_null($damageInformation) && !is_string($damageInformation)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($damageInformation, true), gettype($damageInformation)), __LINE__);
        }
        $this->damageInformation = $damageInformation;
        return $this;
    }
    /**
     * Get parcelNoCsd value
     * @return string|null
     */
    public function getParcelNoCsd()
    {
        return $this->parcelNoCsd;
    }
    /**
     * Set parcelNoCsd value
     * @param string $parcelNoCsd
     * @return \DPDSDK\Shipment\StructType\PrintDataVO
     */
    public function setParcelNoCsd($parcelNoCsd = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNoCsd) && !is_string($parcelNoCsd)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNoCsd, true), gettype($parcelNoCsd)), __LINE__);
        }
        $this->parcelNoCsd = $parcelNoCsd;
        return $this;
    }
}
