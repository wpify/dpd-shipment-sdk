<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StatusResponseVO StructType
 * @subpackage Structs
 */
class StatusResponseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The statusInfoList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\StatusResultVO[]
     */
    public $statusInfoList;
    /**
     * Constructor method for StatusResponseVO
     * @uses StatusResponseVO::setTransactionId()
     * @uses StatusResponseVO::setStatusInfoList()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\StatusResultVO[] $statusInfoList
     */
    public function __construct($transactionId = null, array $statusInfoList = array())
    {
        $this
            ->setTransactionId($transactionId)
            ->setStatusInfoList($statusInfoList);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\StatusResponseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get statusInfoList value
     * @return \DPDSDK\Shipment\StructType\StatusResultVO[]|null
     */
    public function getStatusInfoList()
    {
        return $this->statusInfoList;
    }
    /**
     * This method is responsible for validating the values passed to the setStatusInfoList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatusInfoList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusInfoListForArrayConstraintsFromSetStatusInfoList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $statusResponseVOStatusInfoListItem) {
            // validation for constraint: itemType
            if (!$statusResponseVOStatusInfoListItem instanceof \DPDSDK\Shipment\StructType\StatusResultVO) {
                $invalidValues[] = is_object($statusResponseVOStatusInfoListItem) ? get_class($statusResponseVOStatusInfoListItem) : sprintf('%s(%s)', gettype($statusResponseVOStatusInfoListItem), var_export($statusResponseVOStatusInfoListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The statusInfoList property can only contain items of type \DPDSDK\Shipment\StructType\StatusResultVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set statusInfoList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\StatusResultVO[] $statusInfoList
     * @return \DPDSDK\Shipment\StructType\StatusResponseVO
     */
    public function setStatusInfoList(array $statusInfoList = array())
    {
        // validation for constraint: array
        if ('' !== ($statusInfoListArrayErrorMessage = self::validateStatusInfoListForArrayConstraintsFromSetStatusInfoList($statusInfoList))) {
            throw new \InvalidArgumentException($statusInfoListArrayErrorMessage, __LINE__);
        }
        $this->statusInfoList = $statusInfoList;
        return $this;
    }
    /**
     * Add item to statusInfoList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\StatusResultVO $item
     * @return \DPDSDK\Shipment\StructType\StatusResponseVO
     */
    public function addToStatusInfoList(\DPDSDK\Shipment\StructType\StatusResultVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\StatusResultVO) {
            throw new \InvalidArgumentException(sprintf('The statusInfoList property can only contain items of type \DPDSDK\Shipment\StructType\StatusResultVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->statusInfoList[] = $item;
        return $this;
    }
}
