<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for updateShipmentResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:updateShipmentResponse
 * @subpackage Structs
 */
class UpdateShipmentResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentRepsonseVO
     */
    public $result;
    /**
     * Constructor method for updateShipmentResponse
     * @uses UpdateShipmentResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ShipmentRepsonseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ShipmentRepsonseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ShipmentRepsonseVO $result
     * @return \DPDSDK\Shipment\StructType\UpdateShipmentResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ShipmentRepsonseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
