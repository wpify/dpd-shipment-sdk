<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelDetailsVO StructType
 * @subpackage Structs
 */
class ParcelDetailsVO extends AbstractStructBase
{
    /**
     * The parcelId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelId;
    /**
     * The parcelNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelNo;
    /**
     * The shpId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shpId;
    /**
     * Constructor method for ParcelDetailsVO
     * @uses ParcelDetailsVO::setParcelId()
     * @uses ParcelDetailsVO::setParcelNo()
     * @uses ParcelDetailsVO::setShpId()
     * @param string $parcelId
     * @param string $parcelNo
     * @param string $shpId
     */
    public function __construct($parcelId = null, $parcelNo = null, $shpId = null)
    {
        $this
            ->setParcelId($parcelId)
            ->setParcelNo($parcelNo)
            ->setShpId($shpId);
    }
    /**
     * Get parcelId value
     * @return string|null
     */
    public function getParcelId()
    {
        return $this->parcelId;
    }
    /**
     * Set parcelId value
     * @param string $parcelId
     * @return \DPDSDK\Shipment\StructType\ParcelDetailsVO
     */
    public function setParcelId($parcelId = null)
    {
        // validation for constraint: string
        if (!is_null($parcelId) && !is_string($parcelId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelId, true), gettype($parcelId)), __LINE__);
        }
        $this->parcelId = $parcelId;
        return $this;
    }
    /**
     * Get parcelNo value
     * @return string|null
     */
    public function getParcelNo()
    {
        return $this->parcelNo;
    }
    /**
     * Set parcelNo value
     * @param string $parcelNo
     * @return \DPDSDK\Shipment\StructType\ParcelDetailsVO
     */
    public function setParcelNo($parcelNo = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNo) && !is_string($parcelNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNo, true), gettype($parcelNo)), __LINE__);
        }
        $this->parcelNo = $parcelNo;
        return $this;
    }
    /**
     * Get shpId value
     * @return string|null
     */
    public function getShpId()
    {
        return $this->shpId;
    }
    /**
     * Set shpId value
     * @param string $shpId
     * @return \DPDSDK\Shipment\StructType\ParcelDetailsVO
     */
    public function setShpId($shpId = null)
    {
        // validation for constraint: string
        if (!is_null($shpId) && !is_string($shpId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shpId, true), gettype($shpId)), __LINE__);
        }
        $this->shpId = $shpId;
        return $this;
    }
}
