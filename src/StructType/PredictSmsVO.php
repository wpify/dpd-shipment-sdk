<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PredictSmsVO StructType
 * @subpackage Structs
 */
class PredictSmsVO extends AbstractStructBase
{
    /**
     * The telephoneNr
     * @var string
     */
    public $telephoneNr;
    /**
     * Constructor method for PredictSmsVO
     * @uses PredictSmsVO::setTelephoneNr()
     * @param string $telephoneNr
     */
    public function __construct($telephoneNr = null)
    {
        $this
            ->setTelephoneNr($telephoneNr);
    }
    /**
     * Get telephoneNr value
     * @return string|null
     */
    public function getTelephoneNr()
    {
        return $this->telephoneNr;
    }
    /**
     * Set telephoneNr value
     * @param string $telephoneNr
     * @return \DPDSDK\Shipment\StructType\PredictSmsVO
     */
    public function setTelephoneNr($telephoneNr = null)
    {
        // validation for constraint: string
        if (!is_null($telephoneNr) && !is_string($telephoneNr)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telephoneNr, true), gettype($telephoneNr)), __LINE__);
        }
        $this->telephoneNr = $telephoneNr;
        return $this;
    }
}
