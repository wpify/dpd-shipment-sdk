<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopOpeningHoursVO StructType
 * @subpackage Structs
 */
class ParcelShopOpeningHoursVO extends AbstractStructBase
{
    /**
     * The weekday
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $weekday;
    /**
     * The openMorning
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $openMorning;
    /**
     * The closeMorning
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $closeMorning;
    /**
     * The closeAfternoon
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $closeAfternoon;
    /**
     * The openAfternoon
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $openAfternoon;
    /**
     * Constructor method for ParcelShopOpeningHoursVO
     * @uses ParcelShopOpeningHoursVO::setWeekday()
     * @uses ParcelShopOpeningHoursVO::setOpenMorning()
     * @uses ParcelShopOpeningHoursVO::setCloseMorning()
     * @uses ParcelShopOpeningHoursVO::setCloseAfternoon()
     * @uses ParcelShopOpeningHoursVO::setOpenAfternoon()
     * @param string $weekday
     * @param string $openMorning
     * @param string $closeMorning
     * @param string $closeAfternoon
     * @param string $openAfternoon
     */
    public function __construct($weekday = null, $openMorning = null, $closeMorning = null, $closeAfternoon = null, $openAfternoon = null)
    {
        $this
            ->setWeekday($weekday)
            ->setOpenMorning($openMorning)
            ->setCloseMorning($closeMorning)
            ->setCloseAfternoon($closeAfternoon)
            ->setOpenAfternoon($openAfternoon);
    }
    /**
     * Get weekday value
     * @return string|null
     */
    public function getWeekday()
    {
        return $this->weekday;
    }
    /**
     * Set weekday value
     * @param string $weekday
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO
     */
    public function setWeekday($weekday = null)
    {
        // validation for constraint: string
        if (!is_null($weekday) && !is_string($weekday)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($weekday, true), gettype($weekday)), __LINE__);
        }
        $this->weekday = $weekday;
        return $this;
    }
    /**
     * Get openMorning value
     * @return string|null
     */
    public function getOpenMorning()
    {
        return $this->openMorning;
    }
    /**
     * Set openMorning value
     * @param string $openMorning
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO
     */
    public function setOpenMorning($openMorning = null)
    {
        // validation for constraint: string
        if (!is_null($openMorning) && !is_string($openMorning)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($openMorning, true), gettype($openMorning)), __LINE__);
        }
        $this->openMorning = $openMorning;
        return $this;
    }
    /**
     * Get closeMorning value
     * @return string|null
     */
    public function getCloseMorning()
    {
        return $this->closeMorning;
    }
    /**
     * Set closeMorning value
     * @param string $closeMorning
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO
     */
    public function setCloseMorning($closeMorning = null)
    {
        // validation for constraint: string
        if (!is_null($closeMorning) && !is_string($closeMorning)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($closeMorning, true), gettype($closeMorning)), __LINE__);
        }
        $this->closeMorning = $closeMorning;
        return $this;
    }
    /**
     * Get closeAfternoon value
     * @return string|null
     */
    public function getCloseAfternoon()
    {
        return $this->closeAfternoon;
    }
    /**
     * Set closeAfternoon value
     * @param string $closeAfternoon
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO
     */
    public function setCloseAfternoon($closeAfternoon = null)
    {
        // validation for constraint: string
        if (!is_null($closeAfternoon) && !is_string($closeAfternoon)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($closeAfternoon, true), gettype($closeAfternoon)), __LINE__);
        }
        $this->closeAfternoon = $closeAfternoon;
        return $this;
    }
    /**
     * Get openAfternoon value
     * @return string|null
     */
    public function getOpenAfternoon()
    {
        return $this->openAfternoon;
    }
    /**
     * Set openAfternoon value
     * @param string $openAfternoon
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO
     */
    public function setOpenAfternoon($openAfternoon = null)
    {
        // validation for constraint: string
        if (!is_null($openAfternoon) && !is_string($openAfternoon)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($openAfternoon, true), gettype($openAfternoon)), __LINE__);
        }
        $this->openAfternoon = $openAfternoon;
        return $this;
    }
}
