<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentVOWL StructType
 * @subpackage Structs
 */
class ShipmentVOWL extends AbstractStructBase
{
    /**
     * The shipmentReferenceNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber;
    /**
     * The shipmentReferenceNumber1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber1;
    /**
     * The shipmentReferenceNumber2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber2;
    /**
     * The shipmentReferenceNumber3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber3;
    /**
     * The payerId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $payerId;
    /**
     * The senderAddressId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $senderAddressId;
    /**
     * The receiverName
     * @var string
     */
    public $receiverName;
    /**
     * The receiverFirmName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverFirmName;
    /**
     * The receiverCountryCode
     * @var string
     */
    public $receiverCountryCode;
    /**
     * The receiverEmail
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverEmail;
    /**
     * The receiverZipCode
     * @var string
     */
    public $receiverZipCode;
    /**
     * The receiverCity
     * @var string
     */
    public $receiverCity;
    /**
     * The receiverStreet
     * @var string
     */
    public $receiverStreet;
    /**
     * The receiverHouseNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverHouseNo;
    /**
     * The receiverPhoneNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverPhoneNo;
    /**
     * The mainServiceCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $mainServiceCode;
    /**
     * The exchNumOfParcel
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $exchNumOfParcel;
    /**
     * The parcelType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelType;
    /**
     * The parcelCodType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelCodType;
    /**
     * The additionalInfo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $additionalInfo;
    /**
     * The shipmentGroupRef
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipmentGroupRef;
    /**
     * The additionalServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\AdditionalServiceVOWL
     */
    public $additionalServices;
    /**
     * The parcels
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \DPDSDK\Shipment\StructType\ParcelVO[]
     */
    public $parcels;
    /**
     * Constructor method for ShipmentVOWL
     * @uses ShipmentVOWL::setShipmentReferenceNumber()
     * @uses ShipmentVOWL::setShipmentReferenceNumber1()
     * @uses ShipmentVOWL::setShipmentReferenceNumber2()
     * @uses ShipmentVOWL::setShipmentReferenceNumber3()
     * @uses ShipmentVOWL::setPayerId()
     * @uses ShipmentVOWL::setSenderAddressId()
     * @uses ShipmentVOWL::setReceiverName()
     * @uses ShipmentVOWL::setReceiverFirmName()
     * @uses ShipmentVOWL::setReceiverCountryCode()
     * @uses ShipmentVOWL::setReceiverEmail()
     * @uses ShipmentVOWL::setReceiverZipCode()
     * @uses ShipmentVOWL::setReceiverCity()
     * @uses ShipmentVOWL::setReceiverStreet()
     * @uses ShipmentVOWL::setReceiverHouseNo()
     * @uses ShipmentVOWL::setReceiverPhoneNo()
     * @uses ShipmentVOWL::setMainServiceCode()
     * @uses ShipmentVOWL::setExchNumOfParcel()
     * @uses ShipmentVOWL::setParcelType()
     * @uses ShipmentVOWL::setParcelCodType()
     * @uses ShipmentVOWL::setAdditionalInfo()
     * @uses ShipmentVOWL::setShipmentGroupRef()
     * @uses ShipmentVOWL::setAdditionalServices()
     * @uses ShipmentVOWL::setParcels()
     * @param string $shipmentReferenceNumber
     * @param string $shipmentReferenceNumber1
     * @param string $shipmentReferenceNumber2
     * @param string $shipmentReferenceNumber3
     * @param int $payerId
     * @param int $senderAddressId
     * @param string $receiverName
     * @param string $receiverFirmName
     * @param string $receiverCountryCode
     * @param string $receiverEmail
     * @param string $receiverZipCode
     * @param string $receiverCity
     * @param string $receiverStreet
     * @param string $receiverHouseNo
     * @param string $receiverPhoneNo
     * @param string $mainServiceCode
     * @param int $exchNumOfParcel
     * @param string $parcelType
     * @param string $parcelCodType
     * @param string $additionalInfo
     * @param string $shipmentGroupRef
     * @param \DPDSDK\Shipment\StructType\AdditionalServiceVOWL $additionalServices
     * @param \DPDSDK\Shipment\StructType\ParcelVO[] $parcels
     */
    public function __construct($shipmentReferenceNumber = null, $shipmentReferenceNumber1 = null, $shipmentReferenceNumber2 = null, $shipmentReferenceNumber3 = null, $payerId = null, $senderAddressId = null, $receiverName = null, $receiverFirmName = null, $receiverCountryCode = null, $receiverEmail = null, $receiverZipCode = null, $receiverCity = null, $receiverStreet = null, $receiverHouseNo = null, $receiverPhoneNo = null, $mainServiceCode = null, $exchNumOfParcel = null, $parcelType = null, $parcelCodType = null, $additionalInfo = null, $shipmentGroupRef = null, \DPDSDK\Shipment\StructType\AdditionalServiceVOWL $additionalServices = null, array $parcels = array())
    {
        $this
            ->setShipmentReferenceNumber($shipmentReferenceNumber)
            ->setShipmentReferenceNumber1($shipmentReferenceNumber1)
            ->setShipmentReferenceNumber2($shipmentReferenceNumber2)
            ->setShipmentReferenceNumber3($shipmentReferenceNumber3)
            ->setPayerId($payerId)
            ->setSenderAddressId($senderAddressId)
            ->setReceiverName($receiverName)
            ->setReceiverFirmName($receiverFirmName)
            ->setReceiverCountryCode($receiverCountryCode)
            ->setReceiverEmail($receiverEmail)
            ->setReceiverZipCode($receiverZipCode)
            ->setReceiverCity($receiverCity)
            ->setReceiverStreet($receiverStreet)
            ->setReceiverHouseNo($receiverHouseNo)
            ->setReceiverPhoneNo($receiverPhoneNo)
            ->setMainServiceCode($mainServiceCode)
            ->setExchNumOfParcel($exchNumOfParcel)
            ->setParcelType($parcelType)
            ->setParcelCodType($parcelCodType)
            ->setAdditionalInfo($additionalInfo)
            ->setShipmentGroupRef($shipmentGroupRef)
            ->setAdditionalServices($additionalServices)
            ->setParcels($parcels);
    }
    /**
     * Get shipmentReferenceNumber value
     * @return string|null
     */
    public function getShipmentReferenceNumber()
    {
        return $this->shipmentReferenceNumber;
    }
    /**
     * Set shipmentReferenceNumber value
     * @param string $shipmentReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setShipmentReferenceNumber($shipmentReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber) && !is_string($shipmentReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber, true), gettype($shipmentReferenceNumber)), __LINE__);
        }
        $this->shipmentReferenceNumber = $shipmentReferenceNumber;
        return $this;
    }
    /**
     * Get shipmentReferenceNumber1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber1()
    {
        return isset($this->shipmentReferenceNumber1) ? $this->shipmentReferenceNumber1 : null;
    }
    /**
     * Set shipmentReferenceNumber1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber1
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setShipmentReferenceNumber1($shipmentReferenceNumber1 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber1) && !is_string($shipmentReferenceNumber1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber1, true), gettype($shipmentReferenceNumber1)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber1) || (is_array($shipmentReferenceNumber1) && empty($shipmentReferenceNumber1))) {
            unset($this->shipmentReferenceNumber1);
        } else {
            $this->shipmentReferenceNumber1 = $shipmentReferenceNumber1;
        }
        return $this;
    }
    /**
     * Get shipmentReferenceNumber2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber2()
    {
        return isset($this->shipmentReferenceNumber2) ? $this->shipmentReferenceNumber2 : null;
    }
    /**
     * Set shipmentReferenceNumber2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber2
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setShipmentReferenceNumber2($shipmentReferenceNumber2 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber2) && !is_string($shipmentReferenceNumber2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber2, true), gettype($shipmentReferenceNumber2)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber2) || (is_array($shipmentReferenceNumber2) && empty($shipmentReferenceNumber2))) {
            unset($this->shipmentReferenceNumber2);
        } else {
            $this->shipmentReferenceNumber2 = $shipmentReferenceNumber2;
        }
        return $this;
    }
    /**
     * Get shipmentReferenceNumber3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber3()
    {
        return isset($this->shipmentReferenceNumber3) ? $this->shipmentReferenceNumber3 : null;
    }
    /**
     * Set shipmentReferenceNumber3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber3
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setShipmentReferenceNumber3($shipmentReferenceNumber3 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber3) && !is_string($shipmentReferenceNumber3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber3, true), gettype($shipmentReferenceNumber3)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber3) || (is_array($shipmentReferenceNumber3) && empty($shipmentReferenceNumber3))) {
            unset($this->shipmentReferenceNumber3);
        } else {
            $this->shipmentReferenceNumber3 = $shipmentReferenceNumber3;
        }
        return $this;
    }
    /**
     * Get payerId value
     * @return int|null
     */
    public function getPayerId()
    {
        return $this->payerId;
    }
    /**
     * Set payerId value
     * @param int $payerId
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setPayerId($payerId = null)
    {
        // validation for constraint: int
        if (!is_null($payerId) && !(is_int($payerId) || ctype_digit($payerId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($payerId, true), gettype($payerId)), __LINE__);
        }
        $this->payerId = $payerId;
        return $this;
    }
    /**
     * Get senderAddressId value
     * @return int|null
     */
    public function getSenderAddressId()
    {
        return $this->senderAddressId;
    }
    /**
     * Set senderAddressId value
     * @param int $senderAddressId
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setSenderAddressId($senderAddressId = null)
    {
        // validation for constraint: int
        if (!is_null($senderAddressId) && !(is_int($senderAddressId) || ctype_digit($senderAddressId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($senderAddressId, true), gettype($senderAddressId)), __LINE__);
        }
        $this->senderAddressId = $senderAddressId;
        return $this;
    }
    /**
     * Get receiverName value
     * @return string|null
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }
    /**
     * Set receiverName value
     * @param string $receiverName
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverName($receiverName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverName) && !is_string($receiverName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverName, true), gettype($receiverName)), __LINE__);
        }
        $this->receiverName = $receiverName;
        return $this;
    }
    /**
     * Get receiverFirmName value
     * @return string|null
     */
    public function getReceiverFirmName()
    {
        return $this->receiverFirmName;
    }
    /**
     * Set receiverFirmName value
     * @param string $receiverFirmName
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverFirmName($receiverFirmName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverFirmName) && !is_string($receiverFirmName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverFirmName, true), gettype($receiverFirmName)), __LINE__);
        }
        $this->receiverFirmName = $receiverFirmName;
        return $this;
    }
    /**
     * Get receiverCountryCode value
     * @return string|null
     */
    public function getReceiverCountryCode()
    {
        return $this->receiverCountryCode;
    }
    /**
     * Set receiverCountryCode value
     * @param string $receiverCountryCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverCountryCode($receiverCountryCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCountryCode) && !is_string($receiverCountryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCountryCode, true), gettype($receiverCountryCode)), __LINE__);
        }
        $this->receiverCountryCode = $receiverCountryCode;
        return $this;
    }
    /**
     * Get receiverEmail value
     * @return string|null
     */
    public function getReceiverEmail()
    {
        return $this->receiverEmail;
    }
    /**
     * Set receiverEmail value
     * @param string $receiverEmail
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverEmail($receiverEmail = null)
    {
        // validation for constraint: string
        if (!is_null($receiverEmail) && !is_string($receiverEmail)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverEmail, true), gettype($receiverEmail)), __LINE__);
        }
        $this->receiverEmail = $receiverEmail;
        return $this;
    }
    /**
     * Get receiverZipCode value
     * @return string|null
     */
    public function getReceiverZipCode()
    {
        return $this->receiverZipCode;
    }
    /**
     * Set receiverZipCode value
     * @param string $receiverZipCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverZipCode($receiverZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverZipCode) && !is_string($receiverZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverZipCode, true), gettype($receiverZipCode)), __LINE__);
        }
        $this->receiverZipCode = $receiverZipCode;
        return $this;
    }
    /**
     * Get receiverCity value
     * @return string|null
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }
    /**
     * Set receiverCity value
     * @param string $receiverCity
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverCity($receiverCity = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCity) && !is_string($receiverCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCity, true), gettype($receiverCity)), __LINE__);
        }
        $this->receiverCity = $receiverCity;
        return $this;
    }
    /**
     * Get receiverStreet value
     * @return string|null
     */
    public function getReceiverStreet()
    {
        return $this->receiverStreet;
    }
    /**
     * Set receiverStreet value
     * @param string $receiverStreet
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverStreet($receiverStreet = null)
    {
        // validation for constraint: string
        if (!is_null($receiverStreet) && !is_string($receiverStreet)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverStreet, true), gettype($receiverStreet)), __LINE__);
        }
        $this->receiverStreet = $receiverStreet;
        return $this;
    }
    /**
     * Get receiverHouseNo value
     * @return string|null
     */
    public function getReceiverHouseNo()
    {
        return $this->receiverHouseNo;
    }
    /**
     * Set receiverHouseNo value
     * @param string $receiverHouseNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverHouseNo($receiverHouseNo = null)
    {
        // validation for constraint: string
        if (!is_null($receiverHouseNo) && !is_string($receiverHouseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverHouseNo, true), gettype($receiverHouseNo)), __LINE__);
        }
        $this->receiverHouseNo = $receiverHouseNo;
        return $this;
    }
    /**
     * Get receiverPhoneNo value
     * @return string|null
     */
    public function getReceiverPhoneNo()
    {
        return $this->receiverPhoneNo;
    }
    /**
     * Set receiverPhoneNo value
     * @param string $receiverPhoneNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setReceiverPhoneNo($receiverPhoneNo = null)
    {
        // validation for constraint: string
        if (!is_null($receiverPhoneNo) && !is_string($receiverPhoneNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverPhoneNo, true), gettype($receiverPhoneNo)), __LINE__);
        }
        $this->receiverPhoneNo = $receiverPhoneNo;
        return $this;
    }
    /**
     * Get mainServiceCode value
     * @return string|null
     */
    public function getMainServiceCode()
    {
        return $this->mainServiceCode;
    }
    /**
     * Set mainServiceCode value
     * @param string $mainServiceCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setMainServiceCode($mainServiceCode = null)
    {
        // validation for constraint: string
        if (!is_null($mainServiceCode) && !is_string($mainServiceCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($mainServiceCode, true), gettype($mainServiceCode)), __LINE__);
        }
        $this->mainServiceCode = $mainServiceCode;
        return $this;
    }
    /**
     * Get exchNumOfParcel value
     * @return int|null
     */
    public function getExchNumOfParcel()
    {
        return $this->exchNumOfParcel;
    }
    /**
     * Set exchNumOfParcel value
     * @param int $exchNumOfParcel
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setExchNumOfParcel($exchNumOfParcel = null)
    {
        // validation for constraint: int
        if (!is_null($exchNumOfParcel) && !(is_int($exchNumOfParcel) || ctype_digit($exchNumOfParcel))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($exchNumOfParcel, true), gettype($exchNumOfParcel)), __LINE__);
        }
        $this->exchNumOfParcel = $exchNumOfParcel;
        return $this;
    }
    /**
     * Get parcelType value
     * @return string|null
     */
    public function getParcelType()
    {
        return $this->parcelType;
    }
    /**
     * Set parcelType value
     * @param string $parcelType
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setParcelType($parcelType = null)
    {
        // validation for constraint: string
        if (!is_null($parcelType) && !is_string($parcelType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelType, true), gettype($parcelType)), __LINE__);
        }
        $this->parcelType = $parcelType;
        return $this;
    }
    /**
     * Get parcelCodType value
     * @return string|null
     */
    public function getParcelCodType()
    {
        return $this->parcelCodType;
    }
    /**
     * Set parcelCodType value
     * @param string $parcelCodType
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setParcelCodType($parcelCodType = null)
    {
        // validation for constraint: string
        if (!is_null($parcelCodType) && !is_string($parcelCodType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelCodType, true), gettype($parcelCodType)), __LINE__);
        }
        $this->parcelCodType = $parcelCodType;
        return $this;
    }
    /**
     * Get additionalInfo value
     * @return string|null
     */
    public function getAdditionalInfo()
    {
        return $this->additionalInfo;
    }
    /**
     * Set additionalInfo value
     * @param string $additionalInfo
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setAdditionalInfo($additionalInfo = null)
    {
        // validation for constraint: string
        if (!is_null($additionalInfo) && !is_string($additionalInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($additionalInfo, true), gettype($additionalInfo)), __LINE__);
        }
        $this->additionalInfo = $additionalInfo;
        return $this;
    }
    /**
     * Get shipmentGroupRef value
     * @return string|null
     */
    public function getShipmentGroupRef()
    {
        return $this->shipmentGroupRef;
    }
    /**
     * Set shipmentGroupRef value
     * @param string $shipmentGroupRef
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setShipmentGroupRef($shipmentGroupRef = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentGroupRef) && !is_string($shipmentGroupRef)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentGroupRef, true), gettype($shipmentGroupRef)), __LINE__);
        }
        $this->shipmentGroupRef = $shipmentGroupRef;
        return $this;
    }
    /**
     * Get additionalServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVOWL|null
     */
    public function getAdditionalServices()
    {
        return isset($this->additionalServices) ? $this->additionalServices : null;
    }
    /**
     * Set additionalServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\AdditionalServiceVOWL $additionalServices
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setAdditionalServices(\DPDSDK\Shipment\StructType\AdditionalServiceVOWL $additionalServices = null)
    {
        if (is_null($additionalServices) || (is_array($additionalServices) && empty($additionalServices))) {
            unset($this->additionalServices);
        } else {
            $this->additionalServices = $additionalServices;
        }
        return $this;
    }
    /**
     * Get parcels value
     * @return \DPDSDK\Shipment\StructType\ParcelVO[]|null
     */
    public function getParcels()
    {
        return $this->parcels;
    }
    /**
     * This method is responsible for validating the values passed to the setParcels method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcels method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelsForArrayConstraintsFromSetParcels(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentVOWLParcelsItem) {
            // validation for constraint: itemType
            if (!$shipmentVOWLParcelsItem instanceof \DPDSDK\Shipment\StructType\ParcelVO) {
                $invalidValues[] = is_object($shipmentVOWLParcelsItem) ? get_class($shipmentVOWLParcelsItem) : sprintf('%s(%s)', gettype($shipmentVOWLParcelsItem), var_export($shipmentVOWLParcelsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcels value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelVO[] $parcels
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function setParcels(array $parcels = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelsArrayErrorMessage = self::validateParcelsForArrayConstraintsFromSetParcels($parcels))) {
            throw new \InvalidArgumentException($parcelsArrayErrorMessage, __LINE__);
        }
        $this->parcels = $parcels;
        return $this;
    }
    /**
     * Add item to parcels value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentVOWL
     */
    public function addToParcels(\DPDSDK\Shipment\StructType\ParcelVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelVO) {
            throw new \InvalidArgumentException(sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcels[] = $item;
        return $this;
    }
}
