<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getParcelIdByParcelNumber StructType
 * Meta information extracted from the WSDL
 * - type: tns:getParcelIdByParcelNumber
 * @subpackage Structs
 */
class GetParcelIdByParcelNumber extends AbstractStructBase
{
    /**
     * The wsUserName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsUserName;
    /**
     * The wsPassword
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsPassword;
    /**
     * The secret
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $secret;
    /**
     * The wsLang
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $wsLang;
    /**
     * The applicationType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $applicationType;
    /**
     * The businessApplication
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $businessApplication;
    /**
     * The checkMaster
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var bool
     */
    public $checkMaster;
    /**
     * The parcelReferenceList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $parcelReferenceList;
    /**
     * Constructor method for getParcelIdByParcelNumber
     * @uses GetParcelIdByParcelNumber::setWsUserName()
     * @uses GetParcelIdByParcelNumber::setWsPassword()
     * @uses GetParcelIdByParcelNumber::setSecret()
     * @uses GetParcelIdByParcelNumber::setWsLang()
     * @uses GetParcelIdByParcelNumber::setApplicationType()
     * @uses GetParcelIdByParcelNumber::setBusinessApplication()
     * @uses GetParcelIdByParcelNumber::setCheckMaster()
     * @uses GetParcelIdByParcelNumber::setParcelReferenceList()
     * @param string $wsUserName
     * @param string $wsPassword
     * @param string $secret
     * @param string $wsLang
     * @param string $applicationType
     * @param string $businessApplication
     * @param bool $checkMaster
     * @param string[] $parcelReferenceList
     */
    public function __construct($wsUserName = null, $wsPassword = null, $secret = null, $wsLang = null, $applicationType = null, $businessApplication = null, $checkMaster = null, array $parcelReferenceList = array())
    {
        $this
            ->setWsUserName($wsUserName)
            ->setWsPassword($wsPassword)
            ->setSecret($secret)
            ->setWsLang($wsLang)
            ->setApplicationType($applicationType)
            ->setBusinessApplication($businessApplication)
            ->setCheckMaster($checkMaster)
            ->setParcelReferenceList($parcelReferenceList);
    }
    /**
     * Get wsUserName value
     * @return string|null
     */
    public function getWsUserName()
    {
        return $this->wsUserName;
    }
    /**
     * Set wsUserName value
     * @param string $wsUserName
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setWsUserName($wsUserName = null)
    {
        // validation for constraint: string
        if (!is_null($wsUserName) && !is_string($wsUserName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsUserName, true), gettype($wsUserName)), __LINE__);
        }
        $this->wsUserName = $wsUserName;
        return $this;
    }
    /**
     * Get wsPassword value
     * @return string|null
     */
    public function getWsPassword()
    {
        return $this->wsPassword;
    }
    /**
     * Set wsPassword value
     * @param string $wsPassword
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setWsPassword($wsPassword = null)
    {
        // validation for constraint: string
        if (!is_null($wsPassword) && !is_string($wsPassword)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsPassword, true), gettype($wsPassword)), __LINE__);
        }
        $this->wsPassword = $wsPassword;
        return $this;
    }
    /**
     * Get secret value
     * @return string|null
     */
    public function getSecret()
    {
        return $this->secret;
    }
    /**
     * Set secret value
     * @param string $secret
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setSecret($secret = null)
    {
        // validation for constraint: string
        if (!is_null($secret) && !is_string($secret)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($secret, true), gettype($secret)), __LINE__);
        }
        $this->secret = $secret;
        return $this;
    }
    /**
     * Get wsLang value
     * @return string|null
     */
    public function getWsLang()
    {
        return $this->wsLang;
    }
    /**
     * Set wsLang value
     * @param string $wsLang
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setWsLang($wsLang = null)
    {
        // validation for constraint: string
        if (!is_null($wsLang) && !is_string($wsLang)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($wsLang, true), gettype($wsLang)), __LINE__);
        }
        $this->wsLang = $wsLang;
        return $this;
    }
    /**
     * Get applicationType value
     * @return string|null
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }
    /**
     * Set applicationType value
     * @param string $applicationType
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setApplicationType($applicationType = null)
    {
        // validation for constraint: string
        if (!is_null($applicationType) && !is_string($applicationType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($applicationType, true), gettype($applicationType)), __LINE__);
        }
        $this->applicationType = $applicationType;
        return $this;
    }
    /**
     * Get businessApplication value
     * @return string|null
     */
    public function getBusinessApplication()
    {
        return $this->businessApplication;
    }
    /**
     * Set businessApplication value
     * @param string $businessApplication
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setBusinessApplication($businessApplication = null)
    {
        // validation for constraint: string
        if (!is_null($businessApplication) && !is_string($businessApplication)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($businessApplication, true), gettype($businessApplication)), __LINE__);
        }
        $this->businessApplication = $businessApplication;
        return $this;
    }
    /**
     * Get checkMaster value
     * @return bool|null
     */
    public function getCheckMaster()
    {
        return $this->checkMaster;
    }
    /**
     * Set checkMaster value
     * @param bool $checkMaster
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setCheckMaster($checkMaster = null)
    {
        // validation for constraint: boolean
        if (!is_null($checkMaster) && !is_bool($checkMaster)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($checkMaster, true), gettype($checkMaster)), __LINE__);
        }
        $this->checkMaster = $checkMaster;
        return $this;
    }
    /**
     * Get parcelReferenceList value
     * @return string[]|null
     */
    public function getParcelReferenceList()
    {
        return $this->parcelReferenceList;
    }
    /**
     * This method is responsible for validating the values passed to the setParcelReferenceList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcelReferenceList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelReferenceListForArrayConstraintsFromSetParcelReferenceList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $getParcelIdByParcelNumberParcelReferenceListItem) {
            // validation for constraint: itemType
            if (!is_string($getParcelIdByParcelNumberParcelReferenceListItem)) {
                $invalidValues[] = is_object($getParcelIdByParcelNumberParcelReferenceListItem) ? get_class($getParcelIdByParcelNumberParcelReferenceListItem) : sprintf('%s(%s)', gettype($getParcelIdByParcelNumberParcelReferenceListItem), var_export($getParcelIdByParcelNumberParcelReferenceListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcelReferenceList property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcelReferenceList value
     * @throws \InvalidArgumentException
     * @param string[] $parcelReferenceList
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function setParcelReferenceList(array $parcelReferenceList = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelReferenceListArrayErrorMessage = self::validateParcelReferenceListForArrayConstraintsFromSetParcelReferenceList($parcelReferenceList))) {
            throw new \InvalidArgumentException($parcelReferenceListArrayErrorMessage, __LINE__);
        }
        $this->parcelReferenceList = $parcelReferenceList;
        return $this;
    }
    /**
     * Add item to parcelReferenceList value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber
     */
    public function addToParcelReferenceList($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The parcelReferenceList property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcelReferenceList[] = $item;
        return $this;
    }
}
