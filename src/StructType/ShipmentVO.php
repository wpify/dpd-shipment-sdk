<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentVO StructType
 * @subpackage Structs
 */
class ShipmentVO extends AbstractStructBase
{
    /**
     * The shipmentId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $shipmentId;
    /**
     * The shipmentReferenceNumber
     * @var string
     */
    public $shipmentReferenceNumber;
    /**
     * The shipmentReferenceNumber1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber1;
    /**
     * The shipmentReferenceNumber2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber2;
    /**
     * The shipmentReferenceNumber3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $shipmentReferenceNumber3;
    /**
     * The payerId
     * @var int
     */
    public $payerId;
    /**
     * The senderAddressId
     * @var int
     */
    public $senderAddressId;
    /**
     * The receiverName
     * @var string
     */
    public $receiverName;
    /**
     * The receiverFirmName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverFirmName;
    /**
     * The receiverCountryCode
     * @var string
     */
    public $receiverCountryCode;
    /**
     * The receiverZipCode
     * @var string
     */
    public $receiverZipCode;
    /**
     * The receiverCity
     * @var string
     */
    public $receiverCity;
    /**
     * The receiverStreet
     * @var string
     */
    public $receiverStreet;
    /**
     * The receiverHouseNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverHouseNo;
    /**
     * The receiverPhoneNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverPhoneNo;
    /**
     * The receiverEmail
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $receiverEmail;
    /**
     * The receiverAdditionalAddressInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $receiverAdditionalAddressInfo;
    /**
     * The mainServiceCode
     * @var int
     */
    public $mainServiceCode;
    /**
     * The additionalServices
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\AdditionalServiceVO
     */
    public $additionalServices;
    /**
     * The parcels
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \DPDSDK\Shipment\StructType\ParcelVO[]
     */
    public $parcels;
    /**
     * The returnAddressSubzoneId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $returnAddressSubzoneId;
    /**
     * The returnAddressContactName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $returnAddressContactName;
    /**
     * The returnAddressReferenceNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $returnAddressReferenceNumber;
    /**
     * The returnAddressEmailAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $returnAddressEmailAddress;
    /**
     * The returnAddressTelArea
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $returnAddressTelArea;
    /**
     * The returnAddressAdditionalAdrInfo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressAdditionalAdrInfo;
    /**
     * The returnAddressTownId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressTownId;
    /**
     * The returnAddressCustId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressCustId;
    /**
     * The returnAddressAdrId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressAdrId;
    /**
     * The returnAddressZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressZipCode;
    /**
     * The returnAddressCountryId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressCountryId;
    /**
     * The returnAddressAreaId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressAreaId;
    /**
     * The returnAddressZoneId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressZoneId;
    /**
     * The returnAddressCityId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressCityId;
    /**
     * The returnAddressCity
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressCity;
    /**
     * The returnAddressFirmName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressFirmName;
    /**
     * The returnAddressName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressName;
    /**
     * The returnAddressTaxNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressTaxNo;
    /**
     * The returnAddressAddressText
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressAddressText;
    /**
     * The returnAddressTelNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressTelNo;
    /**
     * The returnAddressStateId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $returnAddressStateId;
    /**
     * The returnAddressHouseNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressHouseNo;
    /**
     * The returnAddressIdType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressIdType;
    /**
     * The returnAddressIdNumber
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressIdNumber;
    /**
     * The returnAddressIdIssuedPlace
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressIdIssuedPlace;
    /**
     * The returnAddressCustType
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $returnAddressCustType;
    /**
     * The differentReturnAddress
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $differentReturnAddress;
    /**
     * Constructor method for ShipmentVO
     * @uses ShipmentVO::setShipmentId()
     * @uses ShipmentVO::setShipmentReferenceNumber()
     * @uses ShipmentVO::setShipmentReferenceNumber1()
     * @uses ShipmentVO::setShipmentReferenceNumber2()
     * @uses ShipmentVO::setShipmentReferenceNumber3()
     * @uses ShipmentVO::setPayerId()
     * @uses ShipmentVO::setSenderAddressId()
     * @uses ShipmentVO::setReceiverName()
     * @uses ShipmentVO::setReceiverFirmName()
     * @uses ShipmentVO::setReceiverCountryCode()
     * @uses ShipmentVO::setReceiverZipCode()
     * @uses ShipmentVO::setReceiverCity()
     * @uses ShipmentVO::setReceiverStreet()
     * @uses ShipmentVO::setReceiverHouseNo()
     * @uses ShipmentVO::setReceiverPhoneNo()
     * @uses ShipmentVO::setReceiverEmail()
     * @uses ShipmentVO::setReceiverAdditionalAddressInfo()
     * @uses ShipmentVO::setMainServiceCode()
     * @uses ShipmentVO::setAdditionalServices()
     * @uses ShipmentVO::setParcels()
     * @uses ShipmentVO::setReturnAddressSubzoneId()
     * @uses ShipmentVO::setReturnAddressContactName()
     * @uses ShipmentVO::setReturnAddressReferenceNumber()
     * @uses ShipmentVO::setReturnAddressEmailAddress()
     * @uses ShipmentVO::setReturnAddressTelArea()
     * @uses ShipmentVO::setReturnAddressAdditionalAdrInfo()
     * @uses ShipmentVO::setReturnAddressTownId()
     * @uses ShipmentVO::setReturnAddressCustId()
     * @uses ShipmentVO::setReturnAddressAdrId()
     * @uses ShipmentVO::setReturnAddressZipCode()
     * @uses ShipmentVO::setReturnAddressCountryId()
     * @uses ShipmentVO::setReturnAddressAreaId()
     * @uses ShipmentVO::setReturnAddressZoneId()
     * @uses ShipmentVO::setReturnAddressCityId()
     * @uses ShipmentVO::setReturnAddressCity()
     * @uses ShipmentVO::setReturnAddressFirmName()
     * @uses ShipmentVO::setReturnAddressName()
     * @uses ShipmentVO::setReturnAddressTaxNo()
     * @uses ShipmentVO::setReturnAddressAddressText()
     * @uses ShipmentVO::setReturnAddressTelNo()
     * @uses ShipmentVO::setReturnAddressStateId()
     * @uses ShipmentVO::setReturnAddressHouseNo()
     * @uses ShipmentVO::setReturnAddressIdType()
     * @uses ShipmentVO::setReturnAddressIdNumber()
     * @uses ShipmentVO::setReturnAddressIdIssuedPlace()
     * @uses ShipmentVO::setReturnAddressCustType()
     * @uses ShipmentVO::setDifferentReturnAddress()
     * @param int $shipmentId
     * @param string $shipmentReferenceNumber
     * @param string $shipmentReferenceNumber1
     * @param string $shipmentReferenceNumber2
     * @param string $shipmentReferenceNumber3
     * @param int $payerId
     * @param int $senderAddressId
     * @param string $receiverName
     * @param string $receiverFirmName
     * @param string $receiverCountryCode
     * @param string $receiverZipCode
     * @param string $receiverCity
     * @param string $receiverStreet
     * @param string $receiverHouseNo
     * @param string $receiverPhoneNo
     * @param string $receiverEmail
     * @param string $receiverAdditionalAddressInfo
     * @param int $mainServiceCode
     * @param \DPDSDK\Shipment\StructType\AdditionalServiceVO $additionalServices
     * @param \DPDSDK\Shipment\StructType\ParcelVO[] $parcels
     * @param string $returnAddressSubzoneId
     * @param string $returnAddressContactName
     * @param string $returnAddressReferenceNumber
     * @param string $returnAddressEmailAddress
     * @param string $returnAddressTelArea
     * @param string $returnAddressAdditionalAdrInfo
     * @param int $returnAddressTownId
     * @param int $returnAddressCustId
     * @param int $returnAddressAdrId
     * @param string $returnAddressZipCode
     * @param int $returnAddressCountryId
     * @param int $returnAddressAreaId
     * @param int $returnAddressZoneId
     * @param int $returnAddressCityId
     * @param string $returnAddressCity
     * @param string $returnAddressFirmName
     * @param string $returnAddressName
     * @param string $returnAddressTaxNo
     * @param string $returnAddressAddressText
     * @param string $returnAddressTelNo
     * @param int $returnAddressStateId
     * @param string $returnAddressHouseNo
     * @param string $returnAddressIdType
     * @param string $returnAddressIdNumber
     * @param string $returnAddressIdIssuedPlace
     * @param string $returnAddressCustType
     * @param string $differentReturnAddress
     */
    public function __construct($shipmentId = null, $shipmentReferenceNumber = null, $shipmentReferenceNumber1 = null, $shipmentReferenceNumber2 = null, $shipmentReferenceNumber3 = null, $payerId = null, $senderAddressId = null, $receiverName = null, $receiverFirmName = null, $receiverCountryCode = null, $receiverZipCode = null, $receiverCity = null, $receiverStreet = null, $receiverHouseNo = null, $receiverPhoneNo = null, $receiverEmail = null, $receiverAdditionalAddressInfo = null, $mainServiceCode = null, \DPDSDK\Shipment\StructType\AdditionalServiceVO $additionalServices = null, array $parcels = array(), $returnAddressSubzoneId = null, $returnAddressContactName = null, $returnAddressReferenceNumber = null, $returnAddressEmailAddress = null, $returnAddressTelArea = null, $returnAddressAdditionalAdrInfo = null, $returnAddressTownId = null, $returnAddressCustId = null, $returnAddressAdrId = null, $returnAddressZipCode = null, $returnAddressCountryId = null, $returnAddressAreaId = null, $returnAddressZoneId = null, $returnAddressCityId = null, $returnAddressCity = null, $returnAddressFirmName = null, $returnAddressName = null, $returnAddressTaxNo = null, $returnAddressAddressText = null, $returnAddressTelNo = null, $returnAddressStateId = null, $returnAddressHouseNo = null, $returnAddressIdType = null, $returnAddressIdNumber = null, $returnAddressIdIssuedPlace = null, $returnAddressCustType = null, $differentReturnAddress = null)
    {
        $this
            ->setShipmentId($shipmentId)
            ->setShipmentReferenceNumber($shipmentReferenceNumber)
            ->setShipmentReferenceNumber1($shipmentReferenceNumber1)
            ->setShipmentReferenceNumber2($shipmentReferenceNumber2)
            ->setShipmentReferenceNumber3($shipmentReferenceNumber3)
            ->setPayerId($payerId)
            ->setSenderAddressId($senderAddressId)
            ->setReceiverName($receiverName)
            ->setReceiverFirmName($receiverFirmName)
            ->setReceiverCountryCode($receiverCountryCode)
            ->setReceiverZipCode($receiverZipCode)
            ->setReceiverCity($receiverCity)
            ->setReceiverStreet($receiverStreet)
            ->setReceiverHouseNo($receiverHouseNo)
            ->setReceiverPhoneNo($receiverPhoneNo)
            ->setReceiverEmail($receiverEmail)
            ->setReceiverAdditionalAddressInfo($receiverAdditionalAddressInfo)
            ->setMainServiceCode($mainServiceCode)
            ->setAdditionalServices($additionalServices)
            ->setParcels($parcels)
            ->setReturnAddressSubzoneId($returnAddressSubzoneId)
            ->setReturnAddressContactName($returnAddressContactName)
            ->setReturnAddressReferenceNumber($returnAddressReferenceNumber)
            ->setReturnAddressEmailAddress($returnAddressEmailAddress)
            ->setReturnAddressTelArea($returnAddressTelArea)
            ->setReturnAddressAdditionalAdrInfo($returnAddressAdditionalAdrInfo)
            ->setReturnAddressTownId($returnAddressTownId)
            ->setReturnAddressCustId($returnAddressCustId)
            ->setReturnAddressAdrId($returnAddressAdrId)
            ->setReturnAddressZipCode($returnAddressZipCode)
            ->setReturnAddressCountryId($returnAddressCountryId)
            ->setReturnAddressAreaId($returnAddressAreaId)
            ->setReturnAddressZoneId($returnAddressZoneId)
            ->setReturnAddressCityId($returnAddressCityId)
            ->setReturnAddressCity($returnAddressCity)
            ->setReturnAddressFirmName($returnAddressFirmName)
            ->setReturnAddressName($returnAddressName)
            ->setReturnAddressTaxNo($returnAddressTaxNo)
            ->setReturnAddressAddressText($returnAddressAddressText)
            ->setReturnAddressTelNo($returnAddressTelNo)
            ->setReturnAddressStateId($returnAddressStateId)
            ->setReturnAddressHouseNo($returnAddressHouseNo)
            ->setReturnAddressIdType($returnAddressIdType)
            ->setReturnAddressIdNumber($returnAddressIdNumber)
            ->setReturnAddressIdIssuedPlace($returnAddressIdIssuedPlace)
            ->setReturnAddressCustType($returnAddressCustType)
            ->setDifferentReturnAddress($differentReturnAddress);
    }
    /**
     * Get shipmentId value
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }
    /**
     * Set shipmentId value
     * @param int $shipmentId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setShipmentId($shipmentId = null)
    {
        // validation for constraint: int
        if (!is_null($shipmentId) && !(is_int($shipmentId) || ctype_digit($shipmentId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($shipmentId, true), gettype($shipmentId)), __LINE__);
        }
        $this->shipmentId = $shipmentId;
        return $this;
    }
    /**
     * Get shipmentReferenceNumber value
     * @return string|null
     */
    public function getShipmentReferenceNumber()
    {
        return $this->shipmentReferenceNumber;
    }
    /**
     * Set shipmentReferenceNumber value
     * @param string $shipmentReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setShipmentReferenceNumber($shipmentReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber) && !is_string($shipmentReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber, true), gettype($shipmentReferenceNumber)), __LINE__);
        }
        $this->shipmentReferenceNumber = $shipmentReferenceNumber;
        return $this;
    }
    /**
     * Get shipmentReferenceNumber1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber1()
    {
        return isset($this->shipmentReferenceNumber1) ? $this->shipmentReferenceNumber1 : null;
    }
    /**
     * Set shipmentReferenceNumber1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber1
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setShipmentReferenceNumber1($shipmentReferenceNumber1 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber1) && !is_string($shipmentReferenceNumber1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber1, true), gettype($shipmentReferenceNumber1)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber1) || (is_array($shipmentReferenceNumber1) && empty($shipmentReferenceNumber1))) {
            unset($this->shipmentReferenceNumber1);
        } else {
            $this->shipmentReferenceNumber1 = $shipmentReferenceNumber1;
        }
        return $this;
    }
    /**
     * Get shipmentReferenceNumber2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber2()
    {
        return isset($this->shipmentReferenceNumber2) ? $this->shipmentReferenceNumber2 : null;
    }
    /**
     * Set shipmentReferenceNumber2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber2
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setShipmentReferenceNumber2($shipmentReferenceNumber2 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber2) && !is_string($shipmentReferenceNumber2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber2, true), gettype($shipmentReferenceNumber2)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber2) || (is_array($shipmentReferenceNumber2) && empty($shipmentReferenceNumber2))) {
            unset($this->shipmentReferenceNumber2);
        } else {
            $this->shipmentReferenceNumber2 = $shipmentReferenceNumber2;
        }
        return $this;
    }
    /**
     * Get shipmentReferenceNumber3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getShipmentReferenceNumber3()
    {
        return isset($this->shipmentReferenceNumber3) ? $this->shipmentReferenceNumber3 : null;
    }
    /**
     * Set shipmentReferenceNumber3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $shipmentReferenceNumber3
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setShipmentReferenceNumber3($shipmentReferenceNumber3 = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber3) && !is_string($shipmentReferenceNumber3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber3, true), gettype($shipmentReferenceNumber3)), __LINE__);
        }
        if (is_null($shipmentReferenceNumber3) || (is_array($shipmentReferenceNumber3) && empty($shipmentReferenceNumber3))) {
            unset($this->shipmentReferenceNumber3);
        } else {
            $this->shipmentReferenceNumber3 = $shipmentReferenceNumber3;
        }
        return $this;
    }
    /**
     * Get payerId value
     * @return int|null
     */
    public function getPayerId()
    {
        return $this->payerId;
    }
    /**
     * Set payerId value
     * @param int $payerId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setPayerId($payerId = null)
    {
        // validation for constraint: int
        if (!is_null($payerId) && !(is_int($payerId) || ctype_digit($payerId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($payerId, true), gettype($payerId)), __LINE__);
        }
        $this->payerId = $payerId;
        return $this;
    }
    /**
     * Get senderAddressId value
     * @return int|null
     */
    public function getSenderAddressId()
    {
        return $this->senderAddressId;
    }
    /**
     * Set senderAddressId value
     * @param int $senderAddressId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setSenderAddressId($senderAddressId = null)
    {
        // validation for constraint: int
        if (!is_null($senderAddressId) && !(is_int($senderAddressId) || ctype_digit($senderAddressId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($senderAddressId, true), gettype($senderAddressId)), __LINE__);
        }
        $this->senderAddressId = $senderAddressId;
        return $this;
    }
    /**
     * Get receiverName value
     * @return string|null
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }
    /**
     * Set receiverName value
     * @param string $receiverName
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverName($receiverName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverName) && !is_string($receiverName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverName, true), gettype($receiverName)), __LINE__);
        }
        $this->receiverName = $receiverName;
        return $this;
    }
    /**
     * Get receiverFirmName value
     * @return string|null
     */
    public function getReceiverFirmName()
    {
        return $this->receiverFirmName;
    }
    /**
     * Set receiverFirmName value
     * @param string $receiverFirmName
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverFirmName($receiverFirmName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverFirmName) && !is_string($receiverFirmName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverFirmName, true), gettype($receiverFirmName)), __LINE__);
        }
        $this->receiverFirmName = $receiverFirmName;
        return $this;
    }
    /**
     * Get receiverCountryCode value
     * @return string|null
     */
    public function getReceiverCountryCode()
    {
        return $this->receiverCountryCode;
    }
    /**
     * Set receiverCountryCode value
     * @param string $receiverCountryCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverCountryCode($receiverCountryCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCountryCode) && !is_string($receiverCountryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCountryCode, true), gettype($receiverCountryCode)), __LINE__);
        }
        $this->receiverCountryCode = $receiverCountryCode;
        return $this;
    }
    /**
     * Get receiverZipCode value
     * @return string|null
     */
    public function getReceiverZipCode()
    {
        return $this->receiverZipCode;
    }
    /**
     * Set receiverZipCode value
     * @param string $receiverZipCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverZipCode($receiverZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverZipCode) && !is_string($receiverZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverZipCode, true), gettype($receiverZipCode)), __LINE__);
        }
        $this->receiverZipCode = $receiverZipCode;
        return $this;
    }
    /**
     * Get receiverCity value
     * @return string|null
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }
    /**
     * Set receiverCity value
     * @param string $receiverCity
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverCity($receiverCity = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCity) && !is_string($receiverCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCity, true), gettype($receiverCity)), __LINE__);
        }
        $this->receiverCity = $receiverCity;
        return $this;
    }
    /**
     * Get receiverStreet value
     * @return string|null
     */
    public function getReceiverStreet()
    {
        return $this->receiverStreet;
    }
    /**
     * Set receiverStreet value
     * @param string $receiverStreet
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverStreet($receiverStreet = null)
    {
        // validation for constraint: string
        if (!is_null($receiverStreet) && !is_string($receiverStreet)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverStreet, true), gettype($receiverStreet)), __LINE__);
        }
        $this->receiverStreet = $receiverStreet;
        return $this;
    }
    /**
     * Get receiverHouseNo value
     * @return string|null
     */
    public function getReceiverHouseNo()
    {
        return $this->receiverHouseNo;
    }
    /**
     * Set receiverHouseNo value
     * @param string $receiverHouseNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverHouseNo($receiverHouseNo = null)
    {
        // validation for constraint: string
        if (!is_null($receiverHouseNo) && !is_string($receiverHouseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverHouseNo, true), gettype($receiverHouseNo)), __LINE__);
        }
        $this->receiverHouseNo = $receiverHouseNo;
        return $this;
    }
    /**
     * Get receiverPhoneNo value
     * @return string|null
     */
    public function getReceiverPhoneNo()
    {
        return $this->receiverPhoneNo;
    }
    /**
     * Set receiverPhoneNo value
     * @param string $receiverPhoneNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverPhoneNo($receiverPhoneNo = null)
    {
        // validation for constraint: string
        if (!is_null($receiverPhoneNo) && !is_string($receiverPhoneNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverPhoneNo, true), gettype($receiverPhoneNo)), __LINE__);
        }
        $this->receiverPhoneNo = $receiverPhoneNo;
        return $this;
    }
    /**
     * Get receiverEmail value
     * @return string|null
     */
    public function getReceiverEmail()
    {
        return $this->receiverEmail;
    }
    /**
     * Set receiverEmail value
     * @param string $receiverEmail
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverEmail($receiverEmail = null)
    {
        // validation for constraint: string
        if (!is_null($receiverEmail) && !is_string($receiverEmail)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverEmail, true), gettype($receiverEmail)), __LINE__);
        }
        $this->receiverEmail = $receiverEmail;
        return $this;
    }
    /**
     * Get receiverAdditionalAddressInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReceiverAdditionalAddressInfo()
    {
        return isset($this->receiverAdditionalAddressInfo) ? $this->receiverAdditionalAddressInfo : null;
    }
    /**
     * Set receiverAdditionalAddressInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $receiverAdditionalAddressInfo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReceiverAdditionalAddressInfo($receiverAdditionalAddressInfo = null)
    {
        // validation for constraint: string
        if (!is_null($receiverAdditionalAddressInfo) && !is_string($receiverAdditionalAddressInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverAdditionalAddressInfo, true), gettype($receiverAdditionalAddressInfo)), __LINE__);
        }
        if (is_null($receiverAdditionalAddressInfo) || (is_array($receiverAdditionalAddressInfo) && empty($receiverAdditionalAddressInfo))) {
            unset($this->receiverAdditionalAddressInfo);
        } else {
            $this->receiverAdditionalAddressInfo = $receiverAdditionalAddressInfo;
        }
        return $this;
    }
    /**
     * Get mainServiceCode value
     * @return int|null
     */
    public function getMainServiceCode()
    {
        return $this->mainServiceCode;
    }
    /**
     * Set mainServiceCode value
     * @param int $mainServiceCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setMainServiceCode($mainServiceCode = null)
    {
        // validation for constraint: int
        if (!is_null($mainServiceCode) && !(is_int($mainServiceCode) || ctype_digit($mainServiceCode))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($mainServiceCode, true), gettype($mainServiceCode)), __LINE__);
        }
        $this->mainServiceCode = $mainServiceCode;
        return $this;
    }
    /**
     * Get additionalServices value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVO|null
     */
    public function getAdditionalServices()
    {
        return isset($this->additionalServices) ? $this->additionalServices : null;
    }
    /**
     * Set additionalServices value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\AdditionalServiceVO $additionalServices
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setAdditionalServices(\DPDSDK\Shipment\StructType\AdditionalServiceVO $additionalServices = null)
    {
        if (is_null($additionalServices) || (is_array($additionalServices) && empty($additionalServices))) {
            unset($this->additionalServices);
        } else {
            $this->additionalServices = $additionalServices;
        }
        return $this;
    }
    /**
     * Get parcels value
     * @return \DPDSDK\Shipment\StructType\ParcelVO[]|null
     */
    public function getParcels()
    {
        return $this->parcels;
    }
    /**
     * This method is responsible for validating the values passed to the setParcels method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcels method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelsForArrayConstraintsFromSetParcels(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentVOParcelsItem) {
            // validation for constraint: itemType
            if (!$shipmentVOParcelsItem instanceof \DPDSDK\Shipment\StructType\ParcelVO) {
                $invalidValues[] = is_object($shipmentVOParcelsItem) ? get_class($shipmentVOParcelsItem) : sprintf('%s(%s)', gettype($shipmentVOParcelsItem), var_export($shipmentVOParcelsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcels value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelVO[] $parcels
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setParcels(array $parcels = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelsArrayErrorMessage = self::validateParcelsForArrayConstraintsFromSetParcels($parcels))) {
            throw new \InvalidArgumentException($parcelsArrayErrorMessage, __LINE__);
        }
        $this->parcels = $parcels;
        return $this;
    }
    /**
     * Add item to parcels value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function addToParcels(\DPDSDK\Shipment\StructType\ParcelVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelVO) {
            throw new \InvalidArgumentException(sprintf('The parcels property can only contain items of type \DPDSDK\Shipment\StructType\ParcelVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcels[] = $item;
        return $this;
    }
    /**
     * Get returnAddressSubzoneId value
     * @return string|null
     */
    public function getReturnAddressSubzoneId()
    {
        return $this->returnAddressSubzoneId;
    }
    /**
     * Set returnAddressSubzoneId value
     * @param string $returnAddressSubzoneId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressSubzoneId($returnAddressSubzoneId = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressSubzoneId) && !is_string($returnAddressSubzoneId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressSubzoneId, true), gettype($returnAddressSubzoneId)), __LINE__);
        }
        $this->returnAddressSubzoneId = $returnAddressSubzoneId;
        return $this;
    }
    /**
     * Get returnAddressContactName value
     * @return string|null
     */
    public function getReturnAddressContactName()
    {
        return $this->returnAddressContactName;
    }
    /**
     * Set returnAddressContactName value
     * @param string $returnAddressContactName
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressContactName($returnAddressContactName = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressContactName) && !is_string($returnAddressContactName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressContactName, true), gettype($returnAddressContactName)), __LINE__);
        }
        $this->returnAddressContactName = $returnAddressContactName;
        return $this;
    }
    /**
     * Get returnAddressReferenceNumber value
     * @return string|null
     */
    public function getReturnAddressReferenceNumber()
    {
        return $this->returnAddressReferenceNumber;
    }
    /**
     * Set returnAddressReferenceNumber value
     * @param string $returnAddressReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressReferenceNumber($returnAddressReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressReferenceNumber) && !is_string($returnAddressReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressReferenceNumber, true), gettype($returnAddressReferenceNumber)), __LINE__);
        }
        $this->returnAddressReferenceNumber = $returnAddressReferenceNumber;
        return $this;
    }
    /**
     * Get returnAddressEmailAddress value
     * @return string|null
     */
    public function getReturnAddressEmailAddress()
    {
        return $this->returnAddressEmailAddress;
    }
    /**
     * Set returnAddressEmailAddress value
     * @param string $returnAddressEmailAddress
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressEmailAddress($returnAddressEmailAddress = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressEmailAddress) && !is_string($returnAddressEmailAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressEmailAddress, true), gettype($returnAddressEmailAddress)), __LINE__);
        }
        $this->returnAddressEmailAddress = $returnAddressEmailAddress;
        return $this;
    }
    /**
     * Get returnAddressTelArea value
     * @return string|null
     */
    public function getReturnAddressTelArea()
    {
        return $this->returnAddressTelArea;
    }
    /**
     * Set returnAddressTelArea value
     * @param string $returnAddressTelArea
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressTelArea($returnAddressTelArea = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressTelArea) && !is_string($returnAddressTelArea)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressTelArea, true), gettype($returnAddressTelArea)), __LINE__);
        }
        $this->returnAddressTelArea = $returnAddressTelArea;
        return $this;
    }
    /**
     * Get returnAddressAdditionalAdrInfo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressAdditionalAdrInfo()
    {
        return isset($this->returnAddressAdditionalAdrInfo) ? $this->returnAddressAdditionalAdrInfo : null;
    }
    /**
     * Set returnAddressAdditionalAdrInfo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressAdditionalAdrInfo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressAdditionalAdrInfo($returnAddressAdditionalAdrInfo = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressAdditionalAdrInfo) && !is_string($returnAddressAdditionalAdrInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressAdditionalAdrInfo, true), gettype($returnAddressAdditionalAdrInfo)), __LINE__);
        }
        if (is_null($returnAddressAdditionalAdrInfo) || (is_array($returnAddressAdditionalAdrInfo) && empty($returnAddressAdditionalAdrInfo))) {
            unset($this->returnAddressAdditionalAdrInfo);
        } else {
            $this->returnAddressAdditionalAdrInfo = $returnAddressAdditionalAdrInfo;
        }
        return $this;
    }
    /**
     * Get returnAddressTownId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressTownId()
    {
        return isset($this->returnAddressTownId) ? $this->returnAddressTownId : null;
    }
    /**
     * Set returnAddressTownId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressTownId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressTownId($returnAddressTownId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressTownId) && !(is_int($returnAddressTownId) || ctype_digit($returnAddressTownId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressTownId, true), gettype($returnAddressTownId)), __LINE__);
        }
        if (is_null($returnAddressTownId) || (is_array($returnAddressTownId) && empty($returnAddressTownId))) {
            unset($this->returnAddressTownId);
        } else {
            $this->returnAddressTownId = $returnAddressTownId;
        }
        return $this;
    }
    /**
     * Get returnAddressCustId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressCustId()
    {
        return isset($this->returnAddressCustId) ? $this->returnAddressCustId : null;
    }
    /**
     * Set returnAddressCustId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressCustId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressCustId($returnAddressCustId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressCustId) && !(is_int($returnAddressCustId) || ctype_digit($returnAddressCustId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressCustId, true), gettype($returnAddressCustId)), __LINE__);
        }
        if (is_null($returnAddressCustId) || (is_array($returnAddressCustId) && empty($returnAddressCustId))) {
            unset($this->returnAddressCustId);
        } else {
            $this->returnAddressCustId = $returnAddressCustId;
        }
        return $this;
    }
    /**
     * Get returnAddressAdrId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressAdrId()
    {
        return isset($this->returnAddressAdrId) ? $this->returnAddressAdrId : null;
    }
    /**
     * Set returnAddressAdrId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressAdrId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressAdrId($returnAddressAdrId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressAdrId) && !(is_int($returnAddressAdrId) || ctype_digit($returnAddressAdrId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressAdrId, true), gettype($returnAddressAdrId)), __LINE__);
        }
        if (is_null($returnAddressAdrId) || (is_array($returnAddressAdrId) && empty($returnAddressAdrId))) {
            unset($this->returnAddressAdrId);
        } else {
            $this->returnAddressAdrId = $returnAddressAdrId;
        }
        return $this;
    }
    /**
     * Get returnAddressZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressZipCode()
    {
        return isset($this->returnAddressZipCode) ? $this->returnAddressZipCode : null;
    }
    /**
     * Set returnAddressZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressZipCode
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressZipCode($returnAddressZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressZipCode) && !is_string($returnAddressZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressZipCode, true), gettype($returnAddressZipCode)), __LINE__);
        }
        if (is_null($returnAddressZipCode) || (is_array($returnAddressZipCode) && empty($returnAddressZipCode))) {
            unset($this->returnAddressZipCode);
        } else {
            $this->returnAddressZipCode = $returnAddressZipCode;
        }
        return $this;
    }
    /**
     * Get returnAddressCountryId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressCountryId()
    {
        return isset($this->returnAddressCountryId) ? $this->returnAddressCountryId : null;
    }
    /**
     * Set returnAddressCountryId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressCountryId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressCountryId($returnAddressCountryId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressCountryId) && !(is_int($returnAddressCountryId) || ctype_digit($returnAddressCountryId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressCountryId, true), gettype($returnAddressCountryId)), __LINE__);
        }
        if (is_null($returnAddressCountryId) || (is_array($returnAddressCountryId) && empty($returnAddressCountryId))) {
            unset($this->returnAddressCountryId);
        } else {
            $this->returnAddressCountryId = $returnAddressCountryId;
        }
        return $this;
    }
    /**
     * Get returnAddressAreaId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressAreaId()
    {
        return isset($this->returnAddressAreaId) ? $this->returnAddressAreaId : null;
    }
    /**
     * Set returnAddressAreaId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressAreaId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressAreaId($returnAddressAreaId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressAreaId) && !(is_int($returnAddressAreaId) || ctype_digit($returnAddressAreaId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressAreaId, true), gettype($returnAddressAreaId)), __LINE__);
        }
        if (is_null($returnAddressAreaId) || (is_array($returnAddressAreaId) && empty($returnAddressAreaId))) {
            unset($this->returnAddressAreaId);
        } else {
            $this->returnAddressAreaId = $returnAddressAreaId;
        }
        return $this;
    }
    /**
     * Get returnAddressZoneId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressZoneId()
    {
        return isset($this->returnAddressZoneId) ? $this->returnAddressZoneId : null;
    }
    /**
     * Set returnAddressZoneId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressZoneId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressZoneId($returnAddressZoneId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressZoneId) && !(is_int($returnAddressZoneId) || ctype_digit($returnAddressZoneId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressZoneId, true), gettype($returnAddressZoneId)), __LINE__);
        }
        if (is_null($returnAddressZoneId) || (is_array($returnAddressZoneId) && empty($returnAddressZoneId))) {
            unset($this->returnAddressZoneId);
        } else {
            $this->returnAddressZoneId = $returnAddressZoneId;
        }
        return $this;
    }
    /**
     * Get returnAddressCityId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressCityId()
    {
        return isset($this->returnAddressCityId) ? $this->returnAddressCityId : null;
    }
    /**
     * Set returnAddressCityId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressCityId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressCityId($returnAddressCityId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressCityId) && !(is_int($returnAddressCityId) || ctype_digit($returnAddressCityId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressCityId, true), gettype($returnAddressCityId)), __LINE__);
        }
        if (is_null($returnAddressCityId) || (is_array($returnAddressCityId) && empty($returnAddressCityId))) {
            unset($this->returnAddressCityId);
        } else {
            $this->returnAddressCityId = $returnAddressCityId;
        }
        return $this;
    }
    /**
     * Get returnAddressCity value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressCity()
    {
        return isset($this->returnAddressCity) ? $this->returnAddressCity : null;
    }
    /**
     * Set returnAddressCity value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressCity
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressCity($returnAddressCity = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressCity) && !is_string($returnAddressCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressCity, true), gettype($returnAddressCity)), __LINE__);
        }
        if (is_null($returnAddressCity) || (is_array($returnAddressCity) && empty($returnAddressCity))) {
            unset($this->returnAddressCity);
        } else {
            $this->returnAddressCity = $returnAddressCity;
        }
        return $this;
    }
    /**
     * Get returnAddressFirmName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressFirmName()
    {
        return isset($this->returnAddressFirmName) ? $this->returnAddressFirmName : null;
    }
    /**
     * Set returnAddressFirmName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressFirmName
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressFirmName($returnAddressFirmName = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressFirmName) && !is_string($returnAddressFirmName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressFirmName, true), gettype($returnAddressFirmName)), __LINE__);
        }
        if (is_null($returnAddressFirmName) || (is_array($returnAddressFirmName) && empty($returnAddressFirmName))) {
            unset($this->returnAddressFirmName);
        } else {
            $this->returnAddressFirmName = $returnAddressFirmName;
        }
        return $this;
    }
    /**
     * Get returnAddressName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressName()
    {
        return isset($this->returnAddressName) ? $this->returnAddressName : null;
    }
    /**
     * Set returnAddressName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressName
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressName($returnAddressName = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressName) && !is_string($returnAddressName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressName, true), gettype($returnAddressName)), __LINE__);
        }
        if (is_null($returnAddressName) || (is_array($returnAddressName) && empty($returnAddressName))) {
            unset($this->returnAddressName);
        } else {
            $this->returnAddressName = $returnAddressName;
        }
        return $this;
    }
    /**
     * Get returnAddressTaxNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressTaxNo()
    {
        return isset($this->returnAddressTaxNo) ? $this->returnAddressTaxNo : null;
    }
    /**
     * Set returnAddressTaxNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressTaxNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressTaxNo($returnAddressTaxNo = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressTaxNo) && !is_string($returnAddressTaxNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressTaxNo, true), gettype($returnAddressTaxNo)), __LINE__);
        }
        if (is_null($returnAddressTaxNo) || (is_array($returnAddressTaxNo) && empty($returnAddressTaxNo))) {
            unset($this->returnAddressTaxNo);
        } else {
            $this->returnAddressTaxNo = $returnAddressTaxNo;
        }
        return $this;
    }
    /**
     * Get returnAddressAddressText value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressAddressText()
    {
        return isset($this->returnAddressAddressText) ? $this->returnAddressAddressText : null;
    }
    /**
     * Set returnAddressAddressText value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressAddressText
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressAddressText($returnAddressAddressText = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressAddressText) && !is_string($returnAddressAddressText)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressAddressText, true), gettype($returnAddressAddressText)), __LINE__);
        }
        if (is_null($returnAddressAddressText) || (is_array($returnAddressAddressText) && empty($returnAddressAddressText))) {
            unset($this->returnAddressAddressText);
        } else {
            $this->returnAddressAddressText = $returnAddressAddressText;
        }
        return $this;
    }
    /**
     * Get returnAddressTelNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressTelNo()
    {
        return isset($this->returnAddressTelNo) ? $this->returnAddressTelNo : null;
    }
    /**
     * Set returnAddressTelNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressTelNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressTelNo($returnAddressTelNo = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressTelNo) && !is_string($returnAddressTelNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressTelNo, true), gettype($returnAddressTelNo)), __LINE__);
        }
        if (is_null($returnAddressTelNo) || (is_array($returnAddressTelNo) && empty($returnAddressTelNo))) {
            unset($this->returnAddressTelNo);
        } else {
            $this->returnAddressTelNo = $returnAddressTelNo;
        }
        return $this;
    }
    /**
     * Get returnAddressStateId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getReturnAddressStateId()
    {
        return isset($this->returnAddressStateId) ? $this->returnAddressStateId : null;
    }
    /**
     * Set returnAddressStateId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $returnAddressStateId
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressStateId($returnAddressStateId = null)
    {
        // validation for constraint: int
        if (!is_null($returnAddressStateId) && !(is_int($returnAddressStateId) || ctype_digit($returnAddressStateId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($returnAddressStateId, true), gettype($returnAddressStateId)), __LINE__);
        }
        if (is_null($returnAddressStateId) || (is_array($returnAddressStateId) && empty($returnAddressStateId))) {
            unset($this->returnAddressStateId);
        } else {
            $this->returnAddressStateId = $returnAddressStateId;
        }
        return $this;
    }
    /**
     * Get returnAddressHouseNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressHouseNo()
    {
        return isset($this->returnAddressHouseNo) ? $this->returnAddressHouseNo : null;
    }
    /**
     * Set returnAddressHouseNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressHouseNo
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressHouseNo($returnAddressHouseNo = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressHouseNo) && !is_string($returnAddressHouseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressHouseNo, true), gettype($returnAddressHouseNo)), __LINE__);
        }
        if (is_null($returnAddressHouseNo) || (is_array($returnAddressHouseNo) && empty($returnAddressHouseNo))) {
            unset($this->returnAddressHouseNo);
        } else {
            $this->returnAddressHouseNo = $returnAddressHouseNo;
        }
        return $this;
    }
    /**
     * Get returnAddressIdType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressIdType()
    {
        return isset($this->returnAddressIdType) ? $this->returnAddressIdType : null;
    }
    /**
     * Set returnAddressIdType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressIdType
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressIdType($returnAddressIdType = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressIdType) && !is_string($returnAddressIdType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressIdType, true), gettype($returnAddressIdType)), __LINE__);
        }
        if (is_null($returnAddressIdType) || (is_array($returnAddressIdType) && empty($returnAddressIdType))) {
            unset($this->returnAddressIdType);
        } else {
            $this->returnAddressIdType = $returnAddressIdType;
        }
        return $this;
    }
    /**
     * Get returnAddressIdNumber value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressIdNumber()
    {
        return isset($this->returnAddressIdNumber) ? $this->returnAddressIdNumber : null;
    }
    /**
     * Set returnAddressIdNumber value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressIdNumber
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressIdNumber($returnAddressIdNumber = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressIdNumber) && !is_string($returnAddressIdNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressIdNumber, true), gettype($returnAddressIdNumber)), __LINE__);
        }
        if (is_null($returnAddressIdNumber) || (is_array($returnAddressIdNumber) && empty($returnAddressIdNumber))) {
            unset($this->returnAddressIdNumber);
        } else {
            $this->returnAddressIdNumber = $returnAddressIdNumber;
        }
        return $this;
    }
    /**
     * Get returnAddressIdIssuedPlace value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressIdIssuedPlace()
    {
        return isset($this->returnAddressIdIssuedPlace) ? $this->returnAddressIdIssuedPlace : null;
    }
    /**
     * Set returnAddressIdIssuedPlace value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressIdIssuedPlace
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressIdIssuedPlace($returnAddressIdIssuedPlace = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressIdIssuedPlace) && !is_string($returnAddressIdIssuedPlace)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressIdIssuedPlace, true), gettype($returnAddressIdIssuedPlace)), __LINE__);
        }
        if (is_null($returnAddressIdIssuedPlace) || (is_array($returnAddressIdIssuedPlace) && empty($returnAddressIdIssuedPlace))) {
            unset($this->returnAddressIdIssuedPlace);
        } else {
            $this->returnAddressIdIssuedPlace = $returnAddressIdIssuedPlace;
        }
        return $this;
    }
    /**
     * Get returnAddressCustType value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReturnAddressCustType()
    {
        return isset($this->returnAddressCustType) ? $this->returnAddressCustType : null;
    }
    /**
     * Set returnAddressCustType value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $returnAddressCustType
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setReturnAddressCustType($returnAddressCustType = null)
    {
        // validation for constraint: string
        if (!is_null($returnAddressCustType) && !is_string($returnAddressCustType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAddressCustType, true), gettype($returnAddressCustType)), __LINE__);
        }
        if (is_null($returnAddressCustType) || (is_array($returnAddressCustType) && empty($returnAddressCustType))) {
            unset($this->returnAddressCustType);
        } else {
            $this->returnAddressCustType = $returnAddressCustType;
        }
        return $this;
    }
    /**
     * Get differentReturnAddress value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getDifferentReturnAddress()
    {
        return isset($this->differentReturnAddress) ? $this->differentReturnAddress : null;
    }
    /**
     * Set differentReturnAddress value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $differentReturnAddress
     * @return \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public function setDifferentReturnAddress($differentReturnAddress = null)
    {
        // validation for constraint: string
        if (!is_null($differentReturnAddress) && !is_string($differentReturnAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($differentReturnAddress, true), gettype($differentReturnAddress)), __LINE__);
        }
        if (is_null($differentReturnAddress) || (is_array($differentReturnAddress) && empty($differentReturnAddress))) {
            unset($this->differentReturnAddress);
        } else {
            $this->differentReturnAddress = $differentReturnAddress;
        }
        return $this;
    }
}
