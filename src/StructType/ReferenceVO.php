<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ReferenceVO StructType
 * @subpackage Structs
 */
class ReferenceVO extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $id;
    /**
     * The referenceNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $referenceNumber;
    /**
     * The referenceNumber1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $referenceNumber1;
    /**
     * The referenceNumber2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $referenceNumber2;
    /**
     * The referenceNumber3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $referenceNumber3;
    /**
     * Constructor method for ReferenceVO
     * @uses ReferenceVO::setId()
     * @uses ReferenceVO::setReferenceNumber()
     * @uses ReferenceVO::setReferenceNumber1()
     * @uses ReferenceVO::setReferenceNumber2()
     * @uses ReferenceVO::setReferenceNumber3()
     * @param int $id
     * @param string $referenceNumber
     * @param string $referenceNumber1
     * @param string $referenceNumber2
     * @param string $referenceNumber3
     */
    public function __construct($id = null, $referenceNumber = null, $referenceNumber1 = null, $referenceNumber2 = null, $referenceNumber3 = null)
    {
        $this
            ->setId($id)
            ->setReferenceNumber($referenceNumber)
            ->setReferenceNumber1($referenceNumber1)
            ->setReferenceNumber2($referenceNumber2)
            ->setReferenceNumber3($referenceNumber3);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get referenceNumber value
     * @return string|null
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }
    /**
     * Set referenceNumber value
     * @param string $referenceNumber
     * @return \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public function setReferenceNumber($referenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($referenceNumber) && !is_string($referenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber, true), gettype($referenceNumber)), __LINE__);
        }
        $this->referenceNumber = $referenceNumber;
        return $this;
    }
    /**
     * Get referenceNumber1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReferenceNumber1()
    {
        return isset($this->referenceNumber1) ? $this->referenceNumber1 : null;
    }
    /**
     * Set referenceNumber1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $referenceNumber1
     * @return \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public function setReferenceNumber1($referenceNumber1 = null)
    {
        // validation for constraint: string
        if (!is_null($referenceNumber1) && !is_string($referenceNumber1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber1, true), gettype($referenceNumber1)), __LINE__);
        }
        if (is_null($referenceNumber1) || (is_array($referenceNumber1) && empty($referenceNumber1))) {
            unset($this->referenceNumber1);
        } else {
            $this->referenceNumber1 = $referenceNumber1;
        }
        return $this;
    }
    /**
     * Get referenceNumber2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReferenceNumber2()
    {
        return isset($this->referenceNumber2) ? $this->referenceNumber2 : null;
    }
    /**
     * Set referenceNumber2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $referenceNumber2
     * @return \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public function setReferenceNumber2($referenceNumber2 = null)
    {
        // validation for constraint: string
        if (!is_null($referenceNumber2) && !is_string($referenceNumber2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber2, true), gettype($referenceNumber2)), __LINE__);
        }
        if (is_null($referenceNumber2) || (is_array($referenceNumber2) && empty($referenceNumber2))) {
            unset($this->referenceNumber2);
        } else {
            $this->referenceNumber2 = $referenceNumber2;
        }
        return $this;
    }
    /**
     * Get referenceNumber3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReferenceNumber3()
    {
        return isset($this->referenceNumber3) ? $this->referenceNumber3 : null;
    }
    /**
     * Set referenceNumber3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $referenceNumber3
     * @return \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public function setReferenceNumber3($referenceNumber3 = null)
    {
        // validation for constraint: string
        if (!is_null($referenceNumber3) && !is_string($referenceNumber3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($referenceNumber3, true), gettype($referenceNumber3)), __LINE__);
        }
        if (is_null($referenceNumber3) || (is_array($referenceNumber3) && empty($referenceNumber3))) {
            unset($this->referenceNumber3);
        } else {
            $this->referenceNumber3 = $referenceNumber3;
        }
        return $this;
    }
}
