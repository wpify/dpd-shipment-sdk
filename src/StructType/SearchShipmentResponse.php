<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for searchShipmentResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:searchShipmentResponse
 * @subpackage Structs
 */
class SearchShipmentResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO
     */
    public $result;
    /**
     * Constructor method for searchShipmentResponse
     * @uses SearchShipmentResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ShipmentSearchResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ShipmentSearchResponseVO $result
     * @return \DPDSDK\Shipment\StructType\SearchShipmentResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ShipmentSearchResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
