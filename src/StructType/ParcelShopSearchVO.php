<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopSearchVO StructType
 * Meta information extracted from the WSDL
 * - type: tns:ParcelShopSearchVO
 * @subpackage Structs
 */
class ParcelShopSearchVO extends AbstractStructBase
{
    /**
     * The street
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $street;
    /**
     * The houseNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $houseNo;
    /**
     * The countryCode
     * @var string
     */
    public $countryCode;
    /**
     * The zipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $zipCode;
    /**
     * The city
     * @var string
     */
    public $city;
    /**
     * The limit
     * Meta information extracted from the WSDL
     * - default: 10
     * @var int
     */
    public $limit;
    /**
     * The consigneePickupAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $consigneePickupAllowed;
    /**
     * The returnAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $returnAllowed;
    /**
     * The expressAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $expressAllowed;
    /**
     * The codAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codAllowed;
    /**
     * The codPaymentType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codPaymentType;
    /**
     * Constructor method for ParcelShopSearchVO
     * @uses ParcelShopSearchVO::setStreet()
     * @uses ParcelShopSearchVO::setHouseNo()
     * @uses ParcelShopSearchVO::setCountryCode()
     * @uses ParcelShopSearchVO::setZipCode()
     * @uses ParcelShopSearchVO::setCity()
     * @uses ParcelShopSearchVO::setLimit()
     * @uses ParcelShopSearchVO::setConsigneePickupAllowed()
     * @uses ParcelShopSearchVO::setReturnAllowed()
     * @uses ParcelShopSearchVO::setExpressAllowed()
     * @uses ParcelShopSearchVO::setCodAllowed()
     * @uses ParcelShopSearchVO::setCodPaymentType()
     * @param string $street
     * @param string $houseNo
     * @param string $countryCode
     * @param string $zipCode
     * @param string $city
     * @param int $limit
     * @param string $consigneePickupAllowed
     * @param string $returnAllowed
     * @param string $expressAllowed
     * @param string $codAllowed
     * @param string $codPaymentType
     */
    public function __construct($street = null, $houseNo = null, $countryCode = null, $zipCode = null, $city = null, $limit = 10, $consigneePickupAllowed = null, $returnAllowed = null, $expressAllowed = null, $codAllowed = null, $codPaymentType = null)
    {
        $this
            ->setStreet($street)
            ->setHouseNo($houseNo)
            ->setCountryCode($countryCode)
            ->setZipCode($zipCode)
            ->setCity($city)
            ->setLimit($limit)
            ->setConsigneePickupAllowed($consigneePickupAllowed)
            ->setReturnAllowed($returnAllowed)
            ->setExpressAllowed($expressAllowed)
            ->setCodAllowed($codAllowed)
            ->setCodPaymentType($codPaymentType);
    }
    /**
     * Get street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * Set street value
     * @param string $street
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        $this->street = $street;
        return $this;
    }
    /**
     * Get houseNo value
     * @return string|null
     */
    public function getHouseNo()
    {
        return $this->houseNo;
    }
    /**
     * Set houseNo value
     * @param string $houseNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setHouseNo($houseNo = null)
    {
        // validation for constraint: string
        if (!is_null($houseNo) && !is_string($houseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseNo, true), gettype($houseNo)), __LINE__);
        }
        $this->houseNo = $houseNo;
        return $this;
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        return $this;
    }
    /**
     * Get zipCode value
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
    /**
     * Set zipCode value
     * @param string $zipCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        $this->zipCode = $zipCode;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get limit value
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }
    /**
     * Set limit value
     * @param int $limit
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setLimit($limit = 10)
    {
        // validation for constraint: int
        if (!is_null($limit) && !(is_int($limit) || ctype_digit($limit))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($limit, true), gettype($limit)), __LINE__);
        }
        $this->limit = $limit;
        return $this;
    }
    /**
     * Get consigneePickupAllowed value
     * @return string|null
     */
    public function getConsigneePickupAllowed()
    {
        return $this->consigneePickupAllowed;
    }
    /**
     * Set consigneePickupAllowed value
     * @param string $consigneePickupAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setConsigneePickupAllowed($consigneePickupAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($consigneePickupAllowed) && !is_string($consigneePickupAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($consigneePickupAllowed, true), gettype($consigneePickupAllowed)), __LINE__);
        }
        $this->consigneePickupAllowed = $consigneePickupAllowed;
        return $this;
    }
    /**
     * Get returnAllowed value
     * @return string|null
     */
    public function getReturnAllowed()
    {
        return $this->returnAllowed;
    }
    /**
     * Set returnAllowed value
     * @param string $returnAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setReturnAllowed($returnAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($returnAllowed) && !is_string($returnAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAllowed, true), gettype($returnAllowed)), __LINE__);
        }
        $this->returnAllowed = $returnAllowed;
        return $this;
    }
    /**
     * Get expressAllowed value
     * @return string|null
     */
    public function getExpressAllowed()
    {
        return $this->expressAllowed;
    }
    /**
     * Set expressAllowed value
     * @param string $expressAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setExpressAllowed($expressAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($expressAllowed) && !is_string($expressAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expressAllowed, true), gettype($expressAllowed)), __LINE__);
        }
        $this->expressAllowed = $expressAllowed;
        return $this;
    }
    /**
     * Get codAllowed value
     * @return string|null
     */
    public function getCodAllowed()
    {
        return $this->codAllowed;
    }
    /**
     * Set codAllowed value
     * @param string $codAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setCodAllowed($codAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($codAllowed) && !is_string($codAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codAllowed, true), gettype($codAllowed)), __LINE__);
        }
        $this->codAllowed = $codAllowed;
        return $this;
    }
    /**
     * Get codPaymentType value
     * @return string|null
     */
    public function getCodPaymentType()
    {
        return $this->codPaymentType;
    }
    /**
     * Set codPaymentType value
     * @param string $codPaymentType
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchVO
     */
    public function setCodPaymentType($codPaymentType = null)
    {
        // validation for constraint: string
        if (!is_null($codPaymentType) && !is_string($codPaymentType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codPaymentType, true), gettype($codPaymentType)), __LINE__);
        }
        $this->codPaymentType = $codPaymentType;
        return $this;
    }
}
