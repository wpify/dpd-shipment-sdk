<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentGetResultVO StructType
 * @subpackage Structs
 */
class ShipmentGetResultVO extends AbstractStructBase
{
    /**
     * The shipmentReference
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public $shipmentReference;
    /**
     * The shipment
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ShipmentVO
     */
    public $shipment;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ShipmentGetResultVO
     * @uses ShipmentGetResultVO::setShipmentReference()
     * @uses ShipmentGetResultVO::setShipment()
     * @uses ShipmentGetResultVO::setError()
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @param \DPDSDK\Shipment\StructType\ShipmentVO $shipment
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null, \DPDSDK\Shipment\StructType\ShipmentVO $shipment = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setShipmentReference($shipmentReference)
            ->setShipment($shipment)
            ->setError($error);
    }
    /**
     * Get shipmentReference value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO|null
     */
    public function getShipmentReference()
    {
        return $this->shipmentReference;
    }
    /**
     * Set shipmentReference value
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResultVO
     */
    public function setShipmentReference(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null)
    {
        $this->shipmentReference = $shipmentReference;
        return $this;
    }
    /**
     * Get shipment value
     * @return \DPDSDK\Shipment\StructType\ShipmentVO|null
     */
    public function getShipment()
    {
        return $this->shipment;
    }
    /**
     * Set shipment value
     * @param \DPDSDK\Shipment\StructType\ShipmentVO $shipment
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResultVO
     */
    public function setShipment(\DPDSDK\Shipment\StructType\ShipmentVO $shipment = null)
    {
        $this->shipment = $shipment;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResultVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
