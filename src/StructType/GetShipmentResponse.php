<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getShipmentResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:getShipmentResponse
 * @subpackage Structs
 */
class GetShipmentResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ShipmentGetResponseVO
     */
    public $result;
    /**
     * Constructor method for getShipmentResponse
     * @uses GetShipmentResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ShipmentGetResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ShipmentGetResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ShipmentGetResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ShipmentGetResponseVO $result
     * @return \DPDSDK\Shipment\StructType\GetShipmentResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ShipmentGetResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
