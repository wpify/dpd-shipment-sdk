<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PredictEmailVO StructType
 * @subpackage Structs
 */
class PredictEmailVO extends AbstractStructBase
{
    /**
     * The email
     * @var string
     */
    public $email;
    /**
     * Constructor method for PredictEmailVO
     * @uses PredictEmailVO::setEmail()
     * @param string $email
     */
    public function __construct($email = null)
    {
        $this
            ->setEmail($email);
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \DPDSDK\Shipment\StructType\PredictEmailVO
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
}
