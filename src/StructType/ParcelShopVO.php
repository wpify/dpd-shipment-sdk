<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopVO StructType
 * @subpackage Structs
 */
class ParcelShopVO extends AbstractStructBase
{
    /**
     * The parcelShopId
     * @var string
     */
    public $parcelShopId;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyName;
    /**
     * The street
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $street;
    /**
     * The houseNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $houseNo;
    /**
     * The state
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $state;
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $countryCode;
    /**
     * The zipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $zipCode;
    /**
     * The city
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $city;
    /**
     * The townName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $townName;
    /**
     * The telNumber
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $telNumber;
    /**
     * The faxNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $faxNo;
    /**
     * The emailAddress
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $emailAddress;
    /**
     * The homepage
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $homepage;
    /**
     * The openingHours
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO[]
     */
    public $openingHours;
    /**
     * The holiday
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelShopHolidayVO[]
     */
    public $holiday;
    /**
     * The extraInfo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $extraInfo;
    /**
     * The consigneePickupAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $consigneePickupAllowed;
    /**
     * The returnAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $returnAllowed;
    /**
     * The expressAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $expressAllowed;
    /**
     * The expressPickupTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $expressPickupTime;
    /**
     * The distance
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $distance;
    /**
     * The longitude
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $longitude;
    /**
     * The latitude
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $latitude;
    /**
     * The coordinateX
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $coordinateX;
    /**
     * The coordinateY
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $coordinateY;
    /**
     * The coordinateZ
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $coordinateZ;
    /**
     * The codAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codAllowed;
    /**
     * The codPaymentType
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $codPaymentType;
    /**
     * The prepaidAllowed
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $prepaidAllowed;
    /**
     * Constructor method for ParcelShopVO
     * @uses ParcelShopVO::setParcelShopId()
     * @uses ParcelShopVO::setCompanyName()
     * @uses ParcelShopVO::setStreet()
     * @uses ParcelShopVO::setHouseNo()
     * @uses ParcelShopVO::setState()
     * @uses ParcelShopVO::setCountryCode()
     * @uses ParcelShopVO::setZipCode()
     * @uses ParcelShopVO::setCity()
     * @uses ParcelShopVO::setTownName()
     * @uses ParcelShopVO::setTelNumber()
     * @uses ParcelShopVO::setFaxNo()
     * @uses ParcelShopVO::setEmailAddress()
     * @uses ParcelShopVO::setHomepage()
     * @uses ParcelShopVO::setOpeningHours()
     * @uses ParcelShopVO::setHoliday()
     * @uses ParcelShopVO::setExtraInfo()
     * @uses ParcelShopVO::setConsigneePickupAllowed()
     * @uses ParcelShopVO::setReturnAllowed()
     * @uses ParcelShopVO::setExpressAllowed()
     * @uses ParcelShopVO::setExpressPickupTime()
     * @uses ParcelShopVO::setDistance()
     * @uses ParcelShopVO::setLongitude()
     * @uses ParcelShopVO::setLatitude()
     * @uses ParcelShopVO::setCoordinateX()
     * @uses ParcelShopVO::setCoordinateY()
     * @uses ParcelShopVO::setCoordinateZ()
     * @uses ParcelShopVO::setCodAllowed()
     * @uses ParcelShopVO::setCodPaymentType()
     * @uses ParcelShopVO::setPrepaidAllowed()
     * @param string $parcelShopId
     * @param string $companyName
     * @param string $street
     * @param string $houseNo
     * @param string $state
     * @param string $countryCode
     * @param string $zipCode
     * @param string $city
     * @param string $townName
     * @param string $telNumber
     * @param string $faxNo
     * @param string $emailAddress
     * @param string $homepage
     * @param \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO[] $openingHours
     * @param \DPDSDK\Shipment\StructType\ParcelShopHolidayVO[] $holiday
     * @param string $extraInfo
     * @param string $consigneePickupAllowed
     * @param string $returnAllowed
     * @param string $expressAllowed
     * @param string $expressPickupTime
     * @param float $distance
     * @param float $longitude
     * @param float $latitude
     * @param float $coordinateX
     * @param float $coordinateY
     * @param float $coordinateZ
     * @param string $codAllowed
     * @param string $codPaymentType
     * @param string $prepaidAllowed
     */
    public function __construct($parcelShopId = null, $companyName = null, $street = null, $houseNo = null, $state = null, $countryCode = null, $zipCode = null, $city = null, $townName = null, $telNumber = null, $faxNo = null, $emailAddress = null, $homepage = null, array $openingHours = array(), array $holiday = array(), $extraInfo = null, $consigneePickupAllowed = null, $returnAllowed = null, $expressAllowed = null, $expressPickupTime = null, $distance = null, $longitude = null, $latitude = null, $coordinateX = null, $coordinateY = null, $coordinateZ = null, $codAllowed = null, $codPaymentType = null, $prepaidAllowed = null)
    {
        $this
            ->setParcelShopId($parcelShopId)
            ->setCompanyName($companyName)
            ->setStreet($street)
            ->setHouseNo($houseNo)
            ->setState($state)
            ->setCountryCode($countryCode)
            ->setZipCode($zipCode)
            ->setCity($city)
            ->setTownName($townName)
            ->setTelNumber($telNumber)
            ->setFaxNo($faxNo)
            ->setEmailAddress($emailAddress)
            ->setHomepage($homepage)
            ->setOpeningHours($openingHours)
            ->setHoliday($holiday)
            ->setExtraInfo($extraInfo)
            ->setConsigneePickupAllowed($consigneePickupAllowed)
            ->setReturnAllowed($returnAllowed)
            ->setExpressAllowed($expressAllowed)
            ->setExpressPickupTime($expressPickupTime)
            ->setDistance($distance)
            ->setLongitude($longitude)
            ->setLatitude($latitude)
            ->setCoordinateX($coordinateX)
            ->setCoordinateY($coordinateY)
            ->setCoordinateZ($coordinateZ)
            ->setCodAllowed($codAllowed)
            ->setCodPaymentType($codPaymentType)
            ->setPrepaidAllowed($prepaidAllowed);
    }
    /**
     * Get parcelShopId value
     * @return string|null
     */
    public function getParcelShopId()
    {
        return $this->parcelShopId;
    }
    /**
     * Set parcelShopId value
     * @param string $parcelShopId
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setParcelShopId($parcelShopId = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopId) && !is_string($parcelShopId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopId, true), gettype($parcelShopId)), __LINE__);
        }
        $this->parcelShopId = $parcelShopId;
        return $this;
    }
    /**
     * Get companyName value
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    /**
     * Set companyName value
     * @param string $companyName
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCompanyName($companyName = null)
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyName, true), gettype($companyName)), __LINE__);
        }
        $this->companyName = $companyName;
        return $this;
    }
    /**
     * Get street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * Set street value
     * @param string $street
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        $this->street = $street;
        return $this;
    }
    /**
     * Get houseNo value
     * @return string|null
     */
    public function getHouseNo()
    {
        return $this->houseNo;
    }
    /**
     * Set houseNo value
     * @param string $houseNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setHouseNo($houseNo = null)
    {
        // validation for constraint: string
        if (!is_null($houseNo) && !is_string($houseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseNo, true), gettype($houseNo)), __LINE__);
        }
        $this->houseNo = $houseNo;
        return $this;
    }
    /**
     * Get state value
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }
    /**
     * Set state value
     * @param string $state
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setState($state = null)
    {
        // validation for constraint: string
        if (!is_null($state) && !is_string($state)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($state, true), gettype($state)), __LINE__);
        }
        $this->state = $state;
        return $this;
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        return $this;
    }
    /**
     * Get zipCode value
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
    /**
     * Set zipCode value
     * @param string $zipCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        $this->zipCode = $zipCode;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get townName value
     * @return string|null
     */
    public function getTownName()
    {
        return $this->townName;
    }
    /**
     * Set townName value
     * @param string $townName
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setTownName($townName = null)
    {
        // validation for constraint: string
        if (!is_null($townName) && !is_string($townName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($townName, true), gettype($townName)), __LINE__);
        }
        $this->townName = $townName;
        return $this;
    }
    /**
     * Get telNumber value
     * @return string|null
     */
    public function getTelNumber()
    {
        return $this->telNumber;
    }
    /**
     * Set telNumber value
     * @param string $telNumber
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setTelNumber($telNumber = null)
    {
        // validation for constraint: string
        if (!is_null($telNumber) && !is_string($telNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($telNumber, true), gettype($telNumber)), __LINE__);
        }
        $this->telNumber = $telNumber;
        return $this;
    }
    /**
     * Get faxNo value
     * @return string|null
     */
    public function getFaxNo()
    {
        return $this->faxNo;
    }
    /**
     * Set faxNo value
     * @param string $faxNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setFaxNo($faxNo = null)
    {
        // validation for constraint: string
        if (!is_null($faxNo) && !is_string($faxNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($faxNo, true), gettype($faxNo)), __LINE__);
        }
        $this->faxNo = $faxNo;
        return $this;
    }
    /**
     * Get emailAddress value
     * @return string|null
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }
    /**
     * Set emailAddress value
     * @param string $emailAddress
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setEmailAddress($emailAddress = null)
    {
        // validation for constraint: string
        if (!is_null($emailAddress) && !is_string($emailAddress)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($emailAddress, true), gettype($emailAddress)), __LINE__);
        }
        $this->emailAddress = $emailAddress;
        return $this;
    }
    /**
     * Get homepage value
     * @return string|null
     */
    public function getHomepage()
    {
        return $this->homepage;
    }
    /**
     * Set homepage value
     * @param string $homepage
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setHomepage($homepage = null)
    {
        // validation for constraint: string
        if (!is_null($homepage) && !is_string($homepage)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($homepage, true), gettype($homepage)), __LINE__);
        }
        $this->homepage = $homepage;
        return $this;
    }
    /**
     * Get openingHours value
     * @return \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO[]|null
     */
    public function getOpeningHours()
    {
        return $this->openingHours;
    }
    /**
     * This method is responsible for validating the values passed to the setOpeningHours method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOpeningHours method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOpeningHoursForArrayConstraintsFromSetOpeningHours(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $parcelShopVOOpeningHoursItem) {
            // validation for constraint: itemType
            if (!$parcelShopVOOpeningHoursItem instanceof \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO) {
                $invalidValues[] = is_object($parcelShopVOOpeningHoursItem) ? get_class($parcelShopVOOpeningHoursItem) : sprintf('%s(%s)', gettype($parcelShopVOOpeningHoursItem), var_export($parcelShopVOOpeningHoursItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The openingHours property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set openingHours value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO[] $openingHours
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setOpeningHours(array $openingHours = array())
    {
        // validation for constraint: array
        if ('' !== ($openingHoursArrayErrorMessage = self::validateOpeningHoursForArrayConstraintsFromSetOpeningHours($openingHours))) {
            throw new \InvalidArgumentException($openingHoursArrayErrorMessage, __LINE__);
        }
        $this->openingHours = $openingHours;
        return $this;
    }
    /**
     * Add item to openingHours value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO $item
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function addToOpeningHours(\DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO) {
            throw new \InvalidArgumentException(sprintf('The openingHours property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopOpeningHoursVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->openingHours[] = $item;
        return $this;
    }
    /**
     * Get holiday value
     * @return \DPDSDK\Shipment\StructType\ParcelShopHolidayVO[]|null
     */
    public function getHoliday()
    {
        return $this->holiday;
    }
    /**
     * This method is responsible for validating the values passed to the setHoliday method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHoliday method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHolidayForArrayConstraintsFromSetHoliday(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $parcelShopVOHolidayItem) {
            // validation for constraint: itemType
            if (!$parcelShopVOHolidayItem instanceof \DPDSDK\Shipment\StructType\ParcelShopHolidayVO) {
                $invalidValues[] = is_object($parcelShopVOHolidayItem) ? get_class($parcelShopVOHolidayItem) : sprintf('%s(%s)', gettype($parcelShopVOHolidayItem), var_export($parcelShopVOHolidayItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The holiday property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopHolidayVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set holiday value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopHolidayVO[] $holiday
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setHoliday(array $holiday = array())
    {
        // validation for constraint: array
        if ('' !== ($holidayArrayErrorMessage = self::validateHolidayForArrayConstraintsFromSetHoliday($holiday))) {
            throw new \InvalidArgumentException($holidayArrayErrorMessage, __LINE__);
        }
        $this->holiday = $holiday;
        return $this;
    }
    /**
     * Add item to holiday value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelShopHolidayVO $item
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function addToHoliday(\DPDSDK\Shipment\StructType\ParcelShopHolidayVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelShopHolidayVO) {
            throw new \InvalidArgumentException(sprintf('The holiday property can only contain items of type \DPDSDK\Shipment\StructType\ParcelShopHolidayVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->holiday[] = $item;
        return $this;
    }
    /**
     * Get extraInfo value
     * @return string|null
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }
    /**
     * Set extraInfo value
     * @param string $extraInfo
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setExtraInfo($extraInfo = null)
    {
        // validation for constraint: string
        if (!is_null($extraInfo) && !is_string($extraInfo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($extraInfo, true), gettype($extraInfo)), __LINE__);
        }
        $this->extraInfo = $extraInfo;
        return $this;
    }
    /**
     * Get consigneePickupAllowed value
     * @return string|null
     */
    public function getConsigneePickupAllowed()
    {
        return $this->consigneePickupAllowed;
    }
    /**
     * Set consigneePickupAllowed value
     * @param string $consigneePickupAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setConsigneePickupAllowed($consigneePickupAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($consigneePickupAllowed) && !is_string($consigneePickupAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($consigneePickupAllowed, true), gettype($consigneePickupAllowed)), __LINE__);
        }
        $this->consigneePickupAllowed = $consigneePickupAllowed;
        return $this;
    }
    /**
     * Get returnAllowed value
     * @return string|null
     */
    public function getReturnAllowed()
    {
        return $this->returnAllowed;
    }
    /**
     * Set returnAllowed value
     * @param string $returnAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setReturnAllowed($returnAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($returnAllowed) && !is_string($returnAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($returnAllowed, true), gettype($returnAllowed)), __LINE__);
        }
        $this->returnAllowed = $returnAllowed;
        return $this;
    }
    /**
     * Get expressAllowed value
     * @return string|null
     */
    public function getExpressAllowed()
    {
        return $this->expressAllowed;
    }
    /**
     * Set expressAllowed value
     * @param string $expressAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setExpressAllowed($expressAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($expressAllowed) && !is_string($expressAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expressAllowed, true), gettype($expressAllowed)), __LINE__);
        }
        $this->expressAllowed = $expressAllowed;
        return $this;
    }
    /**
     * Get expressPickupTime value
     * @return string|null
     */
    public function getExpressPickupTime()
    {
        return $this->expressPickupTime;
    }
    /**
     * Set expressPickupTime value
     * @param string $expressPickupTime
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setExpressPickupTime($expressPickupTime = null)
    {
        // validation for constraint: string
        if (!is_null($expressPickupTime) && !is_string($expressPickupTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expressPickupTime, true), gettype($expressPickupTime)), __LINE__);
        }
        $this->expressPickupTime = $expressPickupTime;
        return $this;
    }
    /**
     * Get distance value
     * @return float|null
     */
    public function getDistance()
    {
        return $this->distance;
    }
    /**
     * Set distance value
     * @param float $distance
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setDistance($distance = null)
    {
        // validation for constraint: float
        if (!is_null($distance) && !(is_float($distance) || is_numeric($distance))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($distance, true), gettype($distance)), __LINE__);
        }
        $this->distance = $distance;
        return $this;
    }
    /**
     * Get longitude value
     * @return float|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
    /**
     * Set longitude value
     * @param float $longitude
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setLongitude($longitude = null)
    {
        // validation for constraint: float
        if (!is_null($longitude) && !(is_float($longitude) || is_numeric($longitude))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($longitude, true), gettype($longitude)), __LINE__);
        }
        $this->longitude = $longitude;
        return $this;
    }
    /**
     * Get latitude value
     * @return float|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }
    /**
     * Set latitude value
     * @param float $latitude
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setLatitude($latitude = null)
    {
        // validation for constraint: float
        if (!is_null($latitude) && !(is_float($latitude) || is_numeric($latitude))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($latitude, true), gettype($latitude)), __LINE__);
        }
        $this->latitude = $latitude;
        return $this;
    }
    /**
     * Get coordinateX value
     * @return float|null
     */
    public function getCoordinateX()
    {
        return $this->coordinateX;
    }
    /**
     * Set coordinateX value
     * @param float $coordinateX
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCoordinateX($coordinateX = null)
    {
        // validation for constraint: float
        if (!is_null($coordinateX) && !(is_float($coordinateX) || is_numeric($coordinateX))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($coordinateX, true), gettype($coordinateX)), __LINE__);
        }
        $this->coordinateX = $coordinateX;
        return $this;
    }
    /**
     * Get coordinateY value
     * @return float|null
     */
    public function getCoordinateY()
    {
        return $this->coordinateY;
    }
    /**
     * Set coordinateY value
     * @param float $coordinateY
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCoordinateY($coordinateY = null)
    {
        // validation for constraint: float
        if (!is_null($coordinateY) && !(is_float($coordinateY) || is_numeric($coordinateY))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($coordinateY, true), gettype($coordinateY)), __LINE__);
        }
        $this->coordinateY = $coordinateY;
        return $this;
    }
    /**
     * Get coordinateZ value
     * @return float|null
     */
    public function getCoordinateZ()
    {
        return $this->coordinateZ;
    }
    /**
     * Set coordinateZ value
     * @param float $coordinateZ
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCoordinateZ($coordinateZ = null)
    {
        // validation for constraint: float
        if (!is_null($coordinateZ) && !(is_float($coordinateZ) || is_numeric($coordinateZ))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($coordinateZ, true), gettype($coordinateZ)), __LINE__);
        }
        $this->coordinateZ = $coordinateZ;
        return $this;
    }
    /**
     * Get codAllowed value
     * @return string|null
     */
    public function getCodAllowed()
    {
        return $this->codAllowed;
    }
    /**
     * Set codAllowed value
     * @param string $codAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCodAllowed($codAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($codAllowed) && !is_string($codAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codAllowed, true), gettype($codAllowed)), __LINE__);
        }
        $this->codAllowed = $codAllowed;
        return $this;
    }
    /**
     * Get codPaymentType value
     * @return string|null
     */
    public function getCodPaymentType()
    {
        return $this->codPaymentType;
    }
    /**
     * Set codPaymentType value
     * @param string $codPaymentType
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setCodPaymentType($codPaymentType = null)
    {
        // validation for constraint: string
        if (!is_null($codPaymentType) && !is_string($codPaymentType)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($codPaymentType, true), gettype($codPaymentType)), __LINE__);
        }
        $this->codPaymentType = $codPaymentType;
        return $this;
    }
    /**
     * Get prepaidAllowed value
     * @return string|null
     */
    public function getPrepaidAllowed()
    {
        return $this->prepaidAllowed;
    }
    /**
     * Set prepaidAllowed value
     * @param string $prepaidAllowed
     * @return \DPDSDK\Shipment\StructType\ParcelShopVO
     */
    public function setPrepaidAllowed($prepaidAllowed = null)
    {
        // validation for constraint: string
        if (!is_null($prepaidAllowed) && !is_string($prepaidAllowed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($prepaidAllowed, true), gettype($prepaidAllowed)), __LINE__);
        }
        $this->prepaidAllowed = $prepaidAllowed;
        return $this;
    }
}
