<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelVO StructType
 * @subpackage Structs
 */
class ParcelVO extends AbstractStructBase
{
    /**
     * The parcelId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $parcelId;
    /**
     * The parcelNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelNo;
    /**
     * The parcelReferenceNumber
     * @var string
     */
    public $parcelReferenceNumber;
    /**
     * The parcelReferenceNumber1
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber1;
    /**
     * The parcelReferenceNumber2
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber2;
    /**
     * The parcelReferenceNumber3
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $parcelReferenceNumber3;
    /**
     * The dimensionsHeight
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $dimensionsHeight;
    /**
     * The dimensionsWidth
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $dimensionsWidth;
    /**
     * The dimensionsLength
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $dimensionsLength;
    /**
     * The weight
     * @var float
     */
    public $weight;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $description;
    /**
     * The exchange
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $exchange;
    /**
     * Constructor method for ParcelVO
     * @uses ParcelVO::setParcelId()
     * @uses ParcelVO::setParcelNo()
     * @uses ParcelVO::setParcelReferenceNumber()
     * @uses ParcelVO::setParcelReferenceNumber1()
     * @uses ParcelVO::setParcelReferenceNumber2()
     * @uses ParcelVO::setParcelReferenceNumber3()
     * @uses ParcelVO::setDimensionsHeight()
     * @uses ParcelVO::setDimensionsWidth()
     * @uses ParcelVO::setDimensionsLength()
     * @uses ParcelVO::setWeight()
     * @uses ParcelVO::setDescription()
     * @uses ParcelVO::setExchange()
     * @param int $parcelId
     * @param string $parcelNo
     * @param string $parcelReferenceNumber
     * @param string $parcelReferenceNumber1
     * @param string $parcelReferenceNumber2
     * @param string $parcelReferenceNumber3
     * @param float $dimensionsHeight
     * @param float $dimensionsWidth
     * @param float $dimensionsLength
     * @param float $weight
     * @param string $description
     * @param string $exchange
     */
    public function __construct($parcelId = null, $parcelNo = null, $parcelReferenceNumber = null, $parcelReferenceNumber1 = null, $parcelReferenceNumber2 = null, $parcelReferenceNumber3 = null, $dimensionsHeight = null, $dimensionsWidth = null, $dimensionsLength = null, $weight = null, $description = null, $exchange = null)
    {
        $this
            ->setParcelId($parcelId)
            ->setParcelNo($parcelNo)
            ->setParcelReferenceNumber($parcelReferenceNumber)
            ->setParcelReferenceNumber1($parcelReferenceNumber1)
            ->setParcelReferenceNumber2($parcelReferenceNumber2)
            ->setParcelReferenceNumber3($parcelReferenceNumber3)
            ->setDimensionsHeight($dimensionsHeight)
            ->setDimensionsWidth($dimensionsWidth)
            ->setDimensionsLength($dimensionsLength)
            ->setWeight($weight)
            ->setDescription($description)
            ->setExchange($exchange);
    }
    /**
     * Get parcelId value
     * @return int|null
     */
    public function getParcelId()
    {
        return $this->parcelId;
    }
    /**
     * Set parcelId value
     * @param int $parcelId
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelId($parcelId = null)
    {
        // validation for constraint: int
        if (!is_null($parcelId) && !(is_int($parcelId) || ctype_digit($parcelId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($parcelId, true), gettype($parcelId)), __LINE__);
        }
        $this->parcelId = $parcelId;
        return $this;
    }
    /**
     * Get parcelNo value
     * @return string|null
     */
    public function getParcelNo()
    {
        return $this->parcelNo;
    }
    /**
     * Set parcelNo value
     * @param string $parcelNo
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelNo($parcelNo = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNo) && !is_string($parcelNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNo, true), gettype($parcelNo)), __LINE__);
        }
        $this->parcelNo = $parcelNo;
        return $this;
    }
    /**
     * Get parcelReferenceNumber value
     * @return string|null
     */
    public function getParcelReferenceNumber()
    {
        return $this->parcelReferenceNumber;
    }
    /**
     * Set parcelReferenceNumber value
     * @param string $parcelReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelReferenceNumber($parcelReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber) && !is_string($parcelReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber, true), gettype($parcelReferenceNumber)), __LINE__);
        }
        $this->parcelReferenceNumber = $parcelReferenceNumber;
        return $this;
    }
    /**
     * Get parcelReferenceNumber1 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber1()
    {
        return isset($this->parcelReferenceNumber1) ? $this->parcelReferenceNumber1 : null;
    }
    /**
     * Set parcelReferenceNumber1 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber1
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelReferenceNumber1($parcelReferenceNumber1 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber1) && !is_string($parcelReferenceNumber1)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber1, true), gettype($parcelReferenceNumber1)), __LINE__);
        }
        if (is_null($parcelReferenceNumber1) || (is_array($parcelReferenceNumber1) && empty($parcelReferenceNumber1))) {
            unset($this->parcelReferenceNumber1);
        } else {
            $this->parcelReferenceNumber1 = $parcelReferenceNumber1;
        }
        return $this;
    }
    /**
     * Get parcelReferenceNumber2 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber2()
    {
        return isset($this->parcelReferenceNumber2) ? $this->parcelReferenceNumber2 : null;
    }
    /**
     * Set parcelReferenceNumber2 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber2
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelReferenceNumber2($parcelReferenceNumber2 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber2) && !is_string($parcelReferenceNumber2)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber2, true), gettype($parcelReferenceNumber2)), __LINE__);
        }
        if (is_null($parcelReferenceNumber2) || (is_array($parcelReferenceNumber2) && empty($parcelReferenceNumber2))) {
            unset($this->parcelReferenceNumber2);
        } else {
            $this->parcelReferenceNumber2 = $parcelReferenceNumber2;
        }
        return $this;
    }
    /**
     * Get parcelReferenceNumber3 value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getParcelReferenceNumber3()
    {
        return isset($this->parcelReferenceNumber3) ? $this->parcelReferenceNumber3 : null;
    }
    /**
     * Set parcelReferenceNumber3 value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $parcelReferenceNumber3
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setParcelReferenceNumber3($parcelReferenceNumber3 = null)
    {
        // validation for constraint: string
        if (!is_null($parcelReferenceNumber3) && !is_string($parcelReferenceNumber3)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelReferenceNumber3, true), gettype($parcelReferenceNumber3)), __LINE__);
        }
        if (is_null($parcelReferenceNumber3) || (is_array($parcelReferenceNumber3) && empty($parcelReferenceNumber3))) {
            unset($this->parcelReferenceNumber3);
        } else {
            $this->parcelReferenceNumber3 = $parcelReferenceNumber3;
        }
        return $this;
    }
    /**
     * Get dimensionsHeight value
     * @return float|null
     */
    public function getDimensionsHeight()
    {
        return $this->dimensionsHeight;
    }
    /**
     * Set dimensionsHeight value
     * @param float $dimensionsHeight
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setDimensionsHeight($dimensionsHeight = null)
    {
        // validation for constraint: float
        if (!is_null($dimensionsHeight) && !(is_float($dimensionsHeight) || is_numeric($dimensionsHeight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($dimensionsHeight, true), gettype($dimensionsHeight)), __LINE__);
        }
        $this->dimensionsHeight = $dimensionsHeight;
        return $this;
    }
    /**
     * Get dimensionsWidth value
     * @return float|null
     */
    public function getDimensionsWidth()
    {
        return $this->dimensionsWidth;
    }
    /**
     * Set dimensionsWidth value
     * @param float $dimensionsWidth
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setDimensionsWidth($dimensionsWidth = null)
    {
        // validation for constraint: float
        if (!is_null($dimensionsWidth) && !(is_float($dimensionsWidth) || is_numeric($dimensionsWidth))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($dimensionsWidth, true), gettype($dimensionsWidth)), __LINE__);
        }
        $this->dimensionsWidth = $dimensionsWidth;
        return $this;
    }
    /**
     * Get dimensionsLength value
     * @return float|null
     */
    public function getDimensionsLength()
    {
        return $this->dimensionsLength;
    }
    /**
     * Set dimensionsLength value
     * @param float $dimensionsLength
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setDimensionsLength($dimensionsLength = null)
    {
        // validation for constraint: float
        if (!is_null($dimensionsLength) && !(is_float($dimensionsLength) || is_numeric($dimensionsLength))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($dimensionsLength, true), gettype($dimensionsLength)), __LINE__);
        }
        $this->dimensionsLength = $dimensionsLength;
        return $this;
    }
    /**
     * Get weight value
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * Set weight value
     * @param float $weight
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        $this->weight = $weight;
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setDescription($description = null)
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        return $this;
    }
    /**
     * Get exchange value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getExchange()
    {
        return isset($this->exchange) ? $this->exchange : null;
    }
    /**
     * Set exchange value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $exchange
     * @return \DPDSDK\Shipment\StructType\ParcelVO
     */
    public function setExchange($exchange = null)
    {
        // validation for constraint: string
        if (!is_null($exchange) && !is_string($exchange)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($exchange, true), gettype($exchange)), __LINE__);
        }
        if (is_null($exchange) || (is_array($exchange) && empty($exchange))) {
            unset($this->exchange);
        } else {
            $this->exchange = $exchange;
        }
        return $this;
    }
}
