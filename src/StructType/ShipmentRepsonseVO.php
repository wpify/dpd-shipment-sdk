<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentRepsonseVO StructType
 * @subpackage Structs
 */
class ShipmentRepsonseVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The resultList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * @var \DPDSDK\Shipment\StructType\ShipmentResultVO[]
     */
    public $resultList;
    /**
     * Constructor method for ShipmentRepsonseVO
     * @uses ShipmentRepsonseVO::setTransactionId()
     * @uses ShipmentRepsonseVO::setResultList()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\ShipmentResultVO[] $resultList
     */
    public function __construct($transactionId = null, array $resultList = array())
    {
        $this
            ->setTransactionId($transactionId)
            ->setResultList($resultList);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get resultList value
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO[]|null
     */
    public function getResultList()
    {
        return $this->resultList;
    }
    /**
     * This method is responsible for validating the values passed to the setResultList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setResultList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateResultListForArrayConstraintsFromSetResultList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentRepsonseVOResultListItem) {
            // validation for constraint: itemType
            if (!$shipmentRepsonseVOResultListItem instanceof \DPDSDK\Shipment\StructType\ShipmentResultVO) {
                $invalidValues[] = is_object($shipmentRepsonseVOResultListItem) ? get_class($shipmentRepsonseVOResultListItem) : sprintf('%s(%s)', gettype($shipmentRepsonseVOResultListItem), var_export($shipmentRepsonseVOResultListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The resultList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentResultVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set resultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentResultVO[] $resultList
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVO
     */
    public function setResultList(array $resultList = array())
    {
        // validation for constraint: array
        if ('' !== ($resultListArrayErrorMessage = self::validateResultListForArrayConstraintsFromSetResultList($resultList))) {
            throw new \InvalidArgumentException($resultListArrayErrorMessage, __LINE__);
        }
        $this->resultList = $resultList;
        return $this;
    }
    /**
     * Add item to resultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ShipmentResultVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentRepsonseVO
     */
    public function addToResultList(\DPDSDK\Shipment\StructType\ShipmentResultVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ShipmentResultVO) {
            throw new \InvalidArgumentException(sprintf('The resultList property can only contain items of type \DPDSDK\Shipment\StructType\ShipmentResultVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->resultList[] = $item;
        return $this;
    }
}
