<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for MoreServicesShopShipmentVO StructType
 * @subpackage Structs
 */
class MoreServicesShopShipmentVO extends AbstractStructBase
{
    /**
     * The service
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ServiceShopShipmentVO[]
     */
    public $service;
    /**
     * Constructor method for MoreServicesShopShipmentVO
     * @uses MoreServicesShopShipmentVO::setService()
     * @param \DPDSDK\Shipment\StructType\ServiceShopShipmentVO[] $service
     */
    public function __construct(array $service = array())
    {
        $this
            ->setService($service);
    }
    /**
     * Get service value
     * @return \DPDSDK\Shipment\StructType\ServiceShopShipmentVO[]|null
     */
    public function getService()
    {
        return $this->service;
    }
    /**
     * This method is responsible for validating the values passed to the setService method
     * This method is willingly generated in order to preserve the one-line inline validation within the setService method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateServiceForArrayConstraintsFromSetService(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $moreServicesShopShipmentVOServiceItem) {
            // validation for constraint: itemType
            if (!$moreServicesShopShipmentVOServiceItem instanceof \DPDSDK\Shipment\StructType\ServiceShopShipmentVO) {
                $invalidValues[] = is_object($moreServicesShopShipmentVOServiceItem) ? get_class($moreServicesShopShipmentVOServiceItem) : sprintf('%s(%s)', gettype($moreServicesShopShipmentVOServiceItem), var_export($moreServicesShopShipmentVOServiceItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The service property can only contain items of type \DPDSDK\Shipment\StructType\ServiceShopShipmentVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set service value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ServiceShopShipmentVO[] $service
     * @return \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO
     */
    public function setService(array $service = array())
    {
        // validation for constraint: array
        if ('' !== ($serviceArrayErrorMessage = self::validateServiceForArrayConstraintsFromSetService($service))) {
            throw new \InvalidArgumentException($serviceArrayErrorMessage, __LINE__);
        }
        $this->service = $service;
        return $this;
    }
    /**
     * Add item to service value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ServiceShopShipmentVO $item
     * @return \DPDSDK\Shipment\StructType\MoreServicesShopShipmentVO
     */
    public function addToService(\DPDSDK\Shipment\StructType\ServiceShopShipmentVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ServiceShopShipmentVO) {
            throw new \InvalidArgumentException(sprintf('The service property can only contain items of type \DPDSDK\Shipment\StructType\ServiceShopShipmentVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->service[] = $item;
        return $this;
    }
}
