<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ScanVO StructType
 * @subpackage Structs
 */
class ScanVO extends AbstractStructBase
{
    /**
     * The date
     * @var string
     */
    public $date;
    /**
     * The time
     * @var string
     */
    public $time;
    /**
     * The scanCode
     * @var string
     */
    public $scanCode;
    /**
     * The scanDescription
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $scanDescription;
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $countryCode;
    /**
     * The city
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $city;
    /**
     * The zipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $zipCode;
    /**
     * Constructor method for ScanVO
     * @uses ScanVO::setDate()
     * @uses ScanVO::setTime()
     * @uses ScanVO::setScanCode()
     * @uses ScanVO::setScanDescription()
     * @uses ScanVO::setCountryCode()
     * @uses ScanVO::setCity()
     * @uses ScanVO::setZipCode()
     * @param string $date
     * @param string $time
     * @param string $scanCode
     * @param string $scanDescription
     * @param string $countryCode
     * @param string $city
     * @param string $zipCode
     */
    public function __construct($date = null, $time = null, $scanCode = null, $scanDescription = null, $countryCode = null, $city = null, $zipCode = null)
    {
        $this
            ->setDate($date)
            ->setTime($time)
            ->setScanCode($scanCode)
            ->setScanDescription($scanDescription)
            ->setCountryCode($countryCode)
            ->setCity($city)
            ->setZipCode($zipCode);
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setDate($date = null)
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        return $this;
    }
    /**
     * Get time value
     * @return string|null
     */
    public function getTime()
    {
        return $this->time;
    }
    /**
     * Set time value
     * @param string $time
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setTime($time = null)
    {
        // validation for constraint: string
        if (!is_null($time) && !is_string($time)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($time, true), gettype($time)), __LINE__);
        }
        $this->time = $time;
        return $this;
    }
    /**
     * Get scanCode value
     * @return string|null
     */
    public function getScanCode()
    {
        return $this->scanCode;
    }
    /**
     * Set scanCode value
     * @param string $scanCode
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setScanCode($scanCode = null)
    {
        // validation for constraint: string
        if (!is_null($scanCode) && !is_string($scanCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($scanCode, true), gettype($scanCode)), __LINE__);
        }
        $this->scanCode = $scanCode;
        return $this;
    }
    /**
     * Get scanDescription value
     * @return string|null
     */
    public function getScanDescription()
    {
        return $this->scanDescription;
    }
    /**
     * Set scanDescription value
     * @param string $scanDescription
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setScanDescription($scanDescription = null)
    {
        // validation for constraint: string
        if (!is_null($scanDescription) && !is_string($scanDescription)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($scanDescription, true), gettype($scanDescription)), __LINE__);
        }
        $this->scanDescription = $scanDescription;
        return $this;
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get zipCode value
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
    /**
     * Set zipCode value
     * @param string $zipCode
     * @return \DPDSDK\Shipment\StructType\ScanVO
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        $this->zipCode = $zipCode;
        return $this;
    }
}
