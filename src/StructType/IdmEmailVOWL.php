<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IdmEmailVOWL StructType
 * @subpackage Structs
 */
class IdmEmailVOWL extends AbstractStructBase
{
    /**
     * The email
     * @var string
     */
    public $email;
    /**
     * The rule
     * @var string
     */
    public $rule;
    /**
     * Constructor method for IdmEmailVOWL
     * @uses IdmEmailVOWL::setEmail()
     * @uses IdmEmailVOWL::setRule()
     * @param string $email
     * @param string $rule
     */
    public function __construct($email = null, $rule = null)
    {
        $this
            ->setEmail($email)
            ->setRule($rule);
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \DPDSDK\Shipment\StructType\IdmEmailVOWL
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
    /**
     * Get rule value
     * @return string|null
     */
    public function getRule()
    {
        return $this->rule;
    }
    /**
     * Set rule value
     * @param string $rule
     * @return \DPDSDK\Shipment\StructType\IdmEmailVOWL
     */
    public function setRule($rule = null)
    {
        // validation for constraint: string
        if (!is_null($rule) && !is_string($rule)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($rule, true), gettype($rule)), __LINE__);
        }
        $this->rule = $rule;
        return $this;
    }
}
