<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ErrorVO StructType
 * @subpackage Structs
 */
class ErrorVO extends AbstractStructBase
{
    /**
     * The code
     * @var int
     */
    public $code;
    /**
     * The text
     * @var string
     */
    public $text;
    /**
     * The solution
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $solution;
    /**
     * Constructor method for ErrorVO
     * @uses ErrorVO::setCode()
     * @uses ErrorVO::setText()
     * @uses ErrorVO::setSolution()
     * @param int $code
     * @param string $text
     * @param string $solution
     */
    public function __construct($code = null, $text = null, $solution = null)
    {
        $this
            ->setCode($code)
            ->setText($text)
            ->setSolution($solution);
    }
    /**
     * Get code value
     * @return int|null
     */
    public function getCode()
    {
        return $this->code;
    }
    /**
     * Set code value
     * @param int $code
     * @return \DPDSDK\Shipment\StructType\ErrorVO
     */
    public function setCode($code = null)
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        $this->code = $code;
        return $this;
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \DPDSDK\Shipment\StructType\ErrorVO
     */
    public function setText($text = null)
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        return $this;
    }
    /**
     * Get solution value
     * @return string|null
     */
    public function getSolution()
    {
        return $this->solution;
    }
    /**
     * Set solution value
     * @param string $solution
     * @return \DPDSDK\Shipment\StructType\ErrorVO
     */
    public function setSolution($solution = null)
    {
        // validation for constraint: string
        if (!is_null($solution) && !is_string($solution)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($solution, true), gettype($solution)), __LINE__);
        }
        $this->solution = $solution;
        return $this;
    }
}
