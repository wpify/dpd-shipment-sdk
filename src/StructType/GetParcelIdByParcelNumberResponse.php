<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getParcelIdByParcelNumberResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:getParcelIdByParcelNumberResponse
 * @subpackage Structs
 */
class GetParcelIdByParcelNumberResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ParcelResponseVO
     */
    public $result;
    /**
     * Constructor method for getParcelIdByParcelNumberResponse
     * @uses GetParcelIdByParcelNumberResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ParcelResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ParcelResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ParcelResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ParcelResponseVO $result
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumberResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ParcelResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
