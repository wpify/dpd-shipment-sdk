<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for IDCheckVO StructType
 * @subpackage Structs
 */
class IDCheckVO extends AbstractStructBase
{
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * Constructor method for IDCheckVO
     * @uses IDCheckVO::setId()
     * @uses IDCheckVO::setName()
     * @param string $id
     * @param string $name
     */
    public function __construct($id = null, $name = null)
    {
        $this
            ->setId($id)
            ->setName($name);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \DPDSDK\Shipment\StructType\IDCheckVO
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \DPDSDK\Shipment\StructType\IDCheckVO
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
}
