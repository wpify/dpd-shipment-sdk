<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AdditionalServiceVOWL StructType
 * @subpackage Structs
 */
class AdditionalServiceVOWL extends AdditionalServiceVO
{
    /**
     * The idmSms
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\IdmSmsVOWL
     */
    public $idmSms;
    /**
     * The idmEmail
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\IdmEmailVOWL
     */
    public $idmEmail;
    /**
     * Constructor method for AdditionalServiceVOWL
     * @uses AdditionalServiceVOWL::setIdmSms()
     * @uses AdditionalServiceVOWL::setIdmEmail()
     * @param \DPDSDK\Shipment\StructType\IdmSmsVOWL $idmSms
     * @param \DPDSDK\Shipment\StructType\IdmEmailVOWL $idmEmail
     */
    public function __construct(\DPDSDK\Shipment\StructType\IdmSmsVOWL $idmSms = null, \DPDSDK\Shipment\StructType\IdmEmailVOWL $idmEmail = null)
    {
        $this
            ->setIdmSms($idmSms)
            ->setIdmEmail($idmEmail);
    }
    /**
     * Get idmSms value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\IdmSmsVOWL|null
     */
    public function getIdmSms()
    {
        return isset($this->idmSms) ? $this->idmSms : null;
    }
    /**
     * Set idmSms value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\IdmSmsVOWL $idmSms
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVOWL
     */
    public function setIdmSms(\DPDSDK\Shipment\StructType\IdmSmsVOWL $idmSms = null)
    {
        if (is_null($idmSms) || (is_array($idmSms) && empty($idmSms))) {
            unset($this->idmSms);
        } else {
            $this->idmSms = $idmSms;
        }
        return $this;
    }
    /**
     * Get idmEmail value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\IdmEmailVOWL|null
     */
    public function getIdmEmail()
    {
        return isset($this->idmEmail) ? $this->idmEmail : null;
    }
    /**
     * Set idmEmail value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param \DPDSDK\Shipment\StructType\IdmEmailVOWL $idmEmail
     * @return \DPDSDK\Shipment\StructType\AdditionalServiceVOWL
     */
    public function setIdmEmail(\DPDSDK\Shipment\StructType\IdmEmailVOWL $idmEmail = null)
    {
        if (is_null($idmEmail) || (is_array($idmEmail) && empty($idmEmail))) {
            unset($this->idmEmail);
        } else {
            $this->idmEmail = $idmEmail;
        }
        return $this;
    }
}
