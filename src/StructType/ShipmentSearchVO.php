<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentSearchVO StructType
 * @subpackage Structs
 */
class ShipmentSearchVO extends AbstractStructBase
{
    /**
     * The startDate
     * @var string
     */
    public $startDate;
    /**
     * The endDate
     * @var string
     */
    public $endDate;
    /**
     * The receiverName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $receiverName;
    /**
     * The receiverCountryCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $receiverCountryCode;
    /**
     * The receiverZipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $receiverZipCode;
    /**
     * The payerId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $payerId;
    /**
     * The senderAddressId
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var int
     */
    public $senderAddressId;
    /**
     * The returnLabel
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $returnLabel;
    /**
     * Constructor method for ShipmentSearchVO
     * @uses ShipmentSearchVO::setStartDate()
     * @uses ShipmentSearchVO::setEndDate()
     * @uses ShipmentSearchVO::setReceiverName()
     * @uses ShipmentSearchVO::setReceiverCountryCode()
     * @uses ShipmentSearchVO::setReceiverZipCode()
     * @uses ShipmentSearchVO::setPayerId()
     * @uses ShipmentSearchVO::setSenderAddressId()
     * @uses ShipmentSearchVO::setReturnLabel()
     * @param string $startDate
     * @param string $endDate
     * @param string $receiverName
     * @param string $receiverCountryCode
     * @param string $receiverZipCode
     * @param int $payerId
     * @param int $senderAddressId
     * @param bool $returnLabel
     */
    public function __construct($startDate = null, $endDate = null, $receiverName = null, $receiverCountryCode = null, $receiverZipCode = null, $payerId = null, $senderAddressId = null, $returnLabel = null)
    {
        $this
            ->setStartDate($startDate)
            ->setEndDate($endDate)
            ->setReceiverName($receiverName)
            ->setReceiverCountryCode($receiverCountryCode)
            ->setReceiverZipCode($receiverZipCode)
            ->setPayerId($payerId)
            ->setSenderAddressId($senderAddressId)
            ->setReturnLabel($returnLabel);
    }
    /**
     * Get startDate value
     * @return string|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
    /**
     * Set startDate value
     * @param string $startDate
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setStartDate($startDate = null)
    {
        // validation for constraint: string
        if (!is_null($startDate) && !is_string($startDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($startDate, true), gettype($startDate)), __LINE__);
        }
        $this->startDate = $startDate;
        return $this;
    }
    /**
     * Get endDate value
     * @return string|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    /**
     * Set endDate value
     * @param string $endDate
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setEndDate($endDate = null)
    {
        // validation for constraint: string
        if (!is_null($endDate) && !is_string($endDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($endDate, true), gettype($endDate)), __LINE__);
        }
        $this->endDate = $endDate;
        return $this;
    }
    /**
     * Get receiverName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReceiverName()
    {
        return isset($this->receiverName) ? $this->receiverName : null;
    }
    /**
     * Set receiverName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $receiverName
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setReceiverName($receiverName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverName) && !is_string($receiverName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverName, true), gettype($receiverName)), __LINE__);
        }
        if (is_null($receiverName) || (is_array($receiverName) && empty($receiverName))) {
            unset($this->receiverName);
        } else {
            $this->receiverName = $receiverName;
        }
        return $this;
    }
    /**
     * Get receiverCountryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReceiverCountryCode()
    {
        return isset($this->receiverCountryCode) ? $this->receiverCountryCode : null;
    }
    /**
     * Set receiverCountryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $receiverCountryCode
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setReceiverCountryCode($receiverCountryCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCountryCode) && !is_string($receiverCountryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCountryCode, true), gettype($receiverCountryCode)), __LINE__);
        }
        if (is_null($receiverCountryCode) || (is_array($receiverCountryCode) && empty($receiverCountryCode))) {
            unset($this->receiverCountryCode);
        } else {
            $this->receiverCountryCode = $receiverCountryCode;
        }
        return $this;
    }
    /**
     * Get receiverZipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getReceiverZipCode()
    {
        return isset($this->receiverZipCode) ? $this->receiverZipCode : null;
    }
    /**
     * Set receiverZipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $receiverZipCode
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setReceiverZipCode($receiverZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverZipCode) && !is_string($receiverZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverZipCode, true), gettype($receiverZipCode)), __LINE__);
        }
        if (is_null($receiverZipCode) || (is_array($receiverZipCode) && empty($receiverZipCode))) {
            unset($this->receiverZipCode);
        } else {
            $this->receiverZipCode = $receiverZipCode;
        }
        return $this;
    }
    /**
     * Get payerId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getPayerId()
    {
        return isset($this->payerId) ? $this->payerId : null;
    }
    /**
     * Set payerId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $payerId
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setPayerId($payerId = null)
    {
        // validation for constraint: int
        if (!is_null($payerId) && !(is_int($payerId) || ctype_digit($payerId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($payerId, true), gettype($payerId)), __LINE__);
        }
        if (is_null($payerId) || (is_array($payerId) && empty($payerId))) {
            unset($this->payerId);
        } else {
            $this->payerId = $payerId;
        }
        return $this;
    }
    /**
     * Get senderAddressId value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return int|null
     */
    public function getSenderAddressId()
    {
        return isset($this->senderAddressId) ? $this->senderAddressId : null;
    }
    /**
     * Set senderAddressId value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param int $senderAddressId
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setSenderAddressId($senderAddressId = null)
    {
        // validation for constraint: int
        if (!is_null($senderAddressId) && !(is_int($senderAddressId) || ctype_digit($senderAddressId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($senderAddressId, true), gettype($senderAddressId)), __LINE__);
        }
        if (is_null($senderAddressId) || (is_array($senderAddressId) && empty($senderAddressId))) {
            unset($this->senderAddressId);
        } else {
            $this->senderAddressId = $senderAddressId;
        }
        return $this;
    }
    /**
     * Get returnLabel value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getReturnLabel()
    {
        return isset($this->returnLabel) ? $this->returnLabel : null;
    }
    /**
     * Set returnLabel value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $returnLabel
     * @return \DPDSDK\Shipment\StructType\ShipmentSearchVO
     */
    public function setReturnLabel($returnLabel = null)
    {
        // validation for constraint: boolean
        if (!is_null($returnLabel) && !is_bool($returnLabel)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($returnLabel, true), gettype($returnLabel)), __LINE__);
        }
        if (is_null($returnLabel) || (is_array($returnLabel) && empty($returnLabel))) {
            unset($this->returnLabel);
        } else {
            $this->returnLabel = $returnLabel;
        }
        return $this;
    }
}
