<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for StatusResultVO StructType
 * @subpackage Structs
 */
class StatusResultVO extends AbstractStructBase
{
    /**
     * The statusReference
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public $statusReference;
    /**
     * The statusInfo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\TrackVO
     */
    public $statusInfo;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for StatusResultVO
     * @uses StatusResultVO::setStatusReference()
     * @uses StatusResultVO::setStatusInfo()
     * @uses StatusResultVO::setError()
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $statusReference
     * @param \DPDSDK\Shipment\StructType\TrackVO $statusInfo
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct(\DPDSDK\Shipment\StructType\ReferenceVO $statusReference = null, \DPDSDK\Shipment\StructType\TrackVO $statusInfo = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setStatusReference($statusReference)
            ->setStatusInfo($statusInfo)
            ->setError($error);
    }
    /**
     * Get statusReference value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO|null
     */
    public function getStatusReference()
    {
        return $this->statusReference;
    }
    /**
     * Set statusReference value
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $statusReference
     * @return \DPDSDK\Shipment\StructType\StatusResultVO
     */
    public function setStatusReference(\DPDSDK\Shipment\StructType\ReferenceVO $statusReference = null)
    {
        $this->statusReference = $statusReference;
        return $this;
    }
    /**
     * Get statusInfo value
     * @return \DPDSDK\Shipment\StructType\TrackVO|null
     */
    public function getStatusInfo()
    {
        return $this->statusInfo;
    }
    /**
     * Set statusInfo value
     * @param \DPDSDK\Shipment\StructType\TrackVO $statusInfo
     * @return \DPDSDK\Shipment\StructType\StatusResultVO
     */
    public function setStatusInfo(\DPDSDK\Shipment\StructType\TrackVO $statusInfo = null)
    {
        $this->statusInfo = $statusInfo;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\StatusResultVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
