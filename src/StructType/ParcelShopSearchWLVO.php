<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopSearchWLVO StructType
 * Meta information extracted from the WSDL
 * - type: tns:ParcelShopSearchWLVO
 * @subpackage Structs
 */
class ParcelShopSearchWLVO extends AbstractStructBase
{
    /**
     * The parcelShopId
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelShopId;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $companyName;
    /**
     * The street
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $street;
    /**
     * The houseNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $houseNo;
    /**
     * The countryCode
     * @var string
     */
    public $countryCode;
    /**
     * The zipCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $zipCode;
    /**
     * The city
     * @var string
     */
    public $city;
    /**
     * The phone
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $phone;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $email;
    /**
     * The fetchGsPUDOpoint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $fetchGsPUDOpoint;
    /**
     * The retrieveOpeningHours
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $retrieveOpeningHours;
    /**
     * Constructor method for ParcelShopSearchWLVO
     * @uses ParcelShopSearchWLVO::setParcelShopId()
     * @uses ParcelShopSearchWLVO::setCompanyName()
     * @uses ParcelShopSearchWLVO::setStreet()
     * @uses ParcelShopSearchWLVO::setHouseNo()
     * @uses ParcelShopSearchWLVO::setCountryCode()
     * @uses ParcelShopSearchWLVO::setZipCode()
     * @uses ParcelShopSearchWLVO::setCity()
     * @uses ParcelShopSearchWLVO::setPhone()
     * @uses ParcelShopSearchWLVO::setEmail()
     * @uses ParcelShopSearchWLVO::setFetchGsPUDOpoint()
     * @uses ParcelShopSearchWLVO::setRetrieveOpeningHours()
     * @param string $parcelShopId
     * @param string $companyName
     * @param string $street
     * @param string $houseNo
     * @param string $countryCode
     * @param string $zipCode
     * @param string $city
     * @param string $phone
     * @param string $email
     * @param bool $fetchGsPUDOpoint
     * @param bool $retrieveOpeningHours
     */
    public function __construct($parcelShopId = null, $companyName = null, $street = null, $houseNo = null, $countryCode = null, $zipCode = null, $city = null, $phone = null, $email = null, $fetchGsPUDOpoint = null, $retrieveOpeningHours = null)
    {
        $this
            ->setParcelShopId($parcelShopId)
            ->setCompanyName($companyName)
            ->setStreet($street)
            ->setHouseNo($houseNo)
            ->setCountryCode($countryCode)
            ->setZipCode($zipCode)
            ->setCity($city)
            ->setPhone($phone)
            ->setEmail($email)
            ->setFetchGsPUDOpoint($fetchGsPUDOpoint)
            ->setRetrieveOpeningHours($retrieveOpeningHours);
    }
    /**
     * Get parcelShopId value
     * @return string|null
     */
    public function getParcelShopId()
    {
        return $this->parcelShopId;
    }
    /**
     * Set parcelShopId value
     * @param string $parcelShopId
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setParcelShopId($parcelShopId = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopId) && !is_string($parcelShopId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopId, true), gettype($parcelShopId)), __LINE__);
        }
        $this->parcelShopId = $parcelShopId;
        return $this;
    }
    /**
     * Get companyName value
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }
    /**
     * Set companyName value
     * @param string $companyName
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setCompanyName($companyName = null)
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyName, true), gettype($companyName)), __LINE__);
        }
        $this->companyName = $companyName;
        return $this;
    }
    /**
     * Get street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * Set street value
     * @param string $street
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        $this->street = $street;
        return $this;
    }
    /**
     * Get houseNo value
     * @return string|null
     */
    public function getHouseNo()
    {
        return $this->houseNo;
    }
    /**
     * Set houseNo value
     * @param string $houseNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setHouseNo($houseNo = null)
    {
        // validation for constraint: string
        if (!is_null($houseNo) && !is_string($houseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseNo, true), gettype($houseNo)), __LINE__);
        }
        $this->houseNo = $houseNo;
        return $this;
    }
    /**
     * Get countryCode value
     * @return string|null
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }
    /**
     * Set countryCode value
     * @param string $countryCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        $this->countryCode = $countryCode;
        return $this;
    }
    /**
     * Get zipCode value
     * @return string|null
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }
    /**
     * Set zipCode value
     * @param string $zipCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        $this->zipCode = $zipCode;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * Set phone value
     * @param string $phone
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        $this->phone = $phone;
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
    /**
     * Get fetchGsPUDOpoint value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getFetchGsPUDOpoint()
    {
        return isset($this->fetchGsPUDOpoint) ? $this->fetchGsPUDOpoint : null;
    }
    /**
     * Set fetchGsPUDOpoint value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $fetchGsPUDOpoint
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setFetchGsPUDOpoint($fetchGsPUDOpoint = null)
    {
        // validation for constraint: boolean
        if (!is_null($fetchGsPUDOpoint) && !is_bool($fetchGsPUDOpoint)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fetchGsPUDOpoint, true), gettype($fetchGsPUDOpoint)), __LINE__);
        }
        if (is_null($fetchGsPUDOpoint) || (is_array($fetchGsPUDOpoint) && empty($fetchGsPUDOpoint))) {
            unset($this->fetchGsPUDOpoint);
        } else {
            $this->fetchGsPUDOpoint = $fetchGsPUDOpoint;
        }
        return $this;
    }
    /**
     * Get retrieveOpeningHours value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getRetrieveOpeningHours()
    {
        return isset($this->retrieveOpeningHours) ? $this->retrieveOpeningHours : null;
    }
    /**
     * Set retrieveOpeningHours value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $retrieveOpeningHours
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public function setRetrieveOpeningHours($retrieveOpeningHours = null)
    {
        // validation for constraint: boolean
        if (!is_null($retrieveOpeningHours) && !is_bool($retrieveOpeningHours)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($retrieveOpeningHours, true), gettype($retrieveOpeningHours)), __LINE__);
        }
        if (is_null($retrieveOpeningHours) || (is_array($retrieveOpeningHours) && empty($retrieveOpeningHours))) {
            unset($this->retrieveOpeningHours);
        } else {
            $this->retrieveOpeningHours = $retrieveOpeningHours;
        }
        return $this;
    }
}
