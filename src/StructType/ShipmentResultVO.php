<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentResultVO StructType
 * @subpackage Structs
 */
class ShipmentResultVO extends AbstractStructBase
{
    /**
     * The shipmentReference
     * @var \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public $shipmentReference;
    /**
     * The parcelResultList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ParcelResultVO[]
     */
    public $parcelResultList;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PriceVO
     */
    public $price;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * The shipmentDate
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipmentDate;
    /**
     * The shipmentTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipmentTime;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $message;
    /**
     * Constructor method for ShipmentResultVO
     * @uses ShipmentResultVO::setShipmentReference()
     * @uses ShipmentResultVO::setParcelResultList()
     * @uses ShipmentResultVO::setPrice()
     * @uses ShipmentResultVO::setError()
     * @uses ShipmentResultVO::setShipmentDate()
     * @uses ShipmentResultVO::setShipmentTime()
     * @uses ShipmentResultVO::setMessage()
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @param \DPDSDK\Shipment\StructType\ParcelResultVO[] $parcelResultList
     * @param \DPDSDK\Shipment\StructType\PriceVO $price
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @param string $shipmentDate
     * @param string $shipmentTime
     * @param string $message
     */
    public function __construct(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null, array $parcelResultList = array(), \DPDSDK\Shipment\StructType\PriceVO $price = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null, $shipmentDate = null, $shipmentTime = null, $message = null)
    {
        $this
            ->setShipmentReference($shipmentReference)
            ->setParcelResultList($parcelResultList)
            ->setPrice($price)
            ->setError($error)
            ->setShipmentDate($shipmentDate)
            ->setShipmentTime($shipmentTime)
            ->setMessage($message);
    }
    /**
     * Get shipmentReference value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO|null
     */
    public function getShipmentReference()
    {
        return $this->shipmentReference;
    }
    /**
     * Set shipmentReference value
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setShipmentReference(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null)
    {
        $this->shipmentReference = $shipmentReference;
        return $this;
    }
    /**
     * Get parcelResultList value
     * @return \DPDSDK\Shipment\StructType\ParcelResultVO[]|null
     */
    public function getParcelResultList()
    {
        return $this->parcelResultList;
    }
    /**
     * This method is responsible for validating the values passed to the setParcelResultList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParcelResultList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParcelResultListForArrayConstraintsFromSetParcelResultList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $shipmentResultVOParcelResultListItem) {
            // validation for constraint: itemType
            if (!$shipmentResultVOParcelResultListItem instanceof \DPDSDK\Shipment\StructType\ParcelResultVO) {
                $invalidValues[] = is_object($shipmentResultVOParcelResultListItem) ? get_class($shipmentResultVOParcelResultListItem) : sprintf('%s(%s)', gettype($shipmentResultVOParcelResultListItem), var_export($shipmentResultVOParcelResultListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The parcelResultList property can only contain items of type \DPDSDK\Shipment\StructType\ParcelResultVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set parcelResultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelResultVO[] $parcelResultList
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setParcelResultList(array $parcelResultList = array())
    {
        // validation for constraint: array
        if ('' !== ($parcelResultListArrayErrorMessage = self::validateParcelResultListForArrayConstraintsFromSetParcelResultList($parcelResultList))) {
            throw new \InvalidArgumentException($parcelResultListArrayErrorMessage, __LINE__);
        }
        $this->parcelResultList = $parcelResultList;
        return $this;
    }
    /**
     * Add item to parcelResultList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ParcelResultVO $item
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function addToParcelResultList(\DPDSDK\Shipment\StructType\ParcelResultVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ParcelResultVO) {
            throw new \InvalidArgumentException(sprintf('The parcelResultList property can only contain items of type \DPDSDK\Shipment\StructType\ParcelResultVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->parcelResultList[] = $item;
        return $this;
    }
    /**
     * Get price value
     * @return \DPDSDK\Shipment\StructType\PriceVO|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param \DPDSDK\Shipment\StructType\PriceVO $price
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setPrice(\DPDSDK\Shipment\StructType\PriceVO $price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
    /**
     * Get shipmentDate value
     * @return string|null
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }
    /**
     * Set shipmentDate value
     * @param string $shipmentDate
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setShipmentDate($shipmentDate = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentDate) && !is_string($shipmentDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentDate, true), gettype($shipmentDate)), __LINE__);
        }
        $this->shipmentDate = $shipmentDate;
        return $this;
    }
    /**
     * Get shipmentTime value
     * @return string|null
     */
    public function getShipmentTime()
    {
        return $this->shipmentTime;
    }
    /**
     * Set shipmentTime value
     * @param string $shipmentTime
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setShipmentTime($shipmentTime = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentTime) && !is_string($shipmentTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentTime, true), gettype($shipmentTime)), __LINE__);
        }
        $this->shipmentTime = $shipmentTime;
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \DPDSDK\Shipment\StructType\ShipmentResultVO
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        return $this;
    }
}
