<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopShipmentVO StructType
 * @subpackage Structs
 */
class ParcelShopShipmentVO extends AbstractStructBase
{
    /**
     * The parcelShopId
     * @var string
     */
    public $parcelShopId;
    /**
     * The companyName
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $companyName;
    /**
     * The street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $street;
    /**
     * The houseNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $houseNo;
    /**
     * The countryCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $countryCode;
    /**
     * The zipCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $zipCode;
    /**
     * The city
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $city;
    /**
     * The phoneNo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $phoneNo;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $email;
    /**
     * The fetchGsPUDOpoint
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var bool
     */
    public $fetchGsPUDOpoint;
    /**
     * Constructor method for ParcelShopShipmentVO
     * @uses ParcelShopShipmentVO::setParcelShopId()
     * @uses ParcelShopShipmentVO::setCompanyName()
     * @uses ParcelShopShipmentVO::setStreet()
     * @uses ParcelShopShipmentVO::setHouseNo()
     * @uses ParcelShopShipmentVO::setCountryCode()
     * @uses ParcelShopShipmentVO::setZipCode()
     * @uses ParcelShopShipmentVO::setCity()
     * @uses ParcelShopShipmentVO::setPhoneNo()
     * @uses ParcelShopShipmentVO::setEmail()
     * @uses ParcelShopShipmentVO::setFetchGsPUDOpoint()
     * @param string $parcelShopId
     * @param string $companyName
     * @param string $street
     * @param string $houseNo
     * @param string $countryCode
     * @param string $zipCode
     * @param string $city
     * @param string $phoneNo
     * @param string $email
     * @param bool $fetchGsPUDOpoint
     */
    public function __construct($parcelShopId = null, $companyName = null, $street = null, $houseNo = null, $countryCode = null, $zipCode = null, $city = null, $phoneNo = null, $email = null, $fetchGsPUDOpoint = null)
    {
        $this
            ->setParcelShopId($parcelShopId)
            ->setCompanyName($companyName)
            ->setStreet($street)
            ->setHouseNo($houseNo)
            ->setCountryCode($countryCode)
            ->setZipCode($zipCode)
            ->setCity($city)
            ->setPhoneNo($phoneNo)
            ->setEmail($email)
            ->setFetchGsPUDOpoint($fetchGsPUDOpoint);
    }
    /**
     * Get parcelShopId value
     * @return string|null
     */
    public function getParcelShopId()
    {
        return $this->parcelShopId;
    }
    /**
     * Set parcelShopId value
     * @param string $parcelShopId
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setParcelShopId($parcelShopId = null)
    {
        // validation for constraint: string
        if (!is_null($parcelShopId) && !is_string($parcelShopId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelShopId, true), gettype($parcelShopId)), __LINE__);
        }
        $this->parcelShopId = $parcelShopId;
        return $this;
    }
    /**
     * Get companyName value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCompanyName()
    {
        return isset($this->companyName) ? $this->companyName : null;
    }
    /**
     * Set companyName value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $companyName
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setCompanyName($companyName = null)
    {
        // validation for constraint: string
        if (!is_null($companyName) && !is_string($companyName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($companyName, true), gettype($companyName)), __LINE__);
        }
        if (is_null($companyName) || (is_array($companyName) && empty($companyName))) {
            unset($this->companyName);
        } else {
            $this->companyName = $companyName;
        }
        return $this;
    }
    /**
     * Get street value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getStreet()
    {
        return isset($this->street) ? $this->street : null;
    }
    /**
     * Set street value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $street
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        if (is_null($street) || (is_array($street) && empty($street))) {
            unset($this->street);
        } else {
            $this->street = $street;
        }
        return $this;
    }
    /**
     * Get houseNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getHouseNo()
    {
        return isset($this->houseNo) ? $this->houseNo : null;
    }
    /**
     * Set houseNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $houseNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setHouseNo($houseNo = null)
    {
        // validation for constraint: string
        if (!is_null($houseNo) && !is_string($houseNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($houseNo, true), gettype($houseNo)), __LINE__);
        }
        if (is_null($houseNo) || (is_array($houseNo) && empty($houseNo))) {
            unset($this->houseNo);
        } else {
            $this->houseNo = $houseNo;
        }
        return $this;
    }
    /**
     * Get countryCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCountryCode()
    {
        return isset($this->countryCode) ? $this->countryCode : null;
    }
    /**
     * Set countryCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $countryCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setCountryCode($countryCode = null)
    {
        // validation for constraint: string
        if (!is_null($countryCode) && !is_string($countryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($countryCode, true), gettype($countryCode)), __LINE__);
        }
        if (is_null($countryCode) || (is_array($countryCode) && empty($countryCode))) {
            unset($this->countryCode);
        } else {
            $this->countryCode = $countryCode;
        }
        return $this;
    }
    /**
     * Get zipCode value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getZipCode()
    {
        return isset($this->zipCode) ? $this->zipCode : null;
    }
    /**
     * Set zipCode value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $zipCode
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setZipCode($zipCode = null)
    {
        // validation for constraint: string
        if (!is_null($zipCode) && !is_string($zipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zipCode, true), gettype($zipCode)), __LINE__);
        }
        if (is_null($zipCode) || (is_array($zipCode) && empty($zipCode))) {
            unset($this->zipCode);
        } else {
            $this->zipCode = $zipCode;
        }
        return $this;
    }
    /**
     * Get city value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCity()
    {
        return isset($this->city) ? $this->city : null;
    }
    /**
     * Set city value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $city
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        if (is_null($city) || (is_array($city) && empty($city))) {
            unset($this->city);
        } else {
            $this->city = $city;
        }
        return $this;
    }
    /**
     * Get phoneNo value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getPhoneNo()
    {
        return isset($this->phoneNo) ? $this->phoneNo : null;
    }
    /**
     * Set phoneNo value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $phoneNo
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setPhoneNo($phoneNo = null)
    {
        // validation for constraint: string
        if (!is_null($phoneNo) && !is_string($phoneNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phoneNo, true), gettype($phoneNo)), __LINE__);
        }
        if (is_null($phoneNo) || (is_array($phoneNo) && empty($phoneNo))) {
            unset($this->phoneNo);
        } else {
            $this->phoneNo = $phoneNo;
        }
        return $this;
    }
    /**
     * Get email value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getEmail()
    {
        return isset($this->email) ? $this->email : null;
    }
    /**
     * Set email value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $email
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        if (is_null($email) || (is_array($email) && empty($email))) {
            unset($this->email);
        } else {
            $this->email = $email;
        }
        return $this;
    }
    /**
     * Get fetchGsPUDOpoint value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return bool|null
     */
    public function getFetchGsPUDOpoint()
    {
        return isset($this->fetchGsPUDOpoint) ? $this->fetchGsPUDOpoint : null;
    }
    /**
     * Set fetchGsPUDOpoint value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param bool $fetchGsPUDOpoint
     * @return \DPDSDK\Shipment\StructType\ParcelShopShipmentVO
     */
    public function setFetchGsPUDOpoint($fetchGsPUDOpoint = null)
    {
        // validation for constraint: boolean
        if (!is_null($fetchGsPUDOpoint) && !is_bool($fetchGsPUDOpoint)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a bool, %s given', var_export($fetchGsPUDOpoint, true), gettype($fetchGsPUDOpoint)), __LINE__);
        }
        if (is_null($fetchGsPUDOpoint) || (is_array($fetchGsPUDOpoint) && empty($fetchGsPUDOpoint))) {
            unset($this->fetchGsPUDOpoint);
        } else {
            $this->fetchGsPUDOpoint = $fetchGsPUDOpoint;
        }
        return $this;
    }
}
