<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for getParcelStatusResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:getParcelStatusResponse
 * @subpackage Structs
 */
class GetParcelStatusResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\StatusResponseVO
     */
    public $result;
    /**
     * Constructor method for getParcelStatusResponse
     * @uses GetParcelStatusResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\StatusResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\StatusResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\StatusResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\StatusResponseVO $result
     * @return \DPDSDK\Shipment\StructType\GetParcelStatusResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\StatusResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
