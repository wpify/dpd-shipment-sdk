<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for parcelShopSearchWL StructType
 * Meta information extracted from the WSDL
 * - type: tns:parcelShopSearchWL
 * @subpackage Structs
 */
class ParcelShopSearchWL extends AbstractStructBase
{
    /**
     * The searchParams
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO
     */
    public $searchParams;
    /**
     * Constructor method for parcelShopSearchWL
     * @uses ParcelShopSearchWL::setSearchParams()
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO $searchParams
     */
    public function __construct(\DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO $searchParams = null)
    {
        $this
            ->setSearchParams($searchParams);
    }
    /**
     * Get searchParams value
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO|null
     */
    public function getSearchParams()
    {
        return $this->searchParams;
    }
    /**
     * Set searchParams value
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO $searchParams
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWL
     */
    public function setSearchParams(\DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO $searchParams = null)
    {
        $this->searchParams = $searchParams;
        return $this;
    }
}
