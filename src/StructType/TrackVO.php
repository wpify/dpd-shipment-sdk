<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for TrackVO StructType
 * @subpackage Structs
 */
class TrackVO extends AbstractStructBase
{
    /**
     * The parcelReference
     * @var \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public $parcelReference;
    /**
     * The shipDate
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipDate;
    /**
     * The shipTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $shipTime;
    /**
     * The deliveryDate
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $deliveryDate;
    /**
     * The deliveryTime
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $deliveryTime;
    /**
     * The parcelNo
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $parcelNo;
    /**
     * The serviceCode
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var int
     */
    public $serviceCode;
    /**
     * The serviceDescription
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $serviceDescription;
    /**
     * The weight
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var float
     */
    public $weight;
    /**
     * The dpdUrl
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $dpdUrl;
    /**
     * The scans
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ScanVO[]
     */
    public $scans;
    /**
     * Constructor method for TrackVO
     * @uses TrackVO::setParcelReference()
     * @uses TrackVO::setShipDate()
     * @uses TrackVO::setShipTime()
     * @uses TrackVO::setDeliveryDate()
     * @uses TrackVO::setDeliveryTime()
     * @uses TrackVO::setParcelNo()
     * @uses TrackVO::setServiceCode()
     * @uses TrackVO::setServiceDescription()
     * @uses TrackVO::setWeight()
     * @uses TrackVO::setDpdUrl()
     * @uses TrackVO::setScans()
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $parcelReference
     * @param string $shipDate
     * @param string $shipTime
     * @param string $deliveryDate
     * @param string $deliveryTime
     * @param string $parcelNo
     * @param int $serviceCode
     * @param string $serviceDescription
     * @param float $weight
     * @param string $dpdUrl
     * @param \DPDSDK\Shipment\StructType\ScanVO[] $scans
     */
    public function __construct(\DPDSDK\Shipment\StructType\ReferenceVO $parcelReference = null, $shipDate = null, $shipTime = null, $deliveryDate = null, $deliveryTime = null, $parcelNo = null, $serviceCode = null, $serviceDescription = null, $weight = null, $dpdUrl = null, array $scans = array())
    {
        $this
            ->setParcelReference($parcelReference)
            ->setShipDate($shipDate)
            ->setShipTime($shipTime)
            ->setDeliveryDate($deliveryDate)
            ->setDeliveryTime($deliveryTime)
            ->setParcelNo($parcelNo)
            ->setServiceCode($serviceCode)
            ->setServiceDescription($serviceDescription)
            ->setWeight($weight)
            ->setDpdUrl($dpdUrl)
            ->setScans($scans);
    }
    /**
     * Get parcelReference value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO|null
     */
    public function getParcelReference()
    {
        return $this->parcelReference;
    }
    /**
     * Set parcelReference value
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $parcelReference
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setParcelReference(\DPDSDK\Shipment\StructType\ReferenceVO $parcelReference = null)
    {
        $this->parcelReference = $parcelReference;
        return $this;
    }
    /**
     * Get shipDate value
     * @return string|null
     */
    public function getShipDate()
    {
        return $this->shipDate;
    }
    /**
     * Set shipDate value
     * @param string $shipDate
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setShipDate($shipDate = null)
    {
        // validation for constraint: string
        if (!is_null($shipDate) && !is_string($shipDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipDate, true), gettype($shipDate)), __LINE__);
        }
        $this->shipDate = $shipDate;
        return $this;
    }
    /**
     * Get shipTime value
     * @return string|null
     */
    public function getShipTime()
    {
        return $this->shipTime;
    }
    /**
     * Set shipTime value
     * @param string $shipTime
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setShipTime($shipTime = null)
    {
        // validation for constraint: string
        if (!is_null($shipTime) && !is_string($shipTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipTime, true), gettype($shipTime)), __LINE__);
        }
        $this->shipTime = $shipTime;
        return $this;
    }
    /**
     * Get deliveryDate value
     * @return string|null
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }
    /**
     * Set deliveryDate value
     * @param string $deliveryDate
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setDeliveryDate($deliveryDate = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryDate) && !is_string($deliveryDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryDate, true), gettype($deliveryDate)), __LINE__);
        }
        $this->deliveryDate = $deliveryDate;
        return $this;
    }
    /**
     * Get deliveryTime value
     * @return string|null
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }
    /**
     * Set deliveryTime value
     * @param string $deliveryTime
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setDeliveryTime($deliveryTime = null)
    {
        // validation for constraint: string
        if (!is_null($deliveryTime) && !is_string($deliveryTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($deliveryTime, true), gettype($deliveryTime)), __LINE__);
        }
        $this->deliveryTime = $deliveryTime;
        return $this;
    }
    /**
     * Get parcelNo value
     * @return string|null
     */
    public function getParcelNo()
    {
        return $this->parcelNo;
    }
    /**
     * Set parcelNo value
     * @param string $parcelNo
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setParcelNo($parcelNo = null)
    {
        // validation for constraint: string
        if (!is_null($parcelNo) && !is_string($parcelNo)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($parcelNo, true), gettype($parcelNo)), __LINE__);
        }
        $this->parcelNo = $parcelNo;
        return $this;
    }
    /**
     * Get serviceCode value
     * @return int|null
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }
    /**
     * Set serviceCode value
     * @param int $serviceCode
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setServiceCode($serviceCode = null)
    {
        // validation for constraint: int
        if (!is_null($serviceCode) && !(is_int($serviceCode) || ctype_digit($serviceCode))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($serviceCode, true), gettype($serviceCode)), __LINE__);
        }
        $this->serviceCode = $serviceCode;
        return $this;
    }
    /**
     * Get serviceDescription value
     * @return string|null
     */
    public function getServiceDescription()
    {
        return $this->serviceDescription;
    }
    /**
     * Set serviceDescription value
     * @param string $serviceDescription
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setServiceDescription($serviceDescription = null)
    {
        // validation for constraint: string
        if (!is_null($serviceDescription) && !is_string($serviceDescription)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($serviceDescription, true), gettype($serviceDescription)), __LINE__);
        }
        $this->serviceDescription = $serviceDescription;
        return $this;
    }
    /**
     * Get weight value
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }
    /**
     * Set weight value
     * @param float $weight
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setWeight($weight = null)
    {
        // validation for constraint: float
        if (!is_null($weight) && !(is_float($weight) || is_numeric($weight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($weight, true), gettype($weight)), __LINE__);
        }
        $this->weight = $weight;
        return $this;
    }
    /**
     * Get dpdUrl value
     * @return string|null
     */
    public function getDpdUrl()
    {
        return $this->dpdUrl;
    }
    /**
     * Set dpdUrl value
     * @param string $dpdUrl
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setDpdUrl($dpdUrl = null)
    {
        // validation for constraint: string
        if (!is_null($dpdUrl) && !is_string($dpdUrl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dpdUrl, true), gettype($dpdUrl)), __LINE__);
        }
        $this->dpdUrl = $dpdUrl;
        return $this;
    }
    /**
     * Get scans value
     * @return \DPDSDK\Shipment\StructType\ScanVO[]|null
     */
    public function getScans()
    {
        return $this->scans;
    }
    /**
     * This method is responsible for validating the values passed to the setScans method
     * This method is willingly generated in order to preserve the one-line inline validation within the setScans method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateScansForArrayConstraintsFromSetScans(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $trackVOScansItem) {
            // validation for constraint: itemType
            if (!$trackVOScansItem instanceof \DPDSDK\Shipment\StructType\ScanVO) {
                $invalidValues[] = is_object($trackVOScansItem) ? get_class($trackVOScansItem) : sprintf('%s(%s)', gettype($trackVOScansItem), var_export($trackVOScansItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The scans property can only contain items of type \DPDSDK\Shipment\StructType\ScanVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set scans value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ScanVO[] $scans
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function setScans(array $scans = array())
    {
        // validation for constraint: array
        if ('' !== ($scansArrayErrorMessage = self::validateScansForArrayConstraintsFromSetScans($scans))) {
            throw new \InvalidArgumentException($scansArrayErrorMessage, __LINE__);
        }
        $this->scans = $scans;
        return $this;
    }
    /**
     * Add item to scans value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\ScanVO $item
     * @return \DPDSDK\Shipment\StructType\TrackVO
     */
    public function addToScans(\DPDSDK\Shipment\StructType\ScanVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\ScanVO) {
            throw new \InvalidArgumentException(sprintf('The scans property can only contain items of type \DPDSDK\Shipment\StructType\ScanVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->scans[] = $item;
        return $this;
    }
}
