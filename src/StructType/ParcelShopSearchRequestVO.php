<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ParcelShopSearchRequestVO StructType
 * @subpackage Structs
 */
class ParcelShopSearchRequestVO extends AbstractStructBase
{
    /**
     * The loginParam
     * @var \DPDSDK\Shipment\StructType\UserLoginVO
     */
    public $loginParam;
    /**
     * The parcelShopSearch
     * @var \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO
     */
    public $parcelShopSearch;
    /**
     * Constructor method for ParcelShopSearchRequestVO
     * @uses ParcelShopSearchRequestVO::setLoginParam()
     * @uses ParcelShopSearchRequestVO::setParcelShopSearch()
     * @param \DPDSDK\Shipment\StructType\UserLoginVO $loginParam
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO $parcelShopSearch
     */
    public function __construct(\DPDSDK\Shipment\StructType\UserLoginVO $loginParam = null, \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO $parcelShopSearch = null)
    {
        $this
            ->setLoginParam($loginParam)
            ->setParcelShopSearch($parcelShopSearch);
    }
    /**
     * Get loginParam value
     * @return \DPDSDK\Shipment\StructType\UserLoginVO|null
     */
    public function getLoginParam()
    {
        return $this->loginParam;
    }
    /**
     * Set loginParam value
     * @param \DPDSDK\Shipment\StructType\UserLoginVO $loginParam
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO
     */
    public function setLoginParam(\DPDSDK\Shipment\StructType\UserLoginVO $loginParam = null)
    {
        $this->loginParam = $loginParam;
        return $this;
    }
    /**
     * Get parcelShopSearch value
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO|null
     */
    public function getParcelShopSearch()
    {
        return $this->parcelShopSearch;
    }
    /**
     * Set parcelShopSearch value
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearchWLVO $parcelShopSearch
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchRequestVO
     */
    public function setParcelShopSearch(\DPDSDK\Shipment\StructType\ParcelShopSearchWLVO $parcelShopSearch = null)
    {
        $this->parcelShopSearch = $parcelShopSearch;
        return $this;
    }
}
