<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PriceVO StructType
 * @subpackage Structs
 */
class PriceVO extends AbstractStructBase
{
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The vatAmount
     * @var float
     */
    public $vatAmount;
    /**
     * The totalAmount
     * @var float
     */
    public $totalAmount;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The amountLocal
     * @var float
     */
    public $amountLocal;
    /**
     * The vatAmountLocal
     * @var float
     */
    public $vatAmountLocal;
    /**
     * The totalAmountLocal
     * @var float
     */
    public $totalAmountLocal;
    /**
     * The currencyLocal
     * @var string
     */
    public $currencyLocal;
    /**
     * Constructor method for PriceVO
     * @uses PriceVO::setAmount()
     * @uses PriceVO::setVatAmount()
     * @uses PriceVO::setTotalAmount()
     * @uses PriceVO::setCurrency()
     * @uses PriceVO::setAmountLocal()
     * @uses PriceVO::setVatAmountLocal()
     * @uses PriceVO::setTotalAmountLocal()
     * @uses PriceVO::setCurrencyLocal()
     * @param float $amount
     * @param float $vatAmount
     * @param float $totalAmount
     * @param string $currency
     * @param float $amountLocal
     * @param float $vatAmountLocal
     * @param float $totalAmountLocal
     * @param string $currencyLocal
     */
    public function __construct($amount = null, $vatAmount = null, $totalAmount = null, $currency = null, $amountLocal = null, $vatAmountLocal = null, $totalAmountLocal = null, $currencyLocal = null)
    {
        $this
            ->setAmount($amount)
            ->setVatAmount($vatAmount)
            ->setTotalAmount($totalAmount)
            ->setCurrency($currency)
            ->setAmountLocal($amountLocal)
            ->setVatAmountLocal($vatAmountLocal)
            ->setTotalAmountLocal($totalAmountLocal)
            ->setCurrencyLocal($currencyLocal);
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get vatAmount value
     * @return float|null
     */
    public function getVatAmount()
    {
        return $this->vatAmount;
    }
    /**
     * Set vatAmount value
     * @param float $vatAmount
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setVatAmount($vatAmount = null)
    {
        // validation for constraint: float
        if (!is_null($vatAmount) && !(is_float($vatAmount) || is_numeric($vatAmount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatAmount, true), gettype($vatAmount)), __LINE__);
        }
        $this->vatAmount = $vatAmount;
        return $this;
    }
    /**
     * Get totalAmount value
     * @return float|null
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }
    /**
     * Set totalAmount value
     * @param float $totalAmount
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setTotalAmount($totalAmount = null)
    {
        // validation for constraint: float
        if (!is_null($totalAmount) && !(is_float($totalAmount) || is_numeric($totalAmount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($totalAmount, true), gettype($totalAmount)), __LINE__);
        }
        $this->totalAmount = $totalAmount;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get amountLocal value
     * @return float|null
     */
    public function getAmountLocal()
    {
        return $this->amountLocal;
    }
    /**
     * Set amountLocal value
     * @param float $amountLocal
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setAmountLocal($amountLocal = null)
    {
        // validation for constraint: float
        if (!is_null($amountLocal) && !(is_float($amountLocal) || is_numeric($amountLocal))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amountLocal, true), gettype($amountLocal)), __LINE__);
        }
        $this->amountLocal = $amountLocal;
        return $this;
    }
    /**
     * Get vatAmountLocal value
     * @return float|null
     */
    public function getVatAmountLocal()
    {
        return $this->vatAmountLocal;
    }
    /**
     * Set vatAmountLocal value
     * @param float $vatAmountLocal
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setVatAmountLocal($vatAmountLocal = null)
    {
        // validation for constraint: float
        if (!is_null($vatAmountLocal) && !(is_float($vatAmountLocal) || is_numeric($vatAmountLocal))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($vatAmountLocal, true), gettype($vatAmountLocal)), __LINE__);
        }
        $this->vatAmountLocal = $vatAmountLocal;
        return $this;
    }
    /**
     * Get totalAmountLocal value
     * @return float|null
     */
    public function getTotalAmountLocal()
    {
        return $this->totalAmountLocal;
    }
    /**
     * Set totalAmountLocal value
     * @param float $totalAmountLocal
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setTotalAmountLocal($totalAmountLocal = null)
    {
        // validation for constraint: float
        if (!is_null($totalAmountLocal) && !(is_float($totalAmountLocal) || is_numeric($totalAmountLocal))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($totalAmountLocal, true), gettype($totalAmountLocal)), __LINE__);
        }
        $this->totalAmountLocal = $totalAmountLocal;
        return $this;
    }
    /**
     * Get currencyLocal value
     * @return string|null
     */
    public function getCurrencyLocal()
    {
        return $this->currencyLocal;
    }
    /**
     * Set currencyLocal value
     * @param string $currencyLocal
     * @return \DPDSDK\Shipment\StructType\PriceVO
     */
    public function setCurrencyLocal($currencyLocal = null)
    {
        // validation for constraint: string
        if (!is_null($currencyLocal) && !is_string($currencyLocal)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currencyLocal, true), gettype($currencyLocal)), __LINE__);
        }
        $this->currencyLocal = $currencyLocal;
        return $this;
    }
}
