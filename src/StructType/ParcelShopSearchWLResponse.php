<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for parcelShopSearchWLResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:parcelShopSearchWLResponse
 * @subpackage Structs
 */
class ParcelShopSearchWLResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\ParcelShopResponseVO
     */
    public $result;
    /**
     * Constructor method for parcelShopSearchWLResponse
     * @uses ParcelShopSearchWLResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\ParcelShopResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\ParcelShopResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\ParcelShopResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\ParcelShopResponseVO $result
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\ParcelShopResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
