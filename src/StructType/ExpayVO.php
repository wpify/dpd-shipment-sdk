<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ExpayVO StructType
 * @subpackage Structs
 */
class ExpayVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var string
     */
    public $transactionId;
    /**
     * The authenticationId
     * @var string
     */
    public $authenticationId;
    /**
     * The hashCode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $hashCode;
    /**
     * The reference
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $reference;
    /**
     * Constructor method for ExpayVO
     * @uses ExpayVO::setTransactionId()
     * @uses ExpayVO::setAuthenticationId()
     * @uses ExpayVO::setHashCode()
     * @uses ExpayVO::setReference()
     * @param string $transactionId
     * @param string $authenticationId
     * @param string $hashCode
     * @param string $reference
     */
    public function __construct($transactionId = null, $authenticationId = null, $hashCode = null, $reference = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setAuthenticationId($authenticationId)
            ->setHashCode($hashCode)
            ->setReference($reference);
    }
    /**
     * Get transactionId value
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param string $transactionId
     * @return \DPDSDK\Shipment\StructType\ExpayVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get authenticationId value
     * @return string|null
     */
    public function getAuthenticationId()
    {
        return $this->authenticationId;
    }
    /**
     * Set authenticationId value
     * @param string $authenticationId
     * @return \DPDSDK\Shipment\StructType\ExpayVO
     */
    public function setAuthenticationId($authenticationId = null)
    {
        // validation for constraint: string
        if (!is_null($authenticationId) && !is_string($authenticationId)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authenticationId, true), gettype($authenticationId)), __LINE__);
        }
        $this->authenticationId = $authenticationId;
        return $this;
    }
    /**
     * Get hashCode value
     * @return string|null
     */
    public function getHashCode()
    {
        return $this->hashCode;
    }
    /**
     * Set hashCode value
     * @param string $hashCode
     * @return \DPDSDK\Shipment\StructType\ExpayVO
     */
    public function setHashCode($hashCode = null)
    {
        // validation for constraint: string
        if (!is_null($hashCode) && !is_string($hashCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hashCode, true), gettype($hashCode)), __LINE__);
        }
        $this->hashCode = $hashCode;
        return $this;
    }
    /**
     * Get reference value
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }
    /**
     * Set reference value
     * @param string $reference
     * @return \DPDSDK\Shipment\StructType\ExpayVO
     */
    public function setReference($reference = null)
    {
        // validation for constraint: string
        if (!is_null($reference) && !is_string($reference)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reference, true), gettype($reference)), __LINE__);
        }
        $this->reference = $reference;
        return $this;
    }
}
