<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for HighInsuranceVO StructType
 * @subpackage Structs
 */
class HighInsuranceVO extends AbstractStructBase
{
    /**
     * The goodsValue
     * @var float
     */
    public $goodsValue;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The content
     * @var string
     */
    public $content;
    /**
     * Constructor method for HighInsuranceVO
     * @uses HighInsuranceVO::setGoodsValue()
     * @uses HighInsuranceVO::setCurrency()
     * @uses HighInsuranceVO::setContent()
     * @param float $goodsValue
     * @param string $currency
     * @param string $content
     */
    public function __construct($goodsValue = null, $currency = null, $content = null)
    {
        $this
            ->setGoodsValue($goodsValue)
            ->setCurrency($currency)
            ->setContent($content);
    }
    /**
     * Get goodsValue value
     * @return float|null
     */
    public function getGoodsValue()
    {
        return $this->goodsValue;
    }
    /**
     * Set goodsValue value
     * @param float $goodsValue
     * @return \DPDSDK\Shipment\StructType\HighInsuranceVO
     */
    public function setGoodsValue($goodsValue = null)
    {
        // validation for constraint: float
        if (!is_null($goodsValue) && !(is_float($goodsValue) || is_numeric($goodsValue))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($goodsValue, true), gettype($goodsValue)), __LINE__);
        }
        $this->goodsValue = $goodsValue;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \DPDSDK\Shipment\StructType\HighInsuranceVO
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get content value
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * Set content value
     * @param string $content
     * @return \DPDSDK\Shipment\StructType\HighInsuranceVO
     */
    public function setContent($content = null)
    {
        // validation for constraint: string
        if (!is_null($content) && !is_string($content)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($content, true), gettype($content)), __LINE__);
        }
        $this->content = $content;
        return $this;
    }
}
