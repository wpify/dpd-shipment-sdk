<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for calculatePriceResponse StructType
 * Meta information extracted from the WSDL
 * - type: tns:calculatePriceResponse
 * @subpackage Structs
 */
class CalculatePriceResponse extends AbstractStructBase
{
    /**
     * The result
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \DPDSDK\Shipment\StructType\PriceResponseVO
     */
    public $result;
    /**
     * Constructor method for calculatePriceResponse
     * @uses CalculatePriceResponse::setResult()
     * @param \DPDSDK\Shipment\StructType\PriceResponseVO $result
     */
    public function __construct(\DPDSDK\Shipment\StructType\PriceResponseVO $result = null)
    {
        $this
            ->setResult($result);
    }
    /**
     * Get result value
     * @return \DPDSDK\Shipment\StructType\PriceResponseVO|null
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * Set result value
     * @param \DPDSDK\Shipment\StructType\PriceResponseVO $result
     * @return \DPDSDK\Shipment\StructType\CalculatePriceResponse
     */
    public function setResult(\DPDSDK\Shipment\StructType\PriceResponseVO $result = null)
    {
        $this->result = $result;
        return $this;
    }
}
