<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentDeleteResultVO StructType
 * @subpackage Structs
 */
class ShipmentDeleteResultVO extends AbstractStructBase
{
    /**
     * The shipmentReference
     * @var \DPDSDK\Shipment\StructType\ReferenceVO
     */
    public $shipmentReference;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for ShipmentDeleteResultVO
     * @uses ShipmentDeleteResultVO::setShipmentReference()
     * @uses ShipmentDeleteResultVO::setError()
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setShipmentReference($shipmentReference)
            ->setError($error);
    }
    /**
     * Get shipmentReference value
     * @return \DPDSDK\Shipment\StructType\ReferenceVO|null
     */
    public function getShipmentReference()
    {
        return $this->shipmentReference;
    }
    /**
     * Set shipmentReference value
     * @param \DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference
     * @return \DPDSDK\Shipment\StructType\ShipmentDeleteResultVO
     */
    public function setShipmentReference(\DPDSDK\Shipment\StructType\ReferenceVO $shipmentReference = null)
    {
        $this->shipmentReference = $shipmentReference;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\ShipmentDeleteResultVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
