<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ShipmentInfoVO StructType
 * @subpackage Structs
 */
class ShipmentInfoVO extends AbstractStructBase
{
    /**
     * The shipmentId
     * @var int
     */
    public $shipmentId;
    /**
     * The shipmentReferenceNumber
     * @var string
     */
    public $shipmentReferenceNumber;
    /**
     * The senderAddressId
     * @var int
     */
    public $senderAddressId;
    /**
     * The payerId
     * @var int
     */
    public $payerId;
    /**
     * The receiverName
     * @var string
     */
    public $receiverName;
    /**
     * The receiverCountryCode
     * @var string
     */
    public $receiverCountryCode;
    /**
     * The receiverZipCode
     * @var string
     */
    public $receiverZipCode;
    /**
     * The receiverCity
     * @var string
     */
    public $receiverCity;
    /**
     * The receiverStreet
     * @var string
     */
    public $receiverStreet;
    /**
     * The mainServiceCode
     * @var int
     */
    public $mainServiceCode;
    /**
     * The totalWeight
     * @var float
     */
    public $totalWeight;
    /**
     * The parcelCount
     * @var int
     */
    public $parcelCount;
    /**
     * The shipmentDate
     * @var string
     */
    public $shipmentDate;
    /**
     * The shipmentTime
     * @var string
     */
    public $shipmentTime;
    /**
     * Constructor method for ShipmentInfoVO
     * @uses ShipmentInfoVO::setShipmentId()
     * @uses ShipmentInfoVO::setShipmentReferenceNumber()
     * @uses ShipmentInfoVO::setSenderAddressId()
     * @uses ShipmentInfoVO::setPayerId()
     * @uses ShipmentInfoVO::setReceiverName()
     * @uses ShipmentInfoVO::setReceiverCountryCode()
     * @uses ShipmentInfoVO::setReceiverZipCode()
     * @uses ShipmentInfoVO::setReceiverCity()
     * @uses ShipmentInfoVO::setReceiverStreet()
     * @uses ShipmentInfoVO::setMainServiceCode()
     * @uses ShipmentInfoVO::setTotalWeight()
     * @uses ShipmentInfoVO::setParcelCount()
     * @uses ShipmentInfoVO::setShipmentDate()
     * @uses ShipmentInfoVO::setShipmentTime()
     * @param int $shipmentId
     * @param string $shipmentReferenceNumber
     * @param int $senderAddressId
     * @param int $payerId
     * @param string $receiverName
     * @param string $receiverCountryCode
     * @param string $receiverZipCode
     * @param string $receiverCity
     * @param string $receiverStreet
     * @param int $mainServiceCode
     * @param float $totalWeight
     * @param int $parcelCount
     * @param string $shipmentDate
     * @param string $shipmentTime
     */
    public function __construct($shipmentId = null, $shipmentReferenceNumber = null, $senderAddressId = null, $payerId = null, $receiverName = null, $receiverCountryCode = null, $receiverZipCode = null, $receiverCity = null, $receiverStreet = null, $mainServiceCode = null, $totalWeight = null, $parcelCount = null, $shipmentDate = null, $shipmentTime = null)
    {
        $this
            ->setShipmentId($shipmentId)
            ->setShipmentReferenceNumber($shipmentReferenceNumber)
            ->setSenderAddressId($senderAddressId)
            ->setPayerId($payerId)
            ->setReceiverName($receiverName)
            ->setReceiverCountryCode($receiverCountryCode)
            ->setReceiverZipCode($receiverZipCode)
            ->setReceiverCity($receiverCity)
            ->setReceiverStreet($receiverStreet)
            ->setMainServiceCode($mainServiceCode)
            ->setTotalWeight($totalWeight)
            ->setParcelCount($parcelCount)
            ->setShipmentDate($shipmentDate)
            ->setShipmentTime($shipmentTime);
    }
    /**
     * Get shipmentId value
     * @return int|null
     */
    public function getShipmentId()
    {
        return $this->shipmentId;
    }
    /**
     * Set shipmentId value
     * @param int $shipmentId
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setShipmentId($shipmentId = null)
    {
        // validation for constraint: int
        if (!is_null($shipmentId) && !(is_int($shipmentId) || ctype_digit($shipmentId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($shipmentId, true), gettype($shipmentId)), __LINE__);
        }
        $this->shipmentId = $shipmentId;
        return $this;
    }
    /**
     * Get shipmentReferenceNumber value
     * @return string|null
     */
    public function getShipmentReferenceNumber()
    {
        return $this->shipmentReferenceNumber;
    }
    /**
     * Set shipmentReferenceNumber value
     * @param string $shipmentReferenceNumber
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setShipmentReferenceNumber($shipmentReferenceNumber = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentReferenceNumber) && !is_string($shipmentReferenceNumber)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentReferenceNumber, true), gettype($shipmentReferenceNumber)), __LINE__);
        }
        $this->shipmentReferenceNumber = $shipmentReferenceNumber;
        return $this;
    }
    /**
     * Get senderAddressId value
     * @return int|null
     */
    public function getSenderAddressId()
    {
        return $this->senderAddressId;
    }
    /**
     * Set senderAddressId value
     * @param int $senderAddressId
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setSenderAddressId($senderAddressId = null)
    {
        // validation for constraint: int
        if (!is_null($senderAddressId) && !(is_int($senderAddressId) || ctype_digit($senderAddressId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($senderAddressId, true), gettype($senderAddressId)), __LINE__);
        }
        $this->senderAddressId = $senderAddressId;
        return $this;
    }
    /**
     * Get payerId value
     * @return int|null
     */
    public function getPayerId()
    {
        return $this->payerId;
    }
    /**
     * Set payerId value
     * @param int $payerId
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setPayerId($payerId = null)
    {
        // validation for constraint: int
        if (!is_null($payerId) && !(is_int($payerId) || ctype_digit($payerId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($payerId, true), gettype($payerId)), __LINE__);
        }
        $this->payerId = $payerId;
        return $this;
    }
    /**
     * Get receiverName value
     * @return string|null
     */
    public function getReceiverName()
    {
        return $this->receiverName;
    }
    /**
     * Set receiverName value
     * @param string $receiverName
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setReceiverName($receiverName = null)
    {
        // validation for constraint: string
        if (!is_null($receiverName) && !is_string($receiverName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverName, true), gettype($receiverName)), __LINE__);
        }
        $this->receiverName = $receiverName;
        return $this;
    }
    /**
     * Get receiverCountryCode value
     * @return string|null
     */
    public function getReceiverCountryCode()
    {
        return $this->receiverCountryCode;
    }
    /**
     * Set receiverCountryCode value
     * @param string $receiverCountryCode
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setReceiverCountryCode($receiverCountryCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCountryCode) && !is_string($receiverCountryCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCountryCode, true), gettype($receiverCountryCode)), __LINE__);
        }
        $this->receiverCountryCode = $receiverCountryCode;
        return $this;
    }
    /**
     * Get receiverZipCode value
     * @return string|null
     */
    public function getReceiverZipCode()
    {
        return $this->receiverZipCode;
    }
    /**
     * Set receiverZipCode value
     * @param string $receiverZipCode
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setReceiverZipCode($receiverZipCode = null)
    {
        // validation for constraint: string
        if (!is_null($receiverZipCode) && !is_string($receiverZipCode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverZipCode, true), gettype($receiverZipCode)), __LINE__);
        }
        $this->receiverZipCode = $receiverZipCode;
        return $this;
    }
    /**
     * Get receiverCity value
     * @return string|null
     */
    public function getReceiverCity()
    {
        return $this->receiverCity;
    }
    /**
     * Set receiverCity value
     * @param string $receiverCity
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setReceiverCity($receiverCity = null)
    {
        // validation for constraint: string
        if (!is_null($receiverCity) && !is_string($receiverCity)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverCity, true), gettype($receiverCity)), __LINE__);
        }
        $this->receiverCity = $receiverCity;
        return $this;
    }
    /**
     * Get receiverStreet value
     * @return string|null
     */
    public function getReceiverStreet()
    {
        return $this->receiverStreet;
    }
    /**
     * Set receiverStreet value
     * @param string $receiverStreet
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setReceiverStreet($receiverStreet = null)
    {
        // validation for constraint: string
        if (!is_null($receiverStreet) && !is_string($receiverStreet)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($receiverStreet, true), gettype($receiverStreet)), __LINE__);
        }
        $this->receiverStreet = $receiverStreet;
        return $this;
    }
    /**
     * Get mainServiceCode value
     * @return int|null
     */
    public function getMainServiceCode()
    {
        return $this->mainServiceCode;
    }
    /**
     * Set mainServiceCode value
     * @param int $mainServiceCode
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setMainServiceCode($mainServiceCode = null)
    {
        // validation for constraint: int
        if (!is_null($mainServiceCode) && !(is_int($mainServiceCode) || ctype_digit($mainServiceCode))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($mainServiceCode, true), gettype($mainServiceCode)), __LINE__);
        }
        $this->mainServiceCode = $mainServiceCode;
        return $this;
    }
    /**
     * Get totalWeight value
     * @return float|null
     */
    public function getTotalWeight()
    {
        return $this->totalWeight;
    }
    /**
     * Set totalWeight value
     * @param float $totalWeight
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setTotalWeight($totalWeight = null)
    {
        // validation for constraint: float
        if (!is_null($totalWeight) && !(is_float($totalWeight) || is_numeric($totalWeight))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($totalWeight, true), gettype($totalWeight)), __LINE__);
        }
        $this->totalWeight = $totalWeight;
        return $this;
    }
    /**
     * Get parcelCount value
     * @return int|null
     */
    public function getParcelCount()
    {
        return $this->parcelCount;
    }
    /**
     * Set parcelCount value
     * @param int $parcelCount
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setParcelCount($parcelCount = null)
    {
        // validation for constraint: int
        if (!is_null($parcelCount) && !(is_int($parcelCount) || ctype_digit($parcelCount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($parcelCount, true), gettype($parcelCount)), __LINE__);
        }
        $this->parcelCount = $parcelCount;
        return $this;
    }
    /**
     * Get shipmentDate value
     * @return string|null
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }
    /**
     * Set shipmentDate value
     * @param string $shipmentDate
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setShipmentDate($shipmentDate = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentDate) && !is_string($shipmentDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentDate, true), gettype($shipmentDate)), __LINE__);
        }
        $this->shipmentDate = $shipmentDate;
        return $this;
    }
    /**
     * Get shipmentTime value
     * @return string|null
     */
    public function getShipmentTime()
    {
        return $this->shipmentTime;
    }
    /**
     * Set shipmentTime value
     * @param string $shipmentTime
     * @return \DPDSDK\Shipment\StructType\ShipmentInfoVO
     */
    public function setShipmentTime($shipmentTime = null)
    {
        // validation for constraint: string
        if (!is_null($shipmentTime) && !is_string($shipmentTime)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($shipmentTime, true), gettype($shipmentTime)), __LINE__);
        }
        $this->shipmentTime = $shipmentTime;
        return $this;
    }
}
