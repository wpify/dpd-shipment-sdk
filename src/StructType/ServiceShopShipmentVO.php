<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ServiceShopShipmentVO StructType
 * @subpackage Structs
 */
class ServiceShopShipmentVO extends AbstractStructBase
{
    /**
     * The code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * - nillable: true
     * @var string
     */
    public $code;
    /**
     * The addField
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO[]
     */
    public $addField;
    /**
     * Constructor method for ServiceShopShipmentVO
     * @uses ServiceShopShipmentVO::setCode()
     * @uses ServiceShopShipmentVO::setAddField()
     * @param string $code
     * @param \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO[] $addField
     */
    public function __construct($code = null, array $addField = array())
    {
        $this
            ->setCode($code)
            ->setAddField($addField);
    }
    /**
     * Get code value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return string|null
     */
    public function getCode()
    {
        return isset($this->code) ? $this->code : null;
    }
    /**
     * Set code value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @param string $code
     * @return \DPDSDK\Shipment\StructType\ServiceShopShipmentVO
     */
    public function setCode($code = null)
    {
        // validation for constraint: string
        if (!is_null($code) && !is_string($code)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($code, true), gettype($code)), __LINE__);
        }
        if (is_null($code) || (is_array($code) && empty($code))) {
            unset($this->code);
        } else {
            $this->code = $code;
        }
        return $this;
    }
    /**
     * Get addField value
     * An additional test has been added (isset) before returning the property value as
     * this property may have been unset before, due to the fact that this property is
     * removable from the request (nillable=true+minOccurs=0)
     * @return \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO[]|null
     */
    public function getAddField()
    {
        return isset($this->addField) ? $this->addField : null;
    }
    /**
     * This method is responsible for validating the values passed to the setAddField method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAddField method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAddFieldForArrayConstraintsFromSetAddField(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $serviceShopShipmentVOAddFieldItem) {
            // validation for constraint: itemType
            if (!$serviceShopShipmentVOAddFieldItem instanceof \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO) {
                $invalidValues[] = is_object($serviceShopShipmentVOAddFieldItem) ? get_class($serviceShopShipmentVOAddFieldItem) : sprintf('%s(%s)', gettype($serviceShopShipmentVOAddFieldItem), var_export($serviceShopShipmentVOAddFieldItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The addField property can only contain items of type \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set addField value
     * This property is removable from request (nillable=true+minOccurs=0), therefore
     * if the value assigned to this property is null, it is removed from this object
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO[] $addField
     * @return \DPDSDK\Shipment\StructType\ServiceShopShipmentVO
     */
    public function setAddField(array $addField = array())
    {
        // validation for constraint: array
        if ('' !== ($addFieldArrayErrorMessage = self::validateAddFieldForArrayConstraintsFromSetAddField($addField))) {
            throw new \InvalidArgumentException($addFieldArrayErrorMessage, __LINE__);
        }
        if (is_null($addField) || (is_array($addField) && empty($addField))) {
            unset($this->addField);
        } else {
            $this->addField = $addField;
        }
        return $this;
    }
    /**
     * Add item to addField value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO $item
     * @return \DPDSDK\Shipment\StructType\ServiceShopShipmentVO
     */
    public function addToAddField(\DPDSDK\Shipment\StructType\AddFieldShopShipmentVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO) {
            throw new \InvalidArgumentException(sprintf('The addField property can only contain items of type \DPDSDK\Shipment\StructType\AddFieldShopShipmentVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->addField[] = $item;
        return $this;
    }
}
