<?php

namespace DPDSDK\Shipment\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for PrintVO StructType
 * @subpackage Structs
 */
class PrintVO extends AbstractStructBase
{
    /**
     * The transactionId
     * @var int
     */
    public $transactionId;
    /**
     * The printParcelList
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\PrintDataVO[]
     */
    public $printParcelList;
    /**
     * The ftpFileName
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $ftpFileName;
    /**
     * The pdfFile
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $pdfFile;
    /**
     * The zpl
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $zpl;
    /**
     * The epl
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var string
     */
    public $epl;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - nillable: true
     * @var \DPDSDK\Shipment\StructType\ErrorVO
     */
    public $error;
    /**
     * Constructor method for PrintVO
     * @uses PrintVO::setTransactionId()
     * @uses PrintVO::setPrintParcelList()
     * @uses PrintVO::setFtpFileName()
     * @uses PrintVO::setPdfFile()
     * @uses PrintVO::setZpl()
     * @uses PrintVO::setEpl()
     * @uses PrintVO::setError()
     * @param int $transactionId
     * @param \DPDSDK\Shipment\StructType\PrintDataVO[] $printParcelList
     * @param string $ftpFileName
     * @param string $pdfFile
     * @param string $zpl
     * @param string $epl
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     */
    public function __construct($transactionId = null, array $printParcelList = array(), $ftpFileName = null, $pdfFile = null, $zpl = null, $epl = null, \DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this
            ->setTransactionId($transactionId)
            ->setPrintParcelList($printParcelList)
            ->setFtpFileName($ftpFileName)
            ->setPdfFile($pdfFile)
            ->setZpl($zpl)
            ->setEpl($epl)
            ->setError($error);
    }
    /**
     * Get transactionId value
     * @return int|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }
    /**
     * Set transactionId value
     * @param int $transactionId
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setTransactionId($transactionId = null)
    {
        // validation for constraint: int
        if (!is_null($transactionId) && !(is_int($transactionId) || ctype_digit($transactionId))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($transactionId, true), gettype($transactionId)), __LINE__);
        }
        $this->transactionId = $transactionId;
        return $this;
    }
    /**
     * Get printParcelList value
     * @return \DPDSDK\Shipment\StructType\PrintDataVO[]|null
     */
    public function getPrintParcelList()
    {
        return $this->printParcelList;
    }
    /**
     * This method is responsible for validating the values passed to the setPrintParcelList method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrintParcelList method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePrintParcelListForArrayConstraintsFromSetPrintParcelList(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $printVOPrintParcelListItem) {
            // validation for constraint: itemType
            if (!$printVOPrintParcelListItem instanceof \DPDSDK\Shipment\StructType\PrintDataVO) {
                $invalidValues[] = is_object($printVOPrintParcelListItem) ? get_class($printVOPrintParcelListItem) : sprintf('%s(%s)', gettype($printVOPrintParcelListItem), var_export($printVOPrintParcelListItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The printParcelList property can only contain items of type \DPDSDK\Shipment\StructType\PrintDataVO, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set printParcelList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\PrintDataVO[] $printParcelList
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setPrintParcelList(array $printParcelList = array())
    {
        // validation for constraint: array
        if ('' !== ($printParcelListArrayErrorMessage = self::validatePrintParcelListForArrayConstraintsFromSetPrintParcelList($printParcelList))) {
            throw new \InvalidArgumentException($printParcelListArrayErrorMessage, __LINE__);
        }
        $this->printParcelList = $printParcelList;
        return $this;
    }
    /**
     * Add item to printParcelList value
     * @throws \InvalidArgumentException
     * @param \DPDSDK\Shipment\StructType\PrintDataVO $item
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function addToPrintParcelList(\DPDSDK\Shipment\StructType\PrintDataVO $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \DPDSDK\Shipment\StructType\PrintDataVO) {
            throw new \InvalidArgumentException(sprintf('The printParcelList property can only contain items of type \DPDSDK\Shipment\StructType\PrintDataVO, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->printParcelList[] = $item;
        return $this;
    }
    /**
     * Get ftpFileName value
     * @return string|null
     */
    public function getFtpFileName()
    {
        return $this->ftpFileName;
    }
    /**
     * Set ftpFileName value
     * @param string $ftpFileName
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setFtpFileName($ftpFileName = null)
    {
        // validation for constraint: string
        if (!is_null($ftpFileName) && !is_string($ftpFileName)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ftpFileName, true), gettype($ftpFileName)), __LINE__);
        }
        $this->ftpFileName = $ftpFileName;
        return $this;
    }
    /**
     * Get pdfFile value
     * @return string|null
     */
    public function getPdfFile()
    {
        return $this->pdfFile;
    }
    /**
     * Set pdfFile value
     * @param string $pdfFile
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setPdfFile($pdfFile = null)
    {
        // validation for constraint: string
        if (!is_null($pdfFile) && !is_string($pdfFile)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pdfFile, true), gettype($pdfFile)), __LINE__);
        }
        $this->pdfFile = $pdfFile;
        return $this;
    }
    /**
     * Get zpl value
     * @return string|null
     */
    public function getZpl()
    {
        return $this->zpl;
    }
    /**
     * Set zpl value
     * @param string $zpl
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setZpl($zpl = null)
    {
        // validation for constraint: string
        if (!is_null($zpl) && !is_string($zpl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($zpl, true), gettype($zpl)), __LINE__);
        }
        $this->zpl = $zpl;
        return $this;
    }
    /**
     * Get epl value
     * @return string|null
     */
    public function getEpl()
    {
        return $this->epl;
    }
    /**
     * Set epl value
     * @param string $epl
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setEpl($epl = null)
    {
        // validation for constraint: string
        if (!is_null($epl) && !is_string($epl)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($epl, true), gettype($epl)), __LINE__);
        }
        $this->epl = $epl;
        return $this;
    }
    /**
     * Get error value
     * @return \DPDSDK\Shipment\StructType\ErrorVO|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \DPDSDK\Shipment\StructType\ErrorVO $error
     * @return \DPDSDK\Shipment\StructType\PrintVO
     */
    public function setError(\DPDSDK\Shipment\StructType\ErrorVO $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
