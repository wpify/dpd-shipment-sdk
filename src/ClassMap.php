<?php

namespace DPDSDK\Shipment;

/**
 * Class which returns the class map definition
 * @package
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get()
    {
        return array(
            'EShopException' => '\\DPDSDK\\Shipment\\StructType\\EShopException',
            'deleteShipment' => '\\DPDSDK\\Shipment\\StructType\\DeleteShipment',
            'ReferenceVO' => '\\DPDSDK\\Shipment\\StructType\\ReferenceVO',
            'deleteShipmentResponse' => '\\DPDSDK\\Shipment\\StructType\\DeleteShipmentResponse',
            'ShipmentDeleteResponseVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentDeleteResponseVO',
            'ShipmentDeleteResultVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentDeleteResultVO',
            'ErrorVO' => '\\DPDSDK\\Shipment\\StructType\\ErrorVO',
            'getParcelStatus' => '\\DPDSDK\\Shipment\\StructType\\GetParcelStatus',
            'getParcelStatusResponse' => '\\DPDSDK\\Shipment\\StructType\\GetParcelStatusResponse',
            'StatusResponseVO' => '\\DPDSDK\\Shipment\\StructType\\StatusResponseVO',
            'StatusResultVO' => '\\DPDSDK\\Shipment\\StructType\\StatusResultVO',
            'TrackVO' => '\\DPDSDK\\Shipment\\StructType\\TrackVO',
            'ScanVO' => '\\DPDSDK\\Shipment\\StructType\\ScanVO',
            'getShipment' => '\\DPDSDK\\Shipment\\StructType\\GetShipment',
            'getShipmentResponse' => '\\DPDSDK\\Shipment\\StructType\\GetShipmentResponse',
            'ShipmentGetResponseVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentGetResponseVO',
            'ShipmentGetResultVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentGetResultVO',
            'ShipmentVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentVO',
            'AdditionalServiceVO' => '\\DPDSDK\\Shipment\\StructType\\AdditionalServiceVO',
            'CodVO' => '\\DPDSDK\\Shipment\\StructType\\CodVO',
            'HighInsuranceVO' => '\\DPDSDK\\Shipment\\StructType\\HighInsuranceVO',
            'DocumentReturnVO' => '\\DPDSDK\\Shipment\\StructType\\DocumentReturnVO',
            'ExpayVO' => '\\DPDSDK\\Shipment\\StructType\\ExpayVO',
            'IDCheckVO' => '\\DPDSDK\\Shipment\\StructType\\IDCheckVO',
            'PredictEmailVO' => '\\DPDSDK\\Shipment\\StructType\\PredictEmailVO',
            'PredictSmsVO' => '\\DPDSDK\\Shipment\\StructType\\PredictSmsVO',
            'ParcelShopShipmentVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopShipmentVO',
            'MoreServicesShopShipmentVO' => '\\DPDSDK\\Shipment\\StructType\\MoreServicesShopShipmentVO',
            'ServiceShopShipmentVO' => '\\DPDSDK\\Shipment\\StructType\\ServiceShopShipmentVO',
            'AddFieldShopShipmentVO' => '\\DPDSDK\\Shipment\\StructType\\AddFieldShopShipmentVO',
            'TimeFrameVO' => '\\DPDSDK\\Shipment\\StructType\\TimeFrameVO',
            'ParcelVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelVO',
            'getShipmentLabel' => '\\DPDSDK\\Shipment\\StructType\\GetShipmentLabel',
            'getShipmentLabelResponse' => '\\DPDSDK\\Shipment\\StructType\\GetShipmentLabelResponse',
            'PrintVO' => '\\DPDSDK\\Shipment\\StructType\\PrintVO',
            'PrintDataVO' => '\\DPDSDK\\Shipment\\StructType\\PrintDataVO',
            'getShipmentStatus' => '\\DPDSDK\\Shipment\\StructType\\GetShipmentStatus',
            'getShipmentStatusResponse' => '\\DPDSDK\\Shipment\\StructType\\GetShipmentStatusResponse',
            'getParcelIdByParcelNumber' => '\\DPDSDK\\Shipment\\StructType\\GetParcelIdByParcelNumber',
            'getParcelIdByParcelNumberResponse' => '\\DPDSDK\\Shipment\\StructType\\GetParcelIdByParcelNumberResponse',
            'ParcelResponseVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelResponseVO',
            'ParcelDetailsVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelDetailsVO',
            'updateShipment' => '\\DPDSDK\\Shipment\\StructType\\UpdateShipment',
            'updateShipmentResponse' => '\\DPDSDK\\Shipment\\StructType\\UpdateShipmentResponse',
            'ShipmentRepsonseVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentRepsonseVO',
            'ShipmentResultVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentResultVO',
            'ParcelResultVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelResultVO',
            'PriceVO' => '\\DPDSDK\\Shipment\\StructType\\PriceVO',
            'searchShipment' => '\\DPDSDK\\Shipment\\StructType\\SearchShipment',
            'ShipmentSearchVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentSearchVO',
            'searchShipmentResponse' => '\\DPDSDK\\Shipment\\StructType\\SearchShipmentResponse',
            'ShipmentSearchResponseVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentSearchResponseVO',
            'ShipmentInfoVO' => '\\DPDSDK\\Shipment\\StructType\\ShipmentInfoVO',
            'parcelShopSearch' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearch',
            'ParcelShopSearchVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchVO',
            'parcelShopSearchResponse' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchResponse',
            'ParcelShopResponseVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopResponseVO',
            'ParcelShopVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopVO',
            'ParcelShopOpeningHoursVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopOpeningHoursVO',
            'ParcelShopHolidayVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopHolidayVO',
            'parcelShopSearchWL' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchWL',
            'ParcelShopSearchRequestVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchRequestVO',
            'UserLoginVO' => '\\DPDSDK\\Shipment\\StructType\\UserLoginVO',
            'ParcelShopSearchWLVO' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchWLVO',
            'parcelShopSearchWLResponse' => '\\DPDSDK\\Shipment\\StructType\\ParcelShopSearchWLResponse',
            'calculatePrice' => '\\DPDSDK\\Shipment\\StructType\\CalculatePrice',
            'calculatePriceResponse' => '\\DPDSDK\\Shipment\\StructType\\CalculatePriceResponse',
            'PriceResponseVO' => '\\DPDSDK\\Shipment\\StructType\\PriceResponseVO',
            'PriceResultVO' => '\\DPDSDK\\Shipment\\StructType\\PriceResultVO',
            'createShipment' => '\\DPDSDK\\Shipment\\StructType\\CreateShipment',
            'createShipmentResponse' => '\\DPDSDK\\Shipment\\StructType\\CreateShipmentResponse',
            'createShipmentWL' => '\\DPDSDK\\Shipment\\StructType\\CreateShipmentWL',
            'ShipmentVOWL' => '\\DPDSDK\\Shipment\\StructType\\ShipmentVOWL',
            'AdditionalServiceVOWL' => '\\DPDSDK\\Shipment\\StructType\\AdditionalServiceVOWL',
            'IdmSmsVOWL' => '\\DPDSDK\\Shipment\\StructType\\IdmSmsVOWL',
            'IdmEmailVOWL' => '\\DPDSDK\\Shipment\\StructType\\IdmEmailVOWL',
            'createShipmentWLResponse' => '\\DPDSDK\\Shipment\\StructType\\CreateShipmentWLResponse',
            'ShipmentRepsonseVOWL' => '\\DPDSDK\\Shipment\\StructType\\ShipmentRepsonseVOWL',
            'reprintParcelLabel' => '\\DPDSDK\\Shipment\\StructType\\ReprintParcelLabel',
            'reprintParcelLabelResponse' => '\\DPDSDK\\Shipment\\StructType\\ReprintParcelLabelResponse',
        );
    }
}
