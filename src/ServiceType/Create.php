<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Create ServiceType
 * @subpackage Services
 */
class Create extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named createShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\CreateShipment $createShipment
     * @return \DPDSDK\Shipment\StructType\CreateShipmentResponse|bool
     */
    public function createShipment(\DPDSDK\Shipment\StructType\CreateShipment $createShipment)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('createShipment', array(
                $createShipment,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named createShipmentWL
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\CreateShipmentWL $createShipmentWL
     * @return \DPDSDK\Shipment\StructType\CreateShipmentWLResponse|bool
     */
    public function createShipmentWL(\DPDSDK\Shipment\StructType\CreateShipmentWL $createShipmentWL)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('createShipmentWL', array(
                $createShipmentWL,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\CreateShipmentResponse|\DPDSDK\Shipment\StructType\CreateShipmentWLResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
