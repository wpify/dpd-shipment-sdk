<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Reprint ServiceType
 * @subpackage Services
 */
class Reprint extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named reprintParcelLabel
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\ReprintParcelLabel $reprintParcelLabel
     * @return \DPDSDK\Shipment\StructType\ReprintParcelLabelResponse|bool
     */
    public function reprintParcelLabel(\DPDSDK\Shipment\StructType\ReprintParcelLabel $reprintParcelLabel)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('reprintParcelLabel', array(
                $reprintParcelLabel,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\ReprintParcelLabelResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
