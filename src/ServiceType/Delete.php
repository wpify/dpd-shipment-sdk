<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Delete ServiceType
 * @subpackage Services
 */
class Delete extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named deleteShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\DeleteShipment $deleteShipment
     * @return \DPDSDK\Shipment\StructType\DeleteShipmentResponse|bool
     */
    public function deleteShipment(\DPDSDK\Shipment\StructType\DeleteShipment $deleteShipment)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('deleteShipment', array(
                $deleteShipment,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\DeleteShipmentResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
