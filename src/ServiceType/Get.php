<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named getParcelIdByParcelNumber
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber $getParcelIdByParcelNumber
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumberResponse|bool
     */
    public function getParcelIdByParcelNumber(\DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber $getParcelIdByParcelNumber)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('getParcelIdByParcelNumber', array(
                $getParcelIdByParcelNumber,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named getParcelStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\GetParcelStatus $getParcelStatus
     * @return \DPDSDK\Shipment\StructType\GetParcelStatusResponse|bool
     */
    public function getParcelStatus(\DPDSDK\Shipment\StructType\GetParcelStatus $getParcelStatus)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('getParcelStatus', array(
                $getParcelStatus,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named getShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\GetShipment $getShipment
     * @return \DPDSDK\Shipment\StructType\GetShipmentResponse|bool
     */
    public function getShipment(\DPDSDK\Shipment\StructType\GetShipment $getShipment)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('getShipment', array(
                $getShipment,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named getShipmentLabel
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\GetShipmentLabel $getShipmentLabel
     * @return \DPDSDK\Shipment\StructType\GetShipmentLabelResponse|bool
     */
    public function getShipmentLabel(\DPDSDK\Shipment\StructType\GetShipmentLabel $getShipmentLabel)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('getShipmentLabel', array(
                $getShipmentLabel,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named getShipmentStatus
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\GetShipmentStatus $getShipmentStatus
     * @return \DPDSDK\Shipment\StructType\GetShipmentStatusResponse|bool
     */
    public function getShipmentStatus(\DPDSDK\Shipment\StructType\GetShipmentStatus $getShipmentStatus)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('getShipmentStatus', array(
                $getShipmentStatus,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumberResponse|\DPDSDK\Shipment\StructType\GetParcelStatusResponse|\DPDSDK\Shipment\StructType\GetShipmentLabelResponse|\DPDSDK\Shipment\StructType\GetShipmentResponse|\DPDSDK\Shipment\StructType\GetShipmentStatusResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
