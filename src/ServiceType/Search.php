<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Search ServiceType
 * @subpackage Services
 */
class Search extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named searchShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\SearchShipment $searchShipment
     * @return \DPDSDK\Shipment\StructType\SearchShipmentResponse|bool
     */
    public function searchShipment(\DPDSDK\Shipment\StructType\SearchShipment $searchShipment)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('searchShipment', array(
                $searchShipment,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\SearchShipmentResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
