<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Parcel ServiceType
 * @subpackage Services
 */
class Parcel extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named parcelShopSearch
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearch $parcelShopSearch
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchResponse|bool
     */
    public function parcelShopSearch(\DPDSDK\Shipment\StructType\ParcelShopSearch $parcelShopSearch)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('parcelShopSearch', array(
                $parcelShopSearch,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named parcelShopSearchWL
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\ParcelShopSearchWL $parcelShopSearchWL
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchWLResponse|bool
     */
    public function parcelShopSearchWL(\DPDSDK\Shipment\StructType\ParcelShopSearchWL $parcelShopSearchWL)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('parcelShopSearchWL', array(
                $parcelShopSearchWL,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\ParcelShopSearchResponse|\DPDSDK\Shipment\StructType\ParcelShopSearchWLResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
