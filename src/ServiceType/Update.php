<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Update ServiceType
 * @subpackage Services
 */
class Update extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named updateShipment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\UpdateShipment $updateShipment
     * @return \DPDSDK\Shipment\StructType\UpdateShipmentResponse|bool
     */
    public function updateShipment(\DPDSDK\Shipment\StructType\UpdateShipment $updateShipment)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('updateShipment', array(
                $updateShipment,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\UpdateShipmentResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
