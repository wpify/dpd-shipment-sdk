<?php

namespace DPDSDK\Shipment\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Calculate ServiceType
 * @subpackage Services
 */
class Calculate extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named calculatePrice
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \DPDSDK\Shipment\StructType\CalculatePrice $calculatePrice
     * @return \DPDSDK\Shipment\StructType\CalculatePriceResponse|bool
     */
    public function calculatePrice(\DPDSDK\Shipment\StructType\CalculatePrice $calculatePrice)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('calculatePrice', array(
                $calculatePrice,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \DPDSDK\Shipment\StructType\CalculatePriceResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
