<?php

namespace DPDSDK\Shipment\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for printFormat EnumType
 * @subpackage Enumerations
 */
class PrintFormat extends AbstractStructEnumBase
{
    /**
     * Constant for value 'A4'
     * @return string 'A4'
     */
    const VALUE_A_4 = 'A4';
    /**
     * Constant for value 'A5'
     * @return string 'A5'
     */
    const VALUE_A_5 = 'A5';
    /**
     * Constant for value 'A6'
     * @return string 'A6'
     */
    const VALUE_A_6 = 'A6';
    /**
     * Return allowed values
     * @uses self::VALUE_A_4
     * @uses self::VALUE_A_5
     * @uses self::VALUE_A_6
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_A_4,
            self::VALUE_A_5,
            self::VALUE_A_6,
        );
    }
}
