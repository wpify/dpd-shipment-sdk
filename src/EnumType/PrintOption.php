<?php

namespace DPDSDK\Shipment\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for printOption EnumType
 * @subpackage Enumerations
 */
class PrintOption extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Data'
     * @return string 'Data'
     */
    const VALUE_DATA = 'Data';
    /**
     * Constant for value 'Pdf'
     * @return string 'Pdf'
     */
    const VALUE_PDF = 'Pdf';
    /**
     * Constant for value 'Ftp'
     * @return string 'Ftp'
     */
    const VALUE_FTP = 'Ftp';
    /**
     * Constant for value 'Zpl'
     * @return string 'Zpl'
     */
    const VALUE_ZPL = 'Zpl';
    /**
     * Constant for value 'Epl'
     * @return string 'Epl'
     */
    const VALUE_EPL = 'Epl';
    /**
     * Return allowed values
     * @uses self::VALUE_DATA
     * @uses self::VALUE_PDF
     * @uses self::VALUE_FTP
     * @uses self::VALUE_ZPL
     * @uses self::VALUE_EPL
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_DATA,
            self::VALUE_PDF,
            self::VALUE_FTP,
            self::VALUE_ZPL,
            self::VALUE_EPL,
        );
    }
}
