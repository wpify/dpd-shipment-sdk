<?php

namespace DPDSDK\Shipment\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for printPosition EnumType
 * @subpackage Enumerations
 */
class PrintPosition extends AbstractStructEnumBase
{
    /**
     * Constant for value 'LeftTop'
     * @return string 'LeftTop'
     */
    const VALUE_LEFT_TOP = 'LeftTop';
    /**
     * Constant for value 'LeftDown'
     * @return string 'LeftDown'
     */
    const VALUE_LEFT_DOWN = 'LeftDown';
    /**
     * Constant for value 'RightTop'
     * @return string 'RightTop'
     */
    const VALUE_RIGHT_TOP = 'RightTop';
    /**
     * Constant for value 'RightDown'
     * @return string 'RightDown'
     */
    const VALUE_RIGHT_DOWN = 'RightDown';
    /**
     * Return allowed values
     * @uses self::VALUE_LEFT_TOP
     * @uses self::VALUE_LEFT_DOWN
     * @uses self::VALUE_RIGHT_TOP
     * @uses self::VALUE_RIGHT_DOWN
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_LEFT_TOP,
            self::VALUE_LEFT_DOWN,
            self::VALUE_RIGHT_TOP,
            self::VALUE_RIGHT_DOWN,
        );
    }
}
