<?php

namespace DPDSDK\Shipment\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for paymentType EnumType
 * @subpackage Enumerations
 */
class PaymentType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'Cash'
     * @return string 'Cash'
     */
    const VALUE_CASH = 'Cash';
    /**
     * Constant for value 'CreditCard'
     * @return string 'CreditCard'
     */
    const VALUE_CREDIT_CARD = 'CreditCard';
    /**
     * Constant for value 'CrossedCheck'
     * @return string 'CrossedCheck'
     */
    const VALUE_CROSSED_CHECK = 'CrossedCheck';
    /**
     * Return allowed values
     * @uses self::VALUE_CASH
     * @uses self::VALUE_CREDIT_CARD
     * @uses self::VALUE_CROSSED_CHECK
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_CASH,
            self::VALUE_CREDIT_CARD,
            self::VALUE_CROSSED_CHECK,
        );
    }
}
