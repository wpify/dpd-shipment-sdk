<?php

namespace DPDSDK\Shipment\EnumType;

use \WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for shipmentPriceOption EnumType
 * @subpackage Enumerations
 */
class ShipmentPriceOption extends AbstractStructEnumBase
{
    /**
     * Constant for value 'WithPrice'
     * @return string 'WithPrice'
     */
    const VALUE_WITH_PRICE = 'WithPrice';
    /**
     * Constant for value 'WithoutPrice'
     * @return string 'WithoutPrice'
     */
    const VALUE_WITHOUT_PRICE = 'WithoutPrice';
    /**
     * Return allowed values
     * @uses self::VALUE_WITH_PRICE
     * @uses self::VALUE_WITHOUT_PRICE
     * @return string[]
     */
    public static function getValidValues()
    {
        return array(
            self::VALUE_WITH_PRICE,
            self::VALUE_WITHOUT_PRICE,
        );
    }
}
