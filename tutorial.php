<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://www.mojedpd.cz/IT4EMWebServices/eshop/ShipmentServiceImpl?wsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://www.mojedpd.cz/IT4EMWebServices/eshop/ShipmentServiceImpl?wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \DPDSDK\Shipment\ClassMap::get(),
);
/**
 * Samples for Calculate ServiceType
 */
$calculate = new \DPDSDK\Shipment\ServiceType\Calculate($options);
/**
 * Sample call for calculatePrice operation/method
 */
if ($calculate->calculatePrice(new \DPDSDK\Shipment\StructType\CalculatePrice()) !== false) {
    print_r($calculate->getResult());
} else {
    print_r($calculate->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \DPDSDK\Shipment\ServiceType\Create($options);
/**
 * Sample call for createShipment operation/method
 */
if ($create->createShipment(new \DPDSDK\Shipment\StructType\CreateShipment()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Sample call for createShipmentWL operation/method
 */
if ($create->createShipmentWL(new \DPDSDK\Shipment\StructType\CreateShipmentWL()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Delete ServiceType
 */
$delete = new \DPDSDK\Shipment\ServiceType\Delete($options);
/**
 * Sample call for deleteShipment operation/method
 */
if ($delete->deleteShipment(new \DPDSDK\Shipment\StructType\DeleteShipment()) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \DPDSDK\Shipment\ServiceType\Get($options);
/**
 * Sample call for getParcelIdByParcelNumber operation/method
 */
if ($get->getParcelIdByParcelNumber(new \DPDSDK\Shipment\StructType\GetParcelIdByParcelNumber()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getParcelStatus operation/method
 */
if ($get->getParcelStatus(new \DPDSDK\Shipment\StructType\GetParcelStatus()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getShipment operation/method
 */
if ($get->getShipment(new \DPDSDK\Shipment\StructType\GetShipment()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getShipmentLabel operation/method
 */
if ($get->getShipmentLabel(new \DPDSDK\Shipment\StructType\GetShipmentLabel()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for getShipmentStatus operation/method
 */
if ($get->getShipmentStatus(new \DPDSDK\Shipment\StructType\GetShipmentStatus()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Parcel ServiceType
 */
$parcel = new \DPDSDK\Shipment\ServiceType\Parcel($options);
/**
 * Sample call for parcelShopSearch operation/method
 */
if ($parcel->parcelShopSearch(new \DPDSDK\Shipment\StructType\ParcelShopSearch()) !== false) {
    print_r($parcel->getResult());
} else {
    print_r($parcel->getLastError());
}
/**
 * Sample call for parcelShopSearchWL operation/method
 */
if ($parcel->parcelShopSearchWL(new \DPDSDK\Shipment\StructType\ParcelShopSearchWL()) !== false) {
    print_r($parcel->getResult());
} else {
    print_r($parcel->getLastError());
}
/**
 * Samples for Reprint ServiceType
 */
$reprint = new \DPDSDK\Shipment\ServiceType\Reprint($options);
/**
 * Sample call for reprintParcelLabel operation/method
 */
if ($reprint->reprintParcelLabel(new \DPDSDK\Shipment\StructType\ReprintParcelLabel()) !== false) {
    print_r($reprint->getResult());
} else {
    print_r($reprint->getLastError());
}
/**
 * Samples for Search ServiceType
 */
$search = new \DPDSDK\Shipment\ServiceType\Search($options);
/**
 * Sample call for searchShipment operation/method
 */
if ($search->searchShipment(new \DPDSDK\Shipment\StructType\SearchShipment()) !== false) {
    print_r($search->getResult());
} else {
    print_r($search->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \DPDSDK\Shipment\ServiceType\Update($options);
/**
 * Sample call for updateShipment operation/method
 */
if ($update->updateShipment(new \DPDSDK\Shipment\StructType\UpdateShipment()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
